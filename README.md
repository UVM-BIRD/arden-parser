# Arden Parser #

Arden Parser is a Java library that primarily consists of [ANTLR 4 grammars](http://www.antlr.org/) designed to parse Medical Logic Modules (MLMs) in the Arden-syntax language.  This library is designed to be used in conjunction with the [Arden Model](https://bitbucket.org/UVM-BIRD/arden-model).

### Usage ###

Including the Arden Parser library in your project can be accomplished by including the following dependency in your project's Maven POM:

    <dependency>
        <groupId>edu.uvm.ccts</groupId>
        <artifactId>arden-parser</artifactId>
        <version>1.0.1</version>
    </dependency>

### Building Arden Parser ###

Building the Arden Parser library and making it available to other local Maven projects is accomplished by running the following:

    $ mvn package
    $ mvn install

### Rebuilding Grammars ###

Rebuilding Arden-syntax grammars is handled by the `build_grammar` shell script.  Execute this script without parameters for a list of command-line options.

#### Example 1 - script executed without parameters:

    $ ./build_grammar
    Please specify individual grammar to build ('mlm', 'logic', 'data', 'jms', 'evoke', 'action', 'expr'), or 'all' if you would like to build all grammars.

#### Example 2 - script executed with 'mlm' as a parameter:

    $ ./build_grammar mlm
    Rebuilding MLM lexer and parser -
	Done.

### Testing Grammars ###

Testing grammars is accomplished using the `test_grammar` shell script.  Execute this script without parameters for a list of command-line options.

When the test executes, it will run ANTLR's TestRig program against the appropriate file in the test_files folder, and will display a graphical representation of the file's generated abstract syntax tree.  This is a simple, useful way to test whether or not something is parsing correctly, and can quickly illustrate issues in the grammar.

_Note that you must first compile project sources before testing grammars._


### License and Copyright ###

The Arden Parser library is Copyright 2015 [The University of Vermont and State Agricultural College](https://www.uvm.edu/), [Vermont Oxford Network](https://public.vtoxford.org/), and [The University of Vermont Medical Center](https://www.uvmhealth.org/medcenter/Pages/default.aspx).  All rights reserved.

The Arden Parser library is licensed under the terms of the [GNU General Public License (GPL) version 3](https://www.gnu.org/licenses/gpl-3.0-standalone.html).


### ANTLR 4 License and Copyright ###

In order to simplify the building process of this project, ANTLR 4 binaries are included in this project.  In compliance with ANTLR licensing terms and conditions, the following is the ANTLR 4 license and copyright statement:

> **ANTLR 4 License**
>
> [The BSD License]
>
> Copyright (c) 2012 Terence Parr and Sam Harwell
> All rights reserved.
>
> Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
>
> * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
> * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
> * Neither the name of the author nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
>
> THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.