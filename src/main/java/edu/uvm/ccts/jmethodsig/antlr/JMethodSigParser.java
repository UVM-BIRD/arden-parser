// Generated from JMethodSig.g4 by ANTLR 4.1
package edu.uvm.ccts.jmethodsig.antlr;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class JMethodSigParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__6=1, T__5=2, T__4=3, T__3=4, T__2=5, T__1=6, T__0=7, ListType=8, MapType=9, 
		PrimitiveType=10, ID=11, WS=12;
	public static final String[] tokenNames = {
		"<INVALID>", "'>'", "'.'", "')'", "','", "':'", "'('", "'<'", "ListType", 
		"'Map'", "PrimitiveType", "ID", "WS"
	};
	public static final int
		RULE_init = 0, RULE_className = 1, RULE_methodName = 2, RULE_parameters = 3, 
		RULE_type = 4, RULE_complexType = 5;
	public static final String[] ruleNames = {
		"init", "className", "methodName", "parameters", "type", "complexType"
	};

	@Override
	public String getGrammarFileName() { return "JMethodSig.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public JMethodSigParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class InitContext extends ParserRuleContext {
		public MethodNameContext methodName() {
			return getRuleContext(MethodNameContext.class,0);
		}
		public TerminalNode EOF() { return getToken(JMethodSigParser.EOF, 0); }
		public ClassNameContext className() {
			return getRuleContext(ClassNameContext.class,0);
		}
		public ParametersContext parameters() {
			return getRuleContext(ParametersContext.class,0);
		}
		public InitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_init; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JMethodSigVisitor ) return ((JMethodSigVisitor<? extends T>)visitor).visitInit(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InitContext init() throws RecognitionException {
		InitContext _localctx = new InitContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_init);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(12); className();
			setState(13); match(5);
			setState(14); methodName();
			setState(15); parameters();
			setState(16); match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassNameContext extends ParserRuleContext {
		public List<TerminalNode> ID() { return getTokens(JMethodSigParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(JMethodSigParser.ID, i);
		}
		public ClassNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_className; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JMethodSigVisitor ) return ((JMethodSigVisitor<? extends T>)visitor).visitClassName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ClassNameContext className() throws RecognitionException {
		ClassNameContext _localctx = new ClassNameContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_className);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(20); 
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(18); match(ID);
					setState(19); match(2);
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(22); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			} while ( _alt!=2 && _alt!=-1 );
			setState(24); match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodNameContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(JMethodSigParser.ID, 0); }
		public MethodNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodName; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JMethodSigVisitor ) return ((JMethodSigVisitor<? extends T>)visitor).visitMethodName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MethodNameContext methodName() throws RecognitionException {
		MethodNameContext _localctx = new MethodNameContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_methodName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(26); match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParametersContext extends ParserRuleContext {
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public ParametersContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameters; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JMethodSigVisitor ) return ((JMethodSigVisitor<? extends T>)visitor).visitParameters(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParametersContext parameters() throws RecognitionException {
		ParametersContext _localctx = new ParametersContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_parameters);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(28); match(6);
			setState(37);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ListType) | (1L << MapType) | (1L << PrimitiveType))) != 0)) {
				{
				setState(29); type();
				setState(34);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==4) {
					{
					{
					setState(30); match(4);
					setState(31); type();
					}
					}
					setState(36);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(39); match(3);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public ComplexTypeContext complexType() {
			return getRuleContext(ComplexTypeContext.class,0);
		}
		public TerminalNode PrimitiveType() { return getToken(JMethodSigParser.PrimitiveType, 0); }
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JMethodSigVisitor ) return ((JMethodSigVisitor<? extends T>)visitor).visitType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_type);
		try {
			setState(43);
			switch (_input.LA(1)) {
			case PrimitiveType:
				enterOuterAlt(_localctx, 1);
				{
				setState(41); match(PrimitiveType);
				}
				break;
			case ListType:
			case MapType:
				enterOuterAlt(_localctx, 2);
				{
				setState(42); complexType();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ComplexTypeContext extends ParserRuleContext {
		public TerminalNode MapType() { return getToken(JMethodSigParser.MapType, 0); }
		public TerminalNode ListType() { return getToken(JMethodSigParser.ListType, 0); }
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public ComplexTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_complexType; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof JMethodSigVisitor ) return ((JMethodSigVisitor<? extends T>)visitor).visitComplexType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ComplexTypeContext complexType() throws RecognitionException {
		ComplexTypeContext _localctx = new ComplexTypeContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_complexType);
		int _la;
		try {
			setState(61);
			switch (_input.LA(1)) {
			case ListType:
				enterOuterAlt(_localctx, 1);
				{
				setState(45); match(ListType);
				setState(50);
				_la = _input.LA(1);
				if (_la==7) {
					{
					setState(46); match(7);
					setState(47); type();
					setState(48); match(1);
					}
				}

				}
				break;
			case MapType:
				enterOuterAlt(_localctx, 2);
				{
				setState(52); match(MapType);
				setState(59);
				_la = _input.LA(1);
				if (_la==7) {
					{
					setState(53); match(7);
					setState(54); type();
					setState(55); match(4);
					setState(56); type();
					setState(57); match(1);
					}
				}

				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\uacf5\uee8c\u4f5d\u8b0d\u4a45\u78bd\u1b2f\u3378\3\16B\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\6\3"+
		"\27\n\3\r\3\16\3\30\3\3\3\3\3\4\3\4\3\5\3\5\3\5\3\5\7\5#\n\5\f\5\16\5"+
		"&\13\5\5\5(\n\5\3\5\3\5\3\6\3\6\5\6.\n\6\3\7\3\7\3\7\3\7\3\7\5\7\65\n"+
		"\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7>\n\7\5\7@\n\7\3\7\2\b\2\4\6\b\n\f\2"+
		"\2B\2\16\3\2\2\2\4\26\3\2\2\2\6\34\3\2\2\2\b\36\3\2\2\2\n-\3\2\2\2\f?"+
		"\3\2\2\2\16\17\5\4\3\2\17\20\7\7\2\2\20\21\5\6\4\2\21\22\5\b\5\2\22\23"+
		"\7\2\2\3\23\3\3\2\2\2\24\25\7\r\2\2\25\27\7\4\2\2\26\24\3\2\2\2\27\30"+
		"\3\2\2\2\30\26\3\2\2\2\30\31\3\2\2\2\31\32\3\2\2\2\32\33\7\r\2\2\33\5"+
		"\3\2\2\2\34\35\7\r\2\2\35\7\3\2\2\2\36\'\7\b\2\2\37$\5\n\6\2 !\7\6\2\2"+
		"!#\5\n\6\2\" \3\2\2\2#&\3\2\2\2$\"\3\2\2\2$%\3\2\2\2%(\3\2\2\2&$\3\2\2"+
		"\2\'\37\3\2\2\2\'(\3\2\2\2()\3\2\2\2)*\7\5\2\2*\t\3\2\2\2+.\7\f\2\2,."+
		"\5\f\7\2-+\3\2\2\2-,\3\2\2\2.\13\3\2\2\2/\64\7\n\2\2\60\61\7\t\2\2\61"+
		"\62\5\n\6\2\62\63\7\3\2\2\63\65\3\2\2\2\64\60\3\2\2\2\64\65\3\2\2\2\65"+
		"@\3\2\2\2\66=\7\13\2\2\678\7\t\2\289\5\n\6\29:\7\6\2\2:;\5\n\6\2;<\7\3"+
		"\2\2<>\3\2\2\2=\67\3\2\2\2=>\3\2\2\2>@\3\2\2\2?/\3\2\2\2?\66\3\2\2\2@"+
		"\r\3\2\2\2\t\30$\'-\64=?";
	public static final ATN _ATN =
		ATNSimulator.deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}