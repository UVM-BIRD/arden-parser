// Generated from JMethodSig.g4 by ANTLR 4.1
package edu.uvm.ccts.jmethodsig.antlr;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link JMethodSigParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface JMethodSigVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link JMethodSigParser#complexType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComplexType(@NotNull JMethodSigParser.ComplexTypeContext ctx);

	/**
	 * Visit a parse tree produced by {@link JMethodSigParser#init}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInit(@NotNull JMethodSigParser.InitContext ctx);

	/**
	 * Visit a parse tree produced by {@link JMethodSigParser#className}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassName(@NotNull JMethodSigParser.ClassNameContext ctx);

	/**
	 * Visit a parse tree produced by {@link JMethodSigParser#parameters}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameters(@NotNull JMethodSigParser.ParametersContext ctx);

	/**
	 * Visit a parse tree produced by {@link JMethodSigParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(@NotNull JMethodSigParser.TypeContext ctx);

	/**
	 * Visit a parse tree produced by {@link JMethodSigParser#methodName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodName(@NotNull JMethodSigParser.MethodNameContext ctx);
}