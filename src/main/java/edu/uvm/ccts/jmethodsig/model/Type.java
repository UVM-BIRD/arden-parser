/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Arden Parser.
 *
 * Arden Parser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Arden Parser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Arden Parser.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.jmethodsig.model;

import edu.uvm.ccts.common.exceptions.antlr.GrammarException;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by mstorer on 11/19/13.
 */
public class Type {
    private String className;

    public Type(String className) {
        this.className = className;
    }

    @Override
    public String toString() {
        return className;
    }

    public Class asClass() {
        ClassType ct = ClassType.valueOf(className.toUpperCase());

        switch (ct) {
            case BYTE :         return Byte.class;
            case SHORT :        return Short.class;
            case INT :
            case INTEGER :      return Integer.class;
            case LONG :         return Long.class;
            case FLOAT :        return Float.class;
            case DOUBLE :       return Double.class;
            case BOOLEAN :      return Boolean.class;
            case CHAR :
            case CHARACTER :    return Character.class;
            case STRING :       return String.class;
            case DATE :         return Date.class;
            case LIST :         return List.class;
            case COLLECTION :   return Collection.class;
        }

        throw new GrammarException("unrecognized class '" + className + "'");
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    private static enum ClassType {
        BYTE,
        SHORT,
        INT,
        INTEGER,
        LONG,
        FLOAT,
        DOUBLE,
        BOOLEAN,
        CHAR,
        CHARACTER,
        STRING,
        DATE,
        LIST,
        COLLECTION
    }
}
