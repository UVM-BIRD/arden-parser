/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Arden Parser.
 *
 * Arden Parser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Arden Parser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Arden Parser.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.jmethodsig.model;

import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mstorer on 11/19/13.
 */
public class JMethodSig {
    private String className;
    private String methodName ;
    private List<Type> parameters = new ArrayList<Type>();

    @Override
    public String toString() {
        List<String> params = new ArrayList<String>();
        for (Type t : parameters) {
            params.add(t.toString());
        }

        return className + ":" + methodName + "(" + StringUtils.join(params, ", ") + ")";
    }

    public Class[] getParameterClassArray() {
        Class[] arr = new Class[parameters.size()];
        for (int i = 0; i < parameters.size(); i ++) {
            arr[i] = parameters.get(i).asClass();
        }
        return arr;
    }

    public Object execute(Object[] args) throws ClassNotFoundException, IllegalAccessException,
            InstantiationException, NoSuchMethodException, InvocationTargetException {

        Class c = Class.forName(getClassName());
        Object t = c.newInstance();

        Method m = c.getMethod(getMethodName(), getParameterClassArray());
        m.setAccessible(true);

        return m.invoke(t, args);
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public List<Type> getParameters() {
        return parameters;
    }

    public void setParameters(List<Type> parameters) {
        this.parameters = parameters;
    }
}
