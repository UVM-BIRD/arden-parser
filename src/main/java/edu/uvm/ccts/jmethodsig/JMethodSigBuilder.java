/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Arden Parser.
 *
 * Arden Parser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Arden Parser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Arden Parser.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.jmethodsig;

import edu.uvm.ccts.jmethodsig.model.JMethodSig;
import edu.uvm.ccts.jmethodsig.antlr.JMethodSigLexer;
import edu.uvm.ccts.jmethodsig.antlr.JMethodSigParser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by mstorer on 11/20/13.
 */
public class JMethodSigBuilder {
    public static JMethodSig build(String s) throws IOException {
        InputStream inputStream = new ByteArrayInputStream(s.getBytes());
        ANTLRInputStream input = new ANTLRInputStream(inputStream);
        JMethodSigLexer lexer = new BailJMethodSigLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        JMethodSigParser parser = new BailJMethodSigParser(tokens);
        ParseTree tree = parser.init();
        JMethodSigEvalVisitor visitor = new JMethodSigEvalVisitor();
        visitor.visit(tree);
        return visitor.getSignature();
    }
}
