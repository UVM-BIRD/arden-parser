/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Arden Parser.
 *
 * Arden Parser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Arden Parser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Arden Parser.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.jmethodsig;

import edu.uvm.ccts.jmethodsig.model.JMethodSig;
import edu.uvm.ccts.jmethodsig.model.Type;
import edu.uvm.ccts.jmethodsig.antlr.JMethodSigBaseVisitor;
import edu.uvm.ccts.jmethodsig.antlr.JMethodSigParser;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mstorer on 11/19/13.
 */
public class JMethodSigEvalVisitor extends JMethodSigBaseVisitor<Object> {
    private JMethodSig signature;

    public JMethodSig getSignature() {
        return signature;
    }

    @Override
    public Object visitInit(@NotNull JMethodSigParser.InitContext ctx) {
        signature = new JMethodSig();

        return super.visitInit(ctx);
    }

    @Override
    public Object visitClassName(@NotNull JMethodSigParser.ClassNameContext ctx) {
        List<String> pathParts = new ArrayList<String>();
        for (TerminalNode tn : ctx.ID()) {
            pathParts.add(tn.getText());
        }

        String s = StringUtils.join(pathParts, ".");

        signature.setClassName(s);

        return super.visitClassName(ctx);
    }

    @Override
    public Object visitMethodName(@NotNull JMethodSigParser.MethodNameContext ctx) {
        String s = ctx.ID().getText();

        signature.setMethodName(s);

        return super.visitMethodName(ctx);
    }

    @Override
    public Object visitParameters(@NotNull JMethodSigParser.ParametersContext ctx) {
        List<Type> parameters = new ArrayList<Type>();

        for (JMethodSigParser.TypeContext tCtx : ctx.type()) {
            Object o = visit(tCtx);
            parameters.add((Type) o);
        }

        signature.setParameters(parameters);

        return super.visitParameters(ctx);
    }

    @Override
    public Object visitType(@NotNull JMethodSigParser.TypeContext ctx) {
        Type type;

        if (ctx.PrimitiveType() != null) {
            type = new Type(ctx.PrimitiveType().getText());

        } else if (ctx.complexType() != null) {
            Object o = visit(ctx.complexType());
            type = (Type) o;

        } else {
            throw new RuntimeException("unexpected parameter type");
        }

        return type;
    }

    @Override
    public Object visitComplexType(@NotNull JMethodSigParser.ComplexTypeContext ctx) {

        // we don't care about generics - grammar ref is purely for matching

        if (ctx.ListType() != null) {
            return new Type(ctx.ListType().getText());

        } else if (ctx.MapType() != null) {
            return new Type(ctx.MapType().getText());

        } else {
            throw new RuntimeException("unexpected complex type");
        }
    }
}
