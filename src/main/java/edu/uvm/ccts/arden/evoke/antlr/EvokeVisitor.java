// Generated from Evoke.g4 by ANTLR 4.1
package edu.uvm.ccts.arden.evoke.antlr;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link EvokeParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface EvokeVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link EvokeParser#durationExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDurationExpr(@NotNull EvokeParser.DurationExprContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#arccos}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArccos(@NotNull EvokeParser.ArccosContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#no}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNo(@NotNull EvokeParser.NoContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#asString}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsString(@NotNull EvokeParser.AsStringContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#firstFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFirstFrom(@NotNull EvokeParser.FirstFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#dot}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDot(@NotNull EvokeParser.DotContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#temporalUnit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTemporalUnit(@NotNull EvokeParser.TemporalUnitContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#minimumFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMinimumFrom(@NotNull EvokeParser.MinimumFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#indexType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexType(@NotNull EvokeParser.IndexTypeContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#matches}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMatches(@NotNull EvokeParser.MatchesContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#durationUnit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDurationUnit(@NotNull EvokeParser.DurationUnitContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#abs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAbs(@NotNull EvokeParser.AbsContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#isIn}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsIn(@NotNull EvokeParser.IsInContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#indexOfFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexOfFrom(@NotNull EvokeParser.IndexOfFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#substring}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubstring(@NotNull EvokeParser.SubstringContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#element}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElement(@NotNull EvokeParser.ElementContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#clone}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClone(@NotNull EvokeParser.CloneContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#occurEqual}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurEqual(@NotNull EvokeParser.OccurEqualContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#isWithinFollowing}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsWithinFollowing(@NotNull EvokeParser.IsWithinFollowingContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#unaryMinus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryMinus(@NotNull EvokeParser.UnaryMinusContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#or}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOr(@NotNull EvokeParser.OrContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#unaryList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryList(@NotNull EvokeParser.UnaryListContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#parens}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParens(@NotNull EvokeParser.ParensContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#timeExprComponent}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimeExprComponent(@NotNull EvokeParser.TimeExprComponentContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#log}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLog(@NotNull EvokeParser.LogContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#atMostOrLeast}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtMostOrLeast(@NotNull EvokeParser.AtMostOrLeastContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#extract}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExtract(@NotNull EvokeParser.ExtractContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#timeOfDayVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimeOfDayVal(@NotNull EvokeParser.TimeOfDayValContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#not}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNot(@NotNull EvokeParser.NotContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#median}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMedian(@NotNull EvokeParser.MedianContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#floor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFloor(@NotNull EvokeParser.FloorContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#now}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNow(@NotNull EvokeParser.NowContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#truncate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTruncate(@NotNull EvokeParser.TruncateContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#isLessThanOrEqual}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsLessThanOrEqual(@NotNull EvokeParser.IsLessThanOrEqualContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#nullVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNullVal(@NotNull EvokeParser.NullValContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#occurBefore}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurBefore(@NotNull EvokeParser.OccurBeforeContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#whereTimeIsPresent}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhereTimeIsPresent(@NotNull EvokeParser.WhereTimeIsPresentContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#sort}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSort(@NotNull EvokeParser.SortContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#interval}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInterval(@NotNull EvokeParser.IntervalContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#minimum}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMinimum(@NotNull EvokeParser.MinimumContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#before}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBefore(@NotNull EvokeParser.BeforeContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#merge}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMerge(@NotNull EvokeParser.MergeContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#isLessThan}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsLessThan(@NotNull EvokeParser.IsLessThanContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#isBefore}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsBefore(@NotNull EvokeParser.IsBeforeContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#unaryPlus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryPlus(@NotNull EvokeParser.UnaryPlusContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#length}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLength(@NotNull EvokeParser.LengthContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#addToList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAddToList(@NotNull EvokeParser.AddToListContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#atTime}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtTime(@NotNull EvokeParser.AtTimeContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#occurWithinFollowing}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurWithinFollowing(@NotNull EvokeParser.OccurWithinFollowingContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#earliestFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEarliestFrom(@NotNull EvokeParser.EarliestFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#occurWithinPreceding}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurWithinPreceding(@NotNull EvokeParser.OccurWithinPrecedingContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#reverse}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReverse(@NotNull EvokeParser.ReverseContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#isDataType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsDataType(@NotNull EvokeParser.IsDataTypeContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#arctan}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArctan(@NotNull EvokeParser.ArctanContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#sqrt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSqrt(@NotNull EvokeParser.SqrtContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#occurWithinPast}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurWithinPast(@NotNull EvokeParser.OccurWithinPastContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#asNumber}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsNumber(@NotNull EvokeParser.AsNumberContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#divide}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDivide(@NotNull EvokeParser.DivideContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#stringVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringVal(@NotNull EvokeParser.StringValContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#replace}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReplace(@NotNull EvokeParser.ReplaceContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#arcsin}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArcsin(@NotNull EvokeParser.ArcsinContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#isWithinTo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsWithinTo(@NotNull EvokeParser.IsWithinToContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#maximumFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMaximumFrom(@NotNull EvokeParser.MaximumFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#isWithinSurrounding}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsWithinSurrounding(@NotNull EvokeParser.IsWithinSurroundingContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#isObjectType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsObjectType(@NotNull EvokeParser.IsObjectTypeContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#ago}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAgo(@NotNull EvokeParser.AgoContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#decrease}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDecrease(@NotNull EvokeParser.DecreaseContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#stdDev}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStdDev(@NotNull EvokeParser.StdDevContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#extractChars}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExtractChars(@NotNull EvokeParser.ExtractCharsContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#last}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLast(@NotNull EvokeParser.LastContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#count}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCount(@NotNull EvokeParser.CountContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#numberVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumberVal(@NotNull EvokeParser.NumberValContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#booleanVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBooleanVal(@NotNull EvokeParser.BooleanValContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#round}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRound(@NotNull EvokeParser.RoundContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#where}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhere(@NotNull EvokeParser.WhereContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#simpleTrigger}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimpleTrigger(@NotNull EvokeParser.SimpleTriggerContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#isWithinPreceding}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsWithinPreceding(@NotNull EvokeParser.IsWithinPrecedingContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#lastFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLastFrom(@NotNull EvokeParser.LastFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#binaryList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinaryList(@NotNull EvokeParser.BinaryListContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#isAfter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsAfter(@NotNull EvokeParser.IsAfterContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#after}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAfter(@NotNull EvokeParser.AfterContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#buildString}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuildString(@NotNull EvokeParser.BuildStringContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#uppercase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUppercase(@NotNull EvokeParser.UppercaseContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#occurWithinTo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurWithinTo(@NotNull EvokeParser.OccurWithinToContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#earliest}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEarliest(@NotNull EvokeParser.EarliestContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#occurWithinSameDay}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurWithinSameDay(@NotNull EvokeParser.OccurWithinSameDayContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#dayOfWeekFunc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDayOfWeekFunc(@NotNull EvokeParser.DayOfWeekFuncContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#any}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAny(@NotNull EvokeParser.AnyContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#isWithinSameDay}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsWithinSameDay(@NotNull EvokeParser.IsWithinSameDayContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#subList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubList(@NotNull EvokeParser.SubListContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#objNamedWith}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObjNamedWith(@NotNull EvokeParser.ObjNamedWithContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#constTimeVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstTimeVal(@NotNull EvokeParser.ConstTimeValContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#index}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndex(@NotNull EvokeParser.IndexContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#init}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInit(@NotNull EvokeParser.InitContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#timeExprSimple}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimeExprSimple(@NotNull EvokeParser.TimeExprSimpleContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#latestFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLatestFrom(@NotNull EvokeParser.LatestFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#indexFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexFrom(@NotNull EvokeParser.IndexFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#and}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnd(@NotNull EvokeParser.AndContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#isGreaterThanOrEqual}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsGreaterThanOrEqual(@NotNull EvokeParser.IsGreaterThanOrEqualContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#extractAttrNames}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExtractAttrNames(@NotNull EvokeParser.ExtractAttrNamesContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExp(@NotNull EvokeParser.ExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#sine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSine(@NotNull EvokeParser.SineContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#ceiling}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCeiling(@NotNull EvokeParser.CeilingContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#isEqual}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsEqual(@NotNull EvokeParser.IsEqualContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#maximum}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMaximum(@NotNull EvokeParser.MaximumContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#asTime}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsTime(@NotNull EvokeParser.AsTimeContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#tangent}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTangent(@NotNull EvokeParser.TangentContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#isGreaterThan}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsGreaterThan(@NotNull EvokeParser.IsGreaterThanContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#occurAfter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurAfter(@NotNull EvokeParser.OccurAfterContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#timeExprSimpleComponent}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimeExprSimpleComponent(@NotNull EvokeParser.TimeExprSimpleComponentContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#perEventTrigger}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPerEventTrigger(@NotNull EvokeParser.PerEventTriggerContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#sum}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSum(@NotNull EvokeParser.SumContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#add}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdd(@NotNull EvokeParser.AddContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#seqto}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSeqto(@NotNull EvokeParser.SeqtoContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#timeOfDayFunc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimeOfDayFunc(@NotNull EvokeParser.TimeOfDayFuncContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#occurWithinSurrounding}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurWithinSurrounding(@NotNull EvokeParser.OccurWithinSurroundingContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#id}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitId(@NotNull EvokeParser.IdContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#newObject}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNewObject(@NotNull EvokeParser.NewObjectContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#attributeFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAttributeFrom(@NotNull EvokeParser.AttributeFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#lowercase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLowercase(@NotNull EvokeParser.LowercaseContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#objOrderedWith}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObjOrderedWith(@NotNull EvokeParser.ObjOrderedWithContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#timeVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimeVal(@NotNull EvokeParser.TimeValContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#constPerTimeTrigger}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstPerTimeTrigger(@NotNull EvokeParser.ConstPerTimeTriggerContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#trim}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTrim(@NotNull EvokeParser.TrimContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#exist}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExist(@NotNull EvokeParser.ExistContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#dayOfWeek}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDayOfWeek(@NotNull EvokeParser.DayOfWeekContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#all}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAll(@NotNull EvokeParser.AllContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#timeExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimeExpr(@NotNull EvokeParser.TimeExprContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#cosine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCosine(@NotNull EvokeParser.CosineContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt(@NotNull EvokeParser.StmtContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#removeFromList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRemoveFromList(@NotNull EvokeParser.RemoveFromListContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#multiply}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiply(@NotNull EvokeParser.MultiplyContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#variance}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariance(@NotNull EvokeParser.VarianceContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#concat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConcat(@NotNull EvokeParser.ConcatContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#nearest}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNearest(@NotNull EvokeParser.NearestContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#emptyList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEmptyList(@NotNull EvokeParser.EmptyListContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#print}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrint(@NotNull EvokeParser.PrintContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#log10}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLog10(@NotNull EvokeParser.Log10Context ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#constTimeTrigger}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstTimeTrigger(@NotNull EvokeParser.ConstTimeTriggerContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#currentTime}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCurrentTime(@NotNull EvokeParser.CurrentTimeContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#subtract}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubtract(@NotNull EvokeParser.SubtractContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#dataType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDataType(@NotNull EvokeParser.DataTypeContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#duration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDuration(@NotNull EvokeParser.DurationContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#findInString}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFindInString(@NotNull EvokeParser.FindInStringContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#isNotEqual}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsNotEqual(@NotNull EvokeParser.IsNotEqualContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#relTimeVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelTimeVal(@NotNull EvokeParser.RelTimeValContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#increase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIncrease(@NotNull EvokeParser.IncreaseContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#raiseToPower}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRaiseToPower(@NotNull EvokeParser.RaiseToPowerContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#timeFunc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimeFunc(@NotNull EvokeParser.TimeFuncContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#latest}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLatest(@NotNull EvokeParser.LatestContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#isWithinPast}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsWithinPast(@NotNull EvokeParser.IsWithinPastContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#first}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFirst(@NotNull EvokeParser.FirstContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#delEventTrigger}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDelEventTrigger(@NotNull EvokeParser.DelEventTriggerContext ctx);

	/**
	 * Visit a parse tree produced by {@link EvokeParser#average}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAverage(@NotNull EvokeParser.AverageContext ctx);
}