// Generated from Logic.g4 by ANTLR 4.1
package edu.uvm.ccts.arden.logic.antlr;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link LogicParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface LogicVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link LogicParser#durationExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDurationExpr(@NotNull LogicParser.DurationExprContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#arccos}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArccos(@NotNull LogicParser.ArccosContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#no}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNo(@NotNull LogicParser.NoContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#asString}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsString(@NotNull LogicParser.AsStringContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#firstFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFirstFrom(@NotNull LogicParser.FirstFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#dot}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDot(@NotNull LogicParser.DotContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#temporalUnit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTemporalUnit(@NotNull LogicParser.TemporalUnitContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#minimumFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMinimumFrom(@NotNull LogicParser.MinimumFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#indexType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexType(@NotNull LogicParser.IndexTypeContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#matches}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMatches(@NotNull LogicParser.MatchesContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#durationUnit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDurationUnit(@NotNull LogicParser.DurationUnitContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#abs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAbs(@NotNull LogicParser.AbsContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#isIn}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsIn(@NotNull LogicParser.IsInContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#indexOfFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexOfFrom(@NotNull LogicParser.IndexOfFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#substring}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubstring(@NotNull LogicParser.SubstringContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#forLoop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForLoop(@NotNull LogicParser.ForLoopContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#element}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElement(@NotNull LogicParser.ElementContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#clone}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClone(@NotNull LogicParser.CloneContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#occurEqual}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurEqual(@NotNull LogicParser.OccurEqualContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#isWithinFollowing}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsWithinFollowing(@NotNull LogicParser.IsWithinFollowingContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#unaryMinus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryMinus(@NotNull LogicParser.UnaryMinusContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#or}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOr(@NotNull LogicParser.OrContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#unaryList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryList(@NotNull LogicParser.UnaryListContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#parens}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParens(@NotNull LogicParser.ParensContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#log}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLog(@NotNull LogicParser.LogContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#atMostOrLeast}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtMostOrLeast(@NotNull LogicParser.AtMostOrLeastContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#extract}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExtract(@NotNull LogicParser.ExtractContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#timeOfDayVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimeOfDayVal(@NotNull LogicParser.TimeOfDayValContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#not}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNot(@NotNull LogicParser.NotContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#median}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMedian(@NotNull LogicParser.MedianContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#floor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFloor(@NotNull LogicParser.FloorContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#now}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNow(@NotNull LogicParser.NowContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#truncate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTruncate(@NotNull LogicParser.TruncateContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#isLessThanOrEqual}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsLessThanOrEqual(@NotNull LogicParser.IsLessThanOrEqualContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#nullVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNullVal(@NotNull LogicParser.NullValContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#occurBefore}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurBefore(@NotNull LogicParser.OccurBeforeContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#objOrIndexRule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObjOrIndexRule(@NotNull LogicParser.ObjOrIndexRuleContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#whereTimeIsPresent}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhereTimeIsPresent(@NotNull LogicParser.WhereTimeIsPresentContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#sort}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSort(@NotNull LogicParser.SortContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#interval}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInterval(@NotNull LogicParser.IntervalContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#minimum}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMinimum(@NotNull LogicParser.MinimumContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#before}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBefore(@NotNull LogicParser.BeforeContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#merge}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMerge(@NotNull LogicParser.MergeContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(@NotNull LogicParser.BlockContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#isLessThan}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsLessThan(@NotNull LogicParser.IsLessThanContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#isBefore}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsBefore(@NotNull LogicParser.IsBeforeContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#unaryPlus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryPlus(@NotNull LogicParser.UnaryPlusContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#length}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLength(@NotNull LogicParser.LengthContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#addToList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAddToList(@NotNull LogicParser.AddToListContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#atTime}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtTime(@NotNull LogicParser.AtTimeContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#occurWithinFollowing}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurWithinFollowing(@NotNull LogicParser.OccurWithinFollowingContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#earliestFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEarliestFrom(@NotNull LogicParser.EarliestFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#occurWithinPreceding}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurWithinPreceding(@NotNull LogicParser.OccurWithinPrecedingContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#multipleAssignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultipleAssignment(@NotNull LogicParser.MultipleAssignmentContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#reverse}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReverse(@NotNull LogicParser.ReverseContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#isDataType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsDataType(@NotNull LogicParser.IsDataTypeContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#arctan}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArctan(@NotNull LogicParser.ArctanContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#sqrt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSqrt(@NotNull LogicParser.SqrtContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#occurWithinPast}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurWithinPast(@NotNull LogicParser.OccurWithinPastContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#asNumber}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsNumber(@NotNull LogicParser.AsNumberContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#divide}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDivide(@NotNull LogicParser.DivideContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#stringVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringVal(@NotNull LogicParser.StringValContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#replace}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReplace(@NotNull LogicParser.ReplaceContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#arcsin}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArcsin(@NotNull LogicParser.ArcsinContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#isWithinTo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsWithinTo(@NotNull LogicParser.IsWithinToContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment(@NotNull LogicParser.AssignmentContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#switchStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwitchStatement(@NotNull LogicParser.SwitchStatementContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#maximumFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMaximumFrom(@NotNull LogicParser.MaximumFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#isWithinSurrounding}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsWithinSurrounding(@NotNull LogicParser.IsWithinSurroundingContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#isObjectType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsObjectType(@NotNull LogicParser.IsObjectTypeContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#whileLoop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileLoop(@NotNull LogicParser.WhileLoopContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#ago}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAgo(@NotNull LogicParser.AgoContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#decrease}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDecrease(@NotNull LogicParser.DecreaseContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#stdDev}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStdDev(@NotNull LogicParser.StdDevContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#extractChars}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExtractChars(@NotNull LogicParser.ExtractCharsContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#last}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLast(@NotNull LogicParser.LastContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#count}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCount(@NotNull LogicParser.CountContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#numberVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumberVal(@NotNull LogicParser.NumberValContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#booleanVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBooleanVal(@NotNull LogicParser.BooleanValContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#round}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRound(@NotNull LogicParser.RoundContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#where}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhere(@NotNull LogicParser.WhereContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#isWithinPreceding}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsWithinPreceding(@NotNull LogicParser.IsWithinPrecedingContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#lastFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLastFrom(@NotNull LogicParser.LastFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#binaryList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinaryList(@NotNull LogicParser.BinaryListContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#isAfter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsAfter(@NotNull LogicParser.IsAfterContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#after}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAfter(@NotNull LogicParser.AfterContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#buildString}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuildString(@NotNull LogicParser.BuildStringContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#uppercase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUppercase(@NotNull LogicParser.UppercaseContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#occurWithinTo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurWithinTo(@NotNull LogicParser.OccurWithinToContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#earliest}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEarliest(@NotNull LogicParser.EarliestContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#occurWithinSameDay}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurWithinSameDay(@NotNull LogicParser.OccurWithinSameDayContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#dayOfWeekFunc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDayOfWeekFunc(@NotNull LogicParser.DayOfWeekFuncContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#any}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAny(@NotNull LogicParser.AnyContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#isWithinSameDay}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsWithinSameDay(@NotNull LogicParser.IsWithinSameDayContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#subList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubList(@NotNull LogicParser.SubListContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#objNamedWith}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObjNamedWith(@NotNull LogicParser.ObjNamedWithContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#conclude}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConclude(@NotNull LogicParser.ConcludeContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#index}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndex(@NotNull LogicParser.IndexContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#assignable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignable(@NotNull LogicParser.AssignableContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#continueLoop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitContinueLoop(@NotNull LogicParser.ContinueLoopContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#init}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInit(@NotNull LogicParser.InitContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#latestFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLatestFrom(@NotNull LogicParser.LatestFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#indexFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexFrom(@NotNull LogicParser.IndexFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#and}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnd(@NotNull LogicParser.AndContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#isGreaterThanOrEqual}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsGreaterThanOrEqual(@NotNull LogicParser.IsGreaterThanOrEqualContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#extractAttrNames}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExtractAttrNames(@NotNull LogicParser.ExtractAttrNamesContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExp(@NotNull LogicParser.ExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#breakLoop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBreakLoop(@NotNull LogicParser.BreakLoopContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#sine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSine(@NotNull LogicParser.SineContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#ceiling}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCeiling(@NotNull LogicParser.CeilingContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#isEqual}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsEqual(@NotNull LogicParser.IsEqualContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#maximum}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMaximum(@NotNull LogicParser.MaximumContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#asTime}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsTime(@NotNull LogicParser.AsTimeContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#tangent}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTangent(@NotNull LogicParser.TangentContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#enhancedAssignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnhancedAssignment(@NotNull LogicParser.EnhancedAssignmentContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#isGreaterThan}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsGreaterThan(@NotNull LogicParser.IsGreaterThanContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#call}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCall(@NotNull LogicParser.CallContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#occurAfter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurAfter(@NotNull LogicParser.OccurAfterContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#sum}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSum(@NotNull LogicParser.SumContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#add}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdd(@NotNull LogicParser.AddContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#seqto}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSeqto(@NotNull LogicParser.SeqtoContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#timeOfDayFunc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimeOfDayFunc(@NotNull LogicParser.TimeOfDayFuncContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#occurWithinSurrounding}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurWithinSurrounding(@NotNull LogicParser.OccurWithinSurroundingContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#id}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitId(@NotNull LogicParser.IdContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#newObject}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNewObject(@NotNull LogicParser.NewObjectContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#attributeFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAttributeFrom(@NotNull LogicParser.AttributeFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#lowercase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLowercase(@NotNull LogicParser.LowercaseContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#objOrderedWith}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObjOrderedWith(@NotNull LogicParser.ObjOrderedWithContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#timeVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimeVal(@NotNull LogicParser.TimeValContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#trim}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTrim(@NotNull LogicParser.TrimContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#exist}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExist(@NotNull LogicParser.ExistContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#dayOfWeek}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDayOfWeek(@NotNull LogicParser.DayOfWeekContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#all}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAll(@NotNull LogicParser.AllContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#cosine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCosine(@NotNull LogicParser.CosineContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt(@NotNull LogicParser.StmtContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#removeFromList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRemoveFromList(@NotNull LogicParser.RemoveFromListContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#multiply}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiply(@NotNull LogicParser.MultiplyContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#variance}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariance(@NotNull LogicParser.VarianceContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#concat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConcat(@NotNull LogicParser.ConcatContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#nearest}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNearest(@NotNull LogicParser.NearestContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#emptyList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEmptyList(@NotNull LogicParser.EmptyListContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#print}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrint(@NotNull LogicParser.PrintContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#log10}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLog10(@NotNull LogicParser.Log10Context ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#ifStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfStatement(@NotNull LogicParser.IfStatementContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#currentTime}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCurrentTime(@NotNull LogicParser.CurrentTimeContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#subtract}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubtract(@NotNull LogicParser.SubtractContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#dataType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDataType(@NotNull LogicParser.DataTypeContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#duration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDuration(@NotNull LogicParser.DurationContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#findInString}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFindInString(@NotNull LogicParser.FindInStringContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#isNotEqual}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsNotEqual(@NotNull LogicParser.IsNotEqualContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#increase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIncrease(@NotNull LogicParser.IncreaseContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#raiseToPower}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRaiseToPower(@NotNull LogicParser.RaiseToPowerContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#timeFunc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimeFunc(@NotNull LogicParser.TimeFuncContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#latest}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLatest(@NotNull LogicParser.LatestContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#isWithinPast}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsWithinPast(@NotNull LogicParser.IsWithinPastContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#first}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFirst(@NotNull LogicParser.FirstContext ctx);

	/**
	 * Visit a parse tree produced by {@link LogicParser#average}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAverage(@NotNull LogicParser.AverageContext ctx);
}