// Generated from Logic.g4 by ANTLR 4.1
package edu.uvm.ccts.arden.logic.antlr;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class LogicParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__7=1, T__6=2, T__5=3, T__4=4, T__3=5, T__2=6, T__1=7, T__0=8, Conclude=9, 
		If=10, Then=11, ElsIf=12, Else=13, EndIf=14, While=15, Do=16, EndDo=17, 
		Switch=18, Case=19, Default=20, EndSwitch=21, BreakLoop=22, Continue=23, 
		And=24, Or=25, SeqTo=26, AsNumber=27, AsTime=28, AsString=29, Plus=30, 
		Minus=31, Mul=32, Div=33, Pow=34, Arccos=35, Arcsin=36, Arctan=37, Cosine=38, 
		Sine=39, Tangent=40, Exp=41, Log=42, Log10=43, Int=44, Floor=45, Ceiling=46, 
		Truncate=47, Round=48, Abs=49, Sqrt=50, LessThan=51, LessThanOrEqual=52, 
		EqualTo=53, NotEqualTo=54, GreaterThan=55, GreaterThanOrEqual=56, Min=57, 
		Max=58, Earliest=59, Latest=60, Nearest=61, Least=62, Most=63, First=64, 
		Last=65, Count=66, Interval=67, Exist=68, Avg=69, Median=70, Sum=71, Stddev=72, 
		Variance=73, Index=74, Any=75, All=76, No=77, Of=78, IsTrue=79, AreTrue=80, 
		Ago=81, AtTime=82, Time=83, Is=84, String=85, MatchesPattern=86, Length=87, 
		Uppercase=88, Lowercase=89, Trim=90, Left=91, Right=92, Find=93, StartingAt=94, 
		Substring=95, Characters=96, Add=97, Elements=98, Percent=99, Increase=100, 
		Decrease=101, List=102, Sublist=103, Remove=104, Merge=105, WhereTimePresent=106, 
		Sort=107, Data=108, Year=109, Month=110, Week=111, Day=112, Hour=113, 
		Minute=114, Second=115, Years=116, Months=117, Weeks=118, Days=119, Hours=120, 
		Minutes=121, Seconds=122, New=123, Clone=124, Attribute=125, Names=126, 
		Extract=127, Duration=128, Null=129, Boolean=130, BooleanVal=131, Replace=132, 
		Present=133, TimeOfDay=134, Reverse=135, EmptyList=136, Concat=137, Number=138, 
		NumberVal=139, IntVal=140, Now=141, CurrentTime=142, TimeVal=143, TimeOfDayFunc=144, 
		TimeOfDayVal=145, DayOfWeekFunc=146, DayOfWeek=147, Print=148, For=149, 
		Let=150, Be=151, As=152, Where=153, Within=154, The=155, To=156, Preceding=157, 
		Following=158, Surrounding=159, Past=160, SameDayAs=161, Before=162, After=163, 
		Occur=164, Not=165, In=166, At=167, From=168, Call=169, With=170, Equal=171, 
		Object=172, StringVal=173, ID=174, S=175, LINE_COMMENT=176, COMMENT=177, 
		WS=178;
	public static final String[] tokenNames = {
		"<INVALID>", "']'", "')'", "'.'", "','", "':='", "'['", "'('", "';'", 
		"Conclude", "If", "Then", "ElsIf", "Else", "EndIf", "While", "Do", "EndDo", 
		"Switch", "Case", "Default", "EndSwitch", "BreakLoop", "Continue", "And", 
		"Or", "SeqTo", "AsNumber", "AsTime", "AsString", "'+'", "'-'", "'*'", 
		"'/'", "'**'", "Arccos", "Arcsin", "Arctan", "Cosine", "Sine", "Tangent", 
		"Exp", "Log", "Log10", "Int", "Floor", "Ceiling", "Truncate", "Round", 
		"Abs", "Sqrt", "LessThan", "LessThanOrEqual", "EqualTo", "NotEqualTo", 
		"GreaterThan", "GreaterThanOrEqual", "Min", "Max", "Earliest", "Latest", 
		"Nearest", "Least", "Most", "First", "Last", "Count", "Interval", "Exist", 
		"Avg", "Median", "Sum", "Stddev", "Variance", "Index", "Any", "All", "No", 
		"Of", "IsTrue", "AreTrue", "Ago", "AtTime", "Time", "Is", "String", "MatchesPattern", 
		"Length", "Uppercase", "Lowercase", "Trim", "Left", "Right", "Find", "StartingAt", 
		"Substring", "Characters", "Add", "Elements", "Percent", "Increase", "Decrease", 
		"List", "Sublist", "Remove", "Merge", "WhereTimePresent", "Sort", "Data", 
		"Year", "Month", "Week", "Day", "Hour", "Minute", "Second", "Years", "Months", 
		"Weeks", "Days", "Hours", "Minutes", "Seconds", "New", "Clone", "Attribute", 
		"Names", "Extract", "Duration", "Null", "Boolean", "BooleanVal", "Replace", 
		"Present", "TimeOfDay", "Reverse", "EmptyList", "'||'", "Number", "NumberVal", 
		"IntVal", "Now", "CurrentTime", "TimeVal", "TimeOfDayFunc", "TimeOfDayVal", 
		"DayOfWeekFunc", "DayOfWeek", "Print", "For", "Let", "Be", "As", "Where", 
		"Within", "The", "To", "Preceding", "Following", "Surrounding", "Past", 
		"SameDayAs", "Before", "After", "Occur", "Not", "In", "At", "From", "Call", 
		"With", "Equal", "Object", "StringVal", "ID", "S", "LINE_COMMENT", "COMMENT", 
		"WS"
	};
	public static final int
		RULE_block = 0, RULE_ifStatement = 1, RULE_switchStatement = 2, RULE_whileLoop = 3, 
		RULE_forLoop = 4, RULE_breakLoop = 5, RULE_continueLoop = 6, RULE_init = 7, 
		RULE_print = 8, RULE_stmt = 9, RULE_assignmentStatement = 10, RULE_objOrIndexRule = 11, 
		RULE_assignable = 12, RULE_call = 13, RULE_conclude = 14, RULE_indexType = 15, 
		RULE_dataType = 16, RULE_durationUnit = 17, RULE_durationExpr = 18, RULE_temporalUnit = 19, 
		RULE_expr = 20, RULE_objOrderedWith = 21, RULE_objNamedWith = 22;
	public static final String[] ruleNames = {
		"block", "ifStatement", "switchStatement", "whileLoop", "forLoop", "breakLoop", 
		"continueLoop", "init", "print", "stmt", "assignmentStatement", "objOrIndexRule", 
		"assignable", "call", "conclude", "indexType", "dataType", "durationUnit", 
		"durationExpr", "temporalUnit", "expr", "objOrderedWith", "objNamedWith"
	};

	@Override
	public String getGrammarFileName() { return "Logic.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public ATN getATN() { return _ATN; }


	  private static final List<Boolean> stack = new ArrayList<Boolean>();
	  private static void push(Boolean b) { stack.add(0, b); }
	  private static void pop()           { if (stack.size() > 0) stack.remove(0); }
	  private static Boolean list()       { return stack.size() > 0 ? stack.get(0) : true; }

	public LogicParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class BlockContext extends ParserRuleContext {
		public StmtContext stmt(int i) {
			return getRuleContext(StmtContext.class,i);
		}
		public List<StmtContext> stmt() {
			return getRuleContexts(StmtContext.class);
		}
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitBlock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_block);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(49);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 7) | (1L << Conclude) | (1L << If) | (1L << While) | (1L << Switch) | (1L << BreakLoop) | (1L << Continue))) != 0) || ((((_la - 148)) & ~0x3f) == 0 && ((1L << (_la - 148)) & ((1L << (Print - 148)) | (1L << (For - 148)) | (1L << (Let - 148)) | (1L << (Call - 148)) | (1L << (ID - 148)))) != 0)) {
				{
				{
				setState(46); stmt();
				}
				}
				setState(51);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfStatementContext extends ParserRuleContext {
		public TerminalNode Then(int i) {
			return getToken(LogicParser.Then, i);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode Else() { return getToken(LogicParser.Else, 0); }
		public TerminalNode EndIf() { return getToken(LogicParser.EndIf, 0); }
		public BlockContext block(int i) {
			return getRuleContext(BlockContext.class,i);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode If() { return getToken(LogicParser.If, 0); }
		public TerminalNode ElsIf(int i) {
			return getToken(LogicParser.ElsIf, i);
		}
		public List<BlockContext> block() {
			return getRuleContexts(BlockContext.class);
		}
		public List<TerminalNode> ElsIf() { return getTokens(LogicParser.ElsIf); }
		public List<TerminalNode> Then() { return getTokens(LogicParser.Then); }
		public IfStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifStatement; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitIfStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IfStatementContext ifStatement() throws RecognitionException {
		IfStatementContext _localctx = new IfStatementContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_ifStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(52); match(If);
			setState(53); expr(0);
			setState(54); match(Then);
			setState(55); block();
			setState(63);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ElsIf) {
				{
				{
				setState(56); match(ElsIf);
				setState(57); expr(0);
				setState(58); match(Then);
				setState(59); block();
				}
				}
				setState(65);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(68);
			_la = _input.LA(1);
			if (_la==Else) {
				{
				setState(66); match(Else);
				setState(67); block();
				}
			}

			setState(70); match(EndIf);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SwitchStatementContext extends ParserRuleContext {
		public TerminalNode EndSwitch() { return getToken(LogicParser.EndSwitch, 0); }
		public TerminalNode Default() { return getToken(LogicParser.Default, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public List<TerminalNode> Case() { return getTokens(LogicParser.Case); }
		public BlockContext block(int i) {
			return getRuleContext(BlockContext.class,i);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode ID() { return getToken(LogicParser.ID, 0); }
		public TerminalNode Switch() { return getToken(LogicParser.Switch, 0); }
		public List<BlockContext> block() {
			return getRuleContexts(BlockContext.class);
		}
		public TerminalNode Case(int i) {
			return getToken(LogicParser.Case, i);
		}
		public SwitchStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_switchStatement; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitSwitchStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SwitchStatementContext switchStatement() throws RecognitionException {
		SwitchStatementContext _localctx = new SwitchStatementContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_switchStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(72); match(Switch);
			setState(73); match(ID);
			setState(78); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(74); match(Case);
				setState(75); expr(0);
				setState(76); block();
				}
				}
				setState(80); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==Case );
			setState(84);
			_la = _input.LA(1);
			if (_la==Default) {
				{
				setState(82); match(Default);
				setState(83); block();
				}
			}

			setState(86); match(EndSwitch);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhileLoopContext extends ParserRuleContext {
		public TerminalNode While() { return getToken(LogicParser.While, 0); }
		public TerminalNode Do() { return getToken(LogicParser.Do, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode EndDo() { return getToken(LogicParser.EndDo, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public WhileLoopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whileLoop; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitWhileLoop(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WhileLoopContext whileLoop() throws RecognitionException {
		WhileLoopContext _localctx = new WhileLoopContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_whileLoop);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(88); match(While);
			setState(89); expr(0);
			setState(90); match(Do);
			setState(91); block();
			setState(92); match(EndDo);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForLoopContext extends ParserRuleContext {
		public TerminalNode For() { return getToken(LogicParser.For, 0); }
		public TerminalNode Do() { return getToken(LogicParser.Do, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode ID() { return getToken(LogicParser.ID, 0); }
		public TerminalNode EndDo() { return getToken(LogicParser.EndDo, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public TerminalNode In() { return getToken(LogicParser.In, 0); }
		public ForLoopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forLoop; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitForLoop(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ForLoopContext forLoop() throws RecognitionException {
		ForLoopContext _localctx = new ForLoopContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_forLoop);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(94); match(For);
			setState(95); match(ID);
			setState(96); match(In);
			setState(97); expr(0);
			setState(98); match(Do);
			setState(99); block();
			setState(100); match(EndDo);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BreakLoopContext extends ParserRuleContext {
		public TerminalNode BreakLoop() { return getToken(LogicParser.BreakLoop, 0); }
		public BreakLoopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_breakLoop; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitBreakLoop(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BreakLoopContext breakLoop() throws RecognitionException {
		BreakLoopContext _localctx = new BreakLoopContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_breakLoop);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(102); match(BreakLoop);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ContinueLoopContext extends ParserRuleContext {
		public TerminalNode Continue() { return getToken(LogicParser.Continue, 0); }
		public ContinueLoopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_continueLoop; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitContinueLoop(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ContinueLoopContext continueLoop() throws RecognitionException {
		ContinueLoopContext _localctx = new ContinueLoopContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_continueLoop);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(104); match(Continue);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InitContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(LogicParser.EOF, 0); }
		public StmtContext stmt(int i) {
			return getRuleContext(StmtContext.class,i);
		}
		public List<StmtContext> stmt() {
			return getRuleContexts(StmtContext.class);
		}
		public InitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_init; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitInit(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InitContext init() throws RecognitionException {
		InitContext _localctx = new InitContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_init);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(109);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 7) | (1L << Conclude) | (1L << If) | (1L << While) | (1L << Switch) | (1L << BreakLoop) | (1L << Continue))) != 0) || ((((_la - 148)) & ~0x3f) == 0 && ((1L << (_la - 148)) & ((1L << (Print - 148)) | (1L << (For - 148)) | (1L << (Let - 148)) | (1L << (Call - 148)) | (1L << (ID - 148)))) != 0)) {
				{
				{
				setState(106); stmt();
				}
				}
				setState(111);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(112); match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrintContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Print() { return getToken(LogicParser.Print, 0); }
		public PrintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_print; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitPrint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrintContext print() throws RecognitionException {
		PrintContext _localctx = new PrintContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_print);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(114); match(Print);
			setState(115); expr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StmtContext extends ParserRuleContext {
		public ContinueLoopContext continueLoop() {
			return getRuleContext(ContinueLoopContext.class,0);
		}
		public AssignmentStatementContext assignmentStatement() {
			return getRuleContext(AssignmentStatementContext.class,0);
		}
		public PrintContext print() {
			return getRuleContext(PrintContext.class,0);
		}
		public WhileLoopContext whileLoop() {
			return getRuleContext(WhileLoopContext.class,0);
		}
		public IfStatementContext ifStatement() {
			return getRuleContext(IfStatementContext.class,0);
		}
		public ForLoopContext forLoop() {
			return getRuleContext(ForLoopContext.class,0);
		}
		public BreakLoopContext breakLoop() {
			return getRuleContext(BreakLoopContext.class,0);
		}
		public CallContext call() {
			return getRuleContext(CallContext.class,0);
		}
		public ConcludeContext conclude() {
			return getRuleContext(ConcludeContext.class,0);
		}
		public SwitchStatementContext switchStatement() {
			return getRuleContext(SwitchStatementContext.class,0);
		}
		public StmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stmt; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitStmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StmtContext stmt() throws RecognitionException {
		StmtContext _localctx = new StmtContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_stmt);
		try {
			setState(147);
			switch (_input.LA(1)) {
			case 7:
			case Let:
			case ID:
				enterOuterAlt(_localctx, 1);
				{
				setState(117); assignmentStatement();
				setState(118); match(8);
				}
				break;
			case If:
				enterOuterAlt(_localctx, 2);
				{
				setState(120); ifStatement();
				setState(121); match(8);
				}
				break;
			case Switch:
				enterOuterAlt(_localctx, 3);
				{
				setState(123); switchStatement();
				setState(124); match(8);
				}
				break;
			case While:
				enterOuterAlt(_localctx, 4);
				{
				setState(126); whileLoop();
				setState(127); match(8);
				}
				break;
			case For:
				enterOuterAlt(_localctx, 5);
				{
				setState(129); forLoop();
				setState(130); match(8);
				}
				break;
			case BreakLoop:
				enterOuterAlt(_localctx, 6);
				{
				setState(132); breakLoop();
				setState(133); match(8);
				}
				break;
			case Continue:
				enterOuterAlt(_localctx, 7);
				{
				setState(135); continueLoop();
				setState(136); match(8);
				}
				break;
			case Conclude:
				enterOuterAlt(_localctx, 8);
				{
				setState(138); conclude();
				setState(139); match(8);
				}
				break;
			case Call:
				enterOuterAlt(_localctx, 9);
				{
				setState(141); call();
				setState(142); match(8);
				}
				break;
			case Print:
				enterOuterAlt(_localctx, 10);
				{
				setState(144); print();
				setState(145); match(8);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignmentStatementContext extends ParserRuleContext {
		public AssignmentStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignmentStatement; }
	 
		public AssignmentStatementContext() { }
		public void copyFrom(AssignmentStatementContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AssignmentContext extends AssignmentStatementContext {
		public AssignableContext assignable() {
			return getRuleContext(AssignableContext.class,0);
		}
		public TerminalNode ID() { return getToken(LogicParser.ID, 0); }
		public TerminalNode Let() { return getToken(LogicParser.Let, 0); }
		public TerminalNode Be() { return getToken(LogicParser.Be, 0); }
		public AssignmentContext(AssignmentStatementContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitAssignment(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MultipleAssignmentContext extends AssignmentStatementContext {
		public AssignableContext assignable() {
			return getRuleContext(AssignableContext.class,0);
		}
		public List<TerminalNode> ID() { return getTokens(LogicParser.ID); }
		public TerminalNode Let() { return getToken(LogicParser.Let, 0); }
		public TerminalNode Be() { return getToken(LogicParser.Be, 0); }
		public TerminalNode ID(int i) {
			return getToken(LogicParser.ID, i);
		}
		public MultipleAssignmentContext(AssignmentStatementContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitMultipleAssignment(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EnhancedAssignmentContext extends AssignmentStatementContext {
		public AssignableContext assignable() {
			return getRuleContext(AssignableContext.class,0);
		}
		public TerminalNode ID() { return getToken(LogicParser.ID, 0); }
		public List<ObjOrIndexRuleContext> objOrIndexRule() {
			return getRuleContexts(ObjOrIndexRuleContext.class);
		}
		public ObjOrIndexRuleContext objOrIndexRule(int i) {
			return getRuleContext(ObjOrIndexRuleContext.class,i);
		}
		public EnhancedAssignmentContext(AssignmentStatementContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitEnhancedAssignment(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AssignmentStatementContext assignmentStatement() throws RecognitionException {
		AssignmentStatementContext _localctx = new AssignmentStatementContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_assignmentStatement);
		int _la;
		try {
			setState(190);
			switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
			case 1:
				_localctx = new AssignmentContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(149); match(ID);
				setState(150); match(5);
				setState(151); assignable();
				}
				break;

			case 2:
				_localctx = new AssignmentContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(152); match(Let);
				setState(153); match(ID);
				setState(154); match(Be);
				setState(155); assignable();
				}
				break;

			case 3:
				_localctx = new MultipleAssignmentContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(156); match(7);
				setState(157); match(ID);
				setState(162);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==4) {
					{
					{
					setState(158); match(4);
					setState(159); match(ID);
					}
					}
					setState(164);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(165); match(2);
				setState(166); match(5);
				setState(167); assignable();
				}
				break;

			case 4:
				_localctx = new MultipleAssignmentContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(168); match(Let);
				setState(169); match(7);
				setState(170); match(ID);
				setState(175);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==4) {
					{
					{
					setState(171); match(4);
					setState(172); match(ID);
					}
					}
					setState(177);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(178); match(2);
				setState(179); match(Be);
				setState(180); assignable();
				}
				break;

			case 5:
				_localctx = new EnhancedAssignmentContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(181); match(ID);
				setState(183); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(182); objOrIndexRule();
					}
					}
					setState(185); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==3 || _la==6 );
				setState(187); match(5);
				setState(188); assignable();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ObjOrIndexRuleContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode ID() { return getToken(LogicParser.ID, 0); }
		public ObjOrIndexRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_objOrIndexRule; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitObjOrIndexRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ObjOrIndexRuleContext objOrIndexRule() throws RecognitionException {
		ObjOrIndexRuleContext _localctx = new ObjOrIndexRuleContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_objOrIndexRule);
		try {
			setState(198);
			switch (_input.LA(1)) {
			case 3:
				enterOuterAlt(_localctx, 1);
				{
				setState(192); match(3);
				setState(193); match(ID);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 2);
				{
				setState(194); match(6);
				setState(195); expr(0);
				setState(196); match(1);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignableContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public CallContext call() {
			return getRuleContext(CallContext.class,0);
		}
		public AssignableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignable; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitAssignable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AssignableContext assignable() throws RecognitionException {
		AssignableContext _localctx = new AssignableContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_assignable);
		try {
			setState(202);
			switch (_input.LA(1)) {
			case 4:
			case 7:
			case Plus:
			case Minus:
			case Arccos:
			case Arcsin:
			case Arctan:
			case Cosine:
			case Sine:
			case Tangent:
			case Exp:
			case Log:
			case Log10:
			case Int:
			case Floor:
			case Ceiling:
			case Truncate:
			case Round:
			case Abs:
			case Sqrt:
			case Min:
			case Max:
			case Earliest:
			case Latest:
			case Nearest:
			case First:
			case Last:
			case Count:
			case Interval:
			case Exist:
			case Avg:
			case Median:
			case Sum:
			case Stddev:
			case Variance:
			case Index:
			case Any:
			case All:
			case No:
			case Time:
			case String:
			case Length:
			case Uppercase:
			case Lowercase:
			case Trim:
			case Find:
			case Substring:
			case Add:
			case Percent:
			case Increase:
			case Decrease:
			case Sublist:
			case Remove:
			case Sort:
			case New:
			case Clone:
			case Attribute:
			case Extract:
			case Null:
			case BooleanVal:
			case Replace:
			case Reverse:
			case EmptyList:
			case NumberVal:
			case Now:
			case CurrentTime:
			case TimeVal:
			case TimeOfDayFunc:
			case TimeOfDayVal:
			case DayOfWeekFunc:
			case DayOfWeek:
			case The:
			case Not:
			case At:
			case StringVal:
			case ID:
				enterOuterAlt(_localctx, 1);
				{
				setState(200); expr(0);
				}
				break;
			case Call:
				enterOuterAlt(_localctx, 2);
				{
				setState(201); call();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CallContext extends ParserRuleContext {
		public TerminalNode With() { return getToken(LogicParser.With, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode Call() { return getToken(LogicParser.Call, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode ID() { return getToken(LogicParser.ID, 0); }
		public CallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_call; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitCall(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CallContext call() throws RecognitionException {
		CallContext _localctx = new CallContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_call);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(204); match(Call);
			setState(205); match(ID);
			setState(215);
			_la = _input.LA(1);
			if (_la==With) {
				{
				setState(206); match(With);
				setState(207); expr(0);
				setState(212);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==4) {
					{
					{
					setState(208); match(4);
					setState(209); expr(0);
					}
					}
					setState(214);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConcludeContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Conclude() { return getToken(LogicParser.Conclude, 0); }
		public ConcludeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_conclude; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitConclude(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConcludeContext conclude() throws RecognitionException {
		ConcludeContext _localctx = new ConcludeContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_conclude);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(217); match(Conclude);
			setState(218); expr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IndexTypeContext extends ParserRuleContext {
		public TerminalNode Latest() { return getToken(LogicParser.Latest, 0); }
		public TerminalNode Earliest() { return getToken(LogicParser.Earliest, 0); }
		public TerminalNode Max() { return getToken(LogicParser.Max, 0); }
		public TerminalNode Min() { return getToken(LogicParser.Min, 0); }
		public IndexTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_indexType; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitIndexType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IndexTypeContext indexType() throws RecognitionException {
		IndexTypeContext _localctx = new IndexTypeContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_indexType);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(220);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Min) | (1L << Max) | (1L << Earliest) | (1L << Latest))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DataTypeContext extends ParserRuleContext {
		public TerminalNode String() { return getToken(LogicParser.String, 0); }
		public TerminalNode Time() { return getToken(LogicParser.Time, 0); }
		public TerminalNode Present() { return getToken(LogicParser.Present, 0); }
		public TerminalNode Number() { return getToken(LogicParser.Number, 0); }
		public TerminalNode Object() { return getToken(LogicParser.Object, 0); }
		public TerminalNode List() { return getToken(LogicParser.List, 0); }
		public TerminalNode Duration() { return getToken(LogicParser.Duration, 0); }
		public TerminalNode Null() { return getToken(LogicParser.Null, 0); }
		public TerminalNode TimeOfDay() { return getToken(LogicParser.TimeOfDay, 0); }
		public TerminalNode Boolean() { return getToken(LogicParser.Boolean, 0); }
		public DataTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dataType; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitDataType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DataTypeContext dataType() throws RecognitionException {
		DataTypeContext _localctx = new DataTypeContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_dataType);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(222);
			_la = _input.LA(1);
			if ( !(((((_la - 83)) & ~0x3f) == 0 && ((1L << (_la - 83)) & ((1L << (Time - 83)) | (1L << (String - 83)) | (1L << (List - 83)) | (1L << (Duration - 83)) | (1L << (Null - 83)) | (1L << (Boolean - 83)) | (1L << (Present - 83)) | (1L << (TimeOfDay - 83)) | (1L << (Number - 83)))) != 0) || _la==Object) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DurationUnitContext extends ParserRuleContext {
		public TerminalNode Minute() { return getToken(LogicParser.Minute, 0); }
		public TerminalNode Days() { return getToken(LogicParser.Days, 0); }
		public TerminalNode Second() { return getToken(LogicParser.Second, 0); }
		public TerminalNode Seconds() { return getToken(LogicParser.Seconds, 0); }
		public TerminalNode Weeks() { return getToken(LogicParser.Weeks, 0); }
		public TerminalNode Week() { return getToken(LogicParser.Week, 0); }
		public TerminalNode Minutes() { return getToken(LogicParser.Minutes, 0); }
		public TerminalNode Month() { return getToken(LogicParser.Month, 0); }
		public TerminalNode Hours() { return getToken(LogicParser.Hours, 0); }
		public TerminalNode Year() { return getToken(LogicParser.Year, 0); }
		public TerminalNode Years() { return getToken(LogicParser.Years, 0); }
		public TerminalNode Day() { return getToken(LogicParser.Day, 0); }
		public TerminalNode Months() { return getToken(LogicParser.Months, 0); }
		public TerminalNode Hour() { return getToken(LogicParser.Hour, 0); }
		public DurationUnitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_durationUnit; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitDurationUnit(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DurationUnitContext durationUnit() throws RecognitionException {
		DurationUnitContext _localctx = new DurationUnitContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_durationUnit);
		int _la;
		try {
			setState(231);
			switch (_input.LA(1)) {
			case Year:
			case Years:
				enterOuterAlt(_localctx, 1);
				{
				setState(224);
				_la = _input.LA(1);
				if ( !(_la==Year || _la==Years) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				}
				break;
			case Month:
			case Months:
				enterOuterAlt(_localctx, 2);
				{
				setState(225);
				_la = _input.LA(1);
				if ( !(_la==Month || _la==Months) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				}
				break;
			case Week:
			case Weeks:
				enterOuterAlt(_localctx, 3);
				{
				setState(226);
				_la = _input.LA(1);
				if ( !(_la==Week || _la==Weeks) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				}
				break;
			case Day:
			case Days:
				enterOuterAlt(_localctx, 4);
				{
				setState(227);
				_la = _input.LA(1);
				if ( !(_la==Day || _la==Days) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				}
				break;
			case Hour:
			case Hours:
				enterOuterAlt(_localctx, 5);
				{
				setState(228);
				_la = _input.LA(1);
				if ( !(_la==Hour || _la==Hours) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				}
				break;
			case Minute:
			case Minutes:
				enterOuterAlt(_localctx, 6);
				{
				setState(229);
				_la = _input.LA(1);
				if ( !(_la==Minute || _la==Minutes) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				}
				break;
			case Second:
			case Seconds:
				enterOuterAlt(_localctx, 7);
				{
				setState(230);
				_la = _input.LA(1);
				if ( !(_la==Second || _la==Seconds) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DurationExprContext extends ParserRuleContext {
		public List<DurationUnitContext> durationUnit() {
			return getRuleContexts(DurationUnitContext.class);
		}
		public DurationUnitContext durationUnit(int i) {
			return getRuleContext(DurationUnitContext.class,i);
		}
		public List<TerminalNode> NumberVal() { return getTokens(LogicParser.NumberVal); }
		public TerminalNode NumberVal(int i) {
			return getToken(LogicParser.NumberVal, i);
		}
		public DurationExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_durationExpr; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitDurationExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DurationExprContext durationExpr() throws RecognitionException {
		DurationExprContext _localctx = new DurationExprContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_durationExpr);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(235); 
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(233); match(NumberVal);
					setState(234); durationUnit();
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(237); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			} while ( _alt!=2 && _alt!=-1 );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TemporalUnitContext extends ParserRuleContext {
		public TerminalNode Second() { return getToken(LogicParser.Second, 0); }
		public TerminalNode Month() { return getToken(LogicParser.Month, 0); }
		public TerminalNode Minute() { return getToken(LogicParser.Minute, 0); }
		public TerminalNode Year() { return getToken(LogicParser.Year, 0); }
		public TerminalNode Day() { return getToken(LogicParser.Day, 0); }
		public TerminalNode Hour() { return getToken(LogicParser.Hour, 0); }
		public TemporalUnitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_temporalUnit; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitTemporalUnit(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TemporalUnitContext temporalUnit() throws RecognitionException {
		TemporalUnitContext _localctx = new TemporalUnitContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_temporalUnit);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(239);
			_la = _input.LA(1);
			if ( !(((((_la - 109)) & ~0x3f) == 0 && ((1L << (_la - 109)) & ((1L << (Year - 109)) | (1L << (Month - 109)) | (1L << (Day - 109)) | (1L << (Hour - 109)) | (1L << (Minute - 109)) | (1L << (Second - 109)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public int _p;
		public ExprContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ExprContext(ParserRuleContext parent, int invokingState, int _p) {
			super(parent, invokingState);
			this._p = _p;
		}
		@Override public int getRuleIndex() { return RULE_expr; }
	 
		public ExprContext() { }
		public void copyFrom(ExprContext ctx) {
			super.copyFrom(ctx);
			this._p = ctx._p;
		}
	}
	public static class ArccosContext extends ExprContext {
		public TerminalNode Arccos() { return getToken(LogicParser.Arccos, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public ArccosContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitArccos(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NoContext extends ExprContext {
		public TerminalNode No() { return getToken(LogicParser.No, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode IsTrue() { return getToken(LogicParser.IsTrue, 0); }
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public NoContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitNo(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FirstFromContext extends ExprContext {
		public TerminalNode First() { return getToken(LogicParser.First, 0); }
		public TerminalNode From() { return getToken(LogicParser.From, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public FirstFromContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitFirstFrom(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AsStringContext extends ExprContext {
		public TerminalNode AsString() { return getToken(LogicParser.AsString, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public AsStringContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitAsString(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DotContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode ID() { return getToken(LogicParser.ID, 0); }
		public DotContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitDot(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MinimumFromContext extends ExprContext {
		public TerminalNode From() { return getToken(LogicParser.From, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Min() { return getToken(LogicParser.Min, 0); }
		public MinimumFromContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitMinimumFrom(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MatchesContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode MatchesPattern() { return getToken(LogicParser.MatchesPattern, 0); }
		public MatchesContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitMatches(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AbsContext extends ExprContext {
		public TerminalNode Abs() { return getToken(LogicParser.Abs, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public AbsContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitAbs(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IndexOfFromContext extends ExprContext {
		public TerminalNode From() { return getToken(LogicParser.From, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Index() { return getToken(LogicParser.Index, 0); }
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public IndexOfFromContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitIndexOfFrom(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IsInContext extends ExprContext {
		public TerminalNode Is() { return getToken(LogicParser.Is, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode Not() { return getToken(LogicParser.Not, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode In() { return getToken(LogicParser.In, 0); }
		public IsInContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitIsIn(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SubstringContext extends ExprContext {
		public TerminalNode From() { return getToken(LogicParser.From, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode StartingAt() { return getToken(LogicParser.StartingAt, 0); }
		public TerminalNode Substring() { return getToken(LogicParser.Substring, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Characters() { return getToken(LogicParser.Characters, 0); }
		public SubstringContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitSubstring(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ElementContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public ElementContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitElement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CloneContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Clone() { return getToken(LogicParser.Clone, 0); }
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public CloneContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitClone(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OccurEqualContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode Not() { return getToken(LogicParser.Not, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Occur() { return getToken(LogicParser.Occur, 0); }
		public TerminalNode At() { return getToken(LogicParser.At, 0); }
		public TerminalNode Equal() { return getToken(LogicParser.Equal, 0); }
		public OccurEqualContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitOccurEqual(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IsWithinFollowingContext extends ExprContext {
		public TerminalNode Is() { return getToken(LogicParser.Is, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode Not() { return getToken(LogicParser.Not, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Following() { return getToken(LogicParser.Following, 0); }
		public TerminalNode Within() { return getToken(LogicParser.Within, 0); }
		public IsWithinFollowingContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitIsWithinFollowing(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class UnaryMinusContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Minus() { return getToken(LogicParser.Minus, 0); }
		public UnaryMinusContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitUnaryMinus(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OrContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Or() { return getToken(LogicParser.Or, 0); }
		public OrContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitOr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class UnaryListContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public UnaryListContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitUnaryList(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ParensContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public ParensContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitParens(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LogContext extends ExprContext {
		public TerminalNode Log() { return getToken(LogicParser.Log, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public LogContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitLog(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AtMostOrLeastContext extends ExprContext {
		public TerminalNode AreTrue() { return getToken(LogicParser.AreTrue, 0); }
		public TerminalNode Least() { return getToken(LogicParser.Least, 0); }
		public TerminalNode From() { return getToken(LogicParser.From, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode At() { return getToken(LogicParser.At, 0); }
		public TerminalNode IsTrue() { return getToken(LogicParser.IsTrue, 0); }
		public TerminalNode Most() { return getToken(LogicParser.Most, 0); }
		public AtMostOrLeastContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitAtMostOrLeast(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExtractContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TemporalUnitContext temporalUnit() {
			return getRuleContext(TemporalUnitContext.class,0);
		}
		public TerminalNode Extract() { return getToken(LogicParser.Extract, 0); }
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public ExtractContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitExtract(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TimeOfDayValContext extends ExprContext {
		public TerminalNode TimeOfDayVal() { return getToken(LogicParser.TimeOfDayVal, 0); }
		public TimeOfDayValContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitTimeOfDayVal(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NotContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Not() { return getToken(LogicParser.Not, 0); }
		public NotContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitNot(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MedianContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Median() { return getToken(LogicParser.Median, 0); }
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public MedianContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitMedian(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FloorContext extends ExprContext {
		public TerminalNode Floor() { return getToken(LogicParser.Floor, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public TerminalNode Int() { return getToken(LogicParser.Int, 0); }
		public FloorContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitFloor(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NowContext extends ExprContext {
		public TerminalNode Now() { return getToken(LogicParser.Now, 0); }
		public NowContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitNow(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TruncateContext extends ExprContext {
		public TerminalNode Truncate() { return getToken(LogicParser.Truncate, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public TruncateContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitTruncate(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IsLessThanOrEqualContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode LessThanOrEqual() { return getToken(LogicParser.LessThanOrEqual, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public IsLessThanOrEqualContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitIsLessThanOrEqual(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NullValContext extends ExprContext {
		public TerminalNode Null() { return getToken(LogicParser.Null, 0); }
		public NullValContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitNullVal(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OccurBeforeContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode Not() { return getToken(LogicParser.Not, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Before() { return getToken(LogicParser.Before, 0); }
		public TerminalNode Occur() { return getToken(LogicParser.Occur, 0); }
		public OccurBeforeContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitOccurBefore(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class WhereTimeIsPresentContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode WhereTimePresent() { return getToken(LogicParser.WhereTimePresent, 0); }
		public WhereTimeIsPresentContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitWhereTimeIsPresent(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SortContext extends ExprContext {
		public TerminalNode Sort() { return getToken(LogicParser.Sort, 0); }
		public TerminalNode Time() { return getToken(LogicParser.Time, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Data() { return getToken(LogicParser.Data, 0); }
		public SortContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitSort(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IntervalContext extends ExprContext {
		public TerminalNode Interval() { return getToken(LogicParser.Interval, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public IntervalContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitInterval(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MinimumContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public TerminalNode Min() { return getToken(LogicParser.Min, 0); }
		public MinimumContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitMinimum(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BeforeContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Before() { return getToken(LogicParser.Before, 0); }
		public BeforeContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitBefore(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MergeContext extends ExprContext {
		public TerminalNode Merge() { return getToken(LogicParser.Merge, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public MergeContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitMerge(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IsLessThanContext extends ExprContext {
		public TerminalNode LessThan() { return getToken(LogicParser.LessThan, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public IsLessThanContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitIsLessThan(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class UnaryPlusContext extends ExprContext {
		public TerminalNode Plus() { return getToken(LogicParser.Plus, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public UnaryPlusContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitUnaryPlus(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IsBeforeContext extends ExprContext {
		public TerminalNode Is() { return getToken(LogicParser.Is, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode Not() { return getToken(LogicParser.Not, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Before() { return getToken(LogicParser.Before, 0); }
		public IsBeforeContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitIsBefore(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AddToListContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode To() { return getToken(LogicParser.To, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode At() { return getToken(LogicParser.At, 0); }
		public TerminalNode Add() { return getToken(LogicParser.Add, 0); }
		public AddToListContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitAddToList(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LengthContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Length() { return getToken(LogicParser.Length, 0); }
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public LengthContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitLength(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AtTimeContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode AtTime() { return getToken(LogicParser.AtTime, 0); }
		public AtTimeContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitAtTime(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EarliestFromContext extends ExprContext {
		public TerminalNode From() { return getToken(LogicParser.From, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Earliest() { return getToken(LogicParser.Earliest, 0); }
		public EarliestFromContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitEarliestFrom(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OccurWithinFollowingContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode Not() { return getToken(LogicParser.Not, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Occur() { return getToken(LogicParser.Occur, 0); }
		public TerminalNode Following() { return getToken(LogicParser.Following, 0); }
		public TerminalNode Within() { return getToken(LogicParser.Within, 0); }
		public OccurWithinFollowingContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitOccurWithinFollowing(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OccurWithinPrecedingContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode Not() { return getToken(LogicParser.Not, 0); }
		public TerminalNode Preceding() { return getToken(LogicParser.Preceding, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Occur() { return getToken(LogicParser.Occur, 0); }
		public TerminalNode Within() { return getToken(LogicParser.Within, 0); }
		public OccurWithinPrecedingContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitOccurWithinPreceding(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ReverseContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Reverse() { return getToken(LogicParser.Reverse, 0); }
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public ReverseContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitReverse(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IsDataTypeContext extends ExprContext {
		public TerminalNode Is() { return getToken(LogicParser.Is, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Not() { return getToken(LogicParser.Not, 0); }
		public DataTypeContext dataType() {
			return getRuleContext(DataTypeContext.class,0);
		}
		public IsDataTypeContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitIsDataType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArctanContext extends ExprContext {
		public TerminalNode Arctan() { return getToken(LogicParser.Arctan, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public ArctanContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitArctan(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SqrtContext extends ExprContext {
		public TerminalNode Sqrt() { return getToken(LogicParser.Sqrt, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public SqrtContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitSqrt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OccurWithinPastContext extends ExprContext {
		public TerminalNode The() { return getToken(LogicParser.The, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode Not() { return getToken(LogicParser.Not, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Past() { return getToken(LogicParser.Past, 0); }
		public TerminalNode Occur() { return getToken(LogicParser.Occur, 0); }
		public TerminalNode Within() { return getToken(LogicParser.Within, 0); }
		public OccurWithinPastContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitOccurWithinPast(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AsNumberContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode AsNumber() { return getToken(LogicParser.AsNumber, 0); }
		public AsNumberContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitAsNumber(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DivideContext extends ExprContext {
		public TerminalNode Div() { return getToken(LogicParser.Div, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public DivideContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitDivide(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class StringValContext extends ExprContext {
		public TerminalNode StringVal() { return getToken(LogicParser.StringVal, 0); }
		public StringValContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitStringVal(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ReplaceContext extends ExprContext {
		public TerminalNode With() { return getToken(LogicParser.With, 0); }
		public TerminalNode Replace() { return getToken(LogicParser.Replace, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TemporalUnitContext temporalUnit() {
			return getRuleContext(TemporalUnitContext.class,0);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public ReplaceContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitReplace(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArcsinContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Arcsin() { return getToken(LogicParser.Arcsin, 0); }
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public ArcsinContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitArcsin(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IsWithinToContext extends ExprContext {
		public TerminalNode Is() { return getToken(LogicParser.Is, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode Not() { return getToken(LogicParser.Not, 0); }
		public TerminalNode To() { return getToken(LogicParser.To, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Within() { return getToken(LogicParser.Within, 0); }
		public IsWithinToContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitIsWithinTo(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MaximumFromContext extends ExprContext {
		public TerminalNode From() { return getToken(LogicParser.From, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Max() { return getToken(LogicParser.Max, 0); }
		public MaximumFromContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitMaximumFrom(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IsWithinSurroundingContext extends ExprContext {
		public TerminalNode Is() { return getToken(LogicParser.Is, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode Not() { return getToken(LogicParser.Not, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Within() { return getToken(LogicParser.Within, 0); }
		public TerminalNode Surrounding() { return getToken(LogicParser.Surrounding, 0); }
		public IsWithinSurroundingContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitIsWithinSurrounding(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IsObjectTypeContext extends ExprContext {
		public TerminalNode Is() { return getToken(LogicParser.Is, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode Not() { return getToken(LogicParser.Not, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public IsObjectTypeContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitIsObjectType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AgoContext extends ExprContext {
		public TerminalNode Ago() { return getToken(LogicParser.Ago, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public AgoContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitAgo(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DecreaseContext extends ExprContext {
		public TerminalNode Percent() { return getToken(LogicParser.Percent, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public TerminalNode Decrease() { return getToken(LogicParser.Decrease, 0); }
		public DecreaseContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitDecrease(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class StdDevContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Stddev() { return getToken(LogicParser.Stddev, 0); }
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public StdDevContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitStdDev(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExtractCharsContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Extract() { return getToken(LogicParser.Extract, 0); }
		public TerminalNode Characters() { return getToken(LogicParser.Characters, 0); }
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public ExtractCharsContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitExtractChars(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CountContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Count() { return getToken(LogicParser.Count, 0); }
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public CountContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitCount(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LastContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Last() { return getToken(LogicParser.Last, 0); }
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public LastContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitLast(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NumberValContext extends ExprContext {
		public TerminalNode NumberVal() { return getToken(LogicParser.NumberVal, 0); }
		public NumberValContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitNumberVal(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BooleanValContext extends ExprContext {
		public TerminalNode BooleanVal() { return getToken(LogicParser.BooleanVal, 0); }
		public BooleanValContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitBooleanVal(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RoundContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Round() { return getToken(LogicParser.Round, 0); }
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public RoundContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitRound(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class WhereContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Where() { return getToken(LogicParser.Where, 0); }
		public WhereContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitWhere(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IsWithinPrecedingContext extends ExprContext {
		public TerminalNode Is() { return getToken(LogicParser.Is, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode Not() { return getToken(LogicParser.Not, 0); }
		public TerminalNode Preceding() { return getToken(LogicParser.Preceding, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Within() { return getToken(LogicParser.Within, 0); }
		public IsWithinPrecedingContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitIsWithinPreceding(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LastFromContext extends ExprContext {
		public TerminalNode From() { return getToken(LogicParser.From, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode Last() { return getToken(LogicParser.Last, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public LastFromContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitLastFrom(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BinaryListContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public BinaryListContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitBinaryList(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IsAfterContext extends ExprContext {
		public TerminalNode Is() { return getToken(LogicParser.Is, 0); }
		public TerminalNode After() { return getToken(LogicParser.After, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode Not() { return getToken(LogicParser.Not, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public IsAfterContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitIsAfter(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class UppercaseContext extends ExprContext {
		public TerminalNode Uppercase() { return getToken(LogicParser.Uppercase, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public UppercaseContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitUppercase(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BuildStringContext extends ExprContext {
		public TerminalNode String() { return getToken(LogicParser.String, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public BuildStringContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitBuildString(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AfterContext extends ExprContext {
		public TerminalNode After() { return getToken(LogicParser.After, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public AfterContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitAfter(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OccurWithinToContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode Not() { return getToken(LogicParser.Not, 0); }
		public TerminalNode To() { return getToken(LogicParser.To, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Occur() { return getToken(LogicParser.Occur, 0); }
		public TerminalNode Within() { return getToken(LogicParser.Within, 0); }
		public OccurWithinToContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitOccurWithinTo(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EarliestContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Earliest() { return getToken(LogicParser.Earliest, 0); }
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public EarliestContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitEarliest(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OccurWithinSameDayContext extends ExprContext {
		public TerminalNode SameDayAs() { return getToken(LogicParser.SameDayAs, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode Not() { return getToken(LogicParser.Not, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Occur() { return getToken(LogicParser.Occur, 0); }
		public TerminalNode Within() { return getToken(LogicParser.Within, 0); }
		public OccurWithinSameDayContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitOccurWithinSameDay(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AnyContext extends ExprContext {
		public TerminalNode Any() { return getToken(LogicParser.Any, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode IsTrue() { return getToken(LogicParser.IsTrue, 0); }
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public AnyContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitAny(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DayOfWeekFuncContext extends ExprContext {
		public TerminalNode DayOfWeekFunc() { return getToken(LogicParser.DayOfWeekFunc, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public DayOfWeekFuncContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitDayOfWeekFunc(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IsWithinSameDayContext extends ExprContext {
		public TerminalNode Is() { return getToken(LogicParser.Is, 0); }
		public TerminalNode SameDayAs() { return getToken(LogicParser.SameDayAs, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode Not() { return getToken(LogicParser.Not, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Within() { return getToken(LogicParser.Within, 0); }
		public IsWithinSameDayContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitIsWithinSameDay(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SubListContext extends ExprContext {
		public TerminalNode From() { return getToken(LogicParser.From, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode StartingAt() { return getToken(LogicParser.StartingAt, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Sublist() { return getToken(LogicParser.Sublist, 0); }
		public TerminalNode Elements() { return getToken(LogicParser.Elements, 0); }
		public SubListContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitSubList(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IndexContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Index() { return getToken(LogicParser.Index, 0); }
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public IndexTypeContext indexType() {
			return getRuleContext(IndexTypeContext.class,0);
		}
		public IndexContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitIndex(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LatestFromContext extends ExprContext {
		public TerminalNode From() { return getToken(LogicParser.From, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode Latest() { return getToken(LogicParser.Latest, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public LatestFromContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitLatestFrom(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IndexFromContext extends ExprContext {
		public TerminalNode From() { return getToken(LogicParser.From, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Index() { return getToken(LogicParser.Index, 0); }
		public IndexTypeContext indexType() {
			return getRuleContext(IndexTypeContext.class,0);
		}
		public IndexFromContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitIndexFrom(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AndContext extends ExprContext {
		public TerminalNode And() { return getToken(LogicParser.And, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public AndContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitAnd(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IsGreaterThanOrEqualContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode GreaterThanOrEqual() { return getToken(LogicParser.GreaterThanOrEqual, 0); }
		public IsGreaterThanOrEqualContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitIsGreaterThanOrEqual(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExtractAttrNamesContext extends ExprContext {
		public TerminalNode Names() { return getToken(LogicParser.Names, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Extract() { return getToken(LogicParser.Extract, 0); }
		public TerminalNode Attribute() { return getToken(LogicParser.Attribute, 0); }
		public ExtractAttrNamesContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitExtractAttrNames(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExpContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public TerminalNode Exp() { return getToken(LogicParser.Exp, 0); }
		public ExpContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitExp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CeilingContext extends ExprContext {
		public TerminalNode Ceiling() { return getToken(LogicParser.Ceiling, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public CeilingContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitCeiling(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SineContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Sine() { return getToken(LogicParser.Sine, 0); }
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public SineContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitSine(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MaximumContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public TerminalNode Max() { return getToken(LogicParser.Max, 0); }
		public MaximumContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitMaximum(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IsEqualContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode EqualTo() { return getToken(LogicParser.EqualTo, 0); }
		public IsEqualContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitIsEqual(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TangentContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Tangent() { return getToken(LogicParser.Tangent, 0); }
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public TangentContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitTangent(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AsTimeContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode AsTime() { return getToken(LogicParser.AsTime, 0); }
		public AsTimeContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitAsTime(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IsGreaterThanContext extends ExprContext {
		public TerminalNode GreaterThan() { return getToken(LogicParser.GreaterThan, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public IsGreaterThanContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitIsGreaterThan(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OccurAfterContext extends ExprContext {
		public TerminalNode After() { return getToken(LogicParser.After, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode Not() { return getToken(LogicParser.Not, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Occur() { return getToken(LogicParser.Occur, 0); }
		public OccurAfterContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitOccurAfter(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SumContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public TerminalNode Sum() { return getToken(LogicParser.Sum, 0); }
		public SumContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitSum(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AddContext extends ExprContext {
		public TerminalNode Plus() { return getToken(LogicParser.Plus, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public AddContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitAdd(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SeqtoContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode SeqTo() { return getToken(LogicParser.SeqTo, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public SeqtoContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitSeqto(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TimeOfDayFuncContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode TimeOfDayFunc() { return getToken(LogicParser.TimeOfDayFunc, 0); }
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public TimeOfDayFuncContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitTimeOfDayFunc(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OccurWithinSurroundingContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode Not() { return getToken(LogicParser.Not, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Occur() { return getToken(LogicParser.Occur, 0); }
		public TerminalNode Within() { return getToken(LogicParser.Within, 0); }
		public TerminalNode Surrounding() { return getToken(LogicParser.Surrounding, 0); }
		public OccurWithinSurroundingContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitOccurWithinSurrounding(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IdContext extends ExprContext {
		public TerminalNode The() { return getToken(LogicParser.The, 0); }
		public TerminalNode ID() { return getToken(LogicParser.ID, 0); }
		public IdContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitId(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NewObjectContext extends ExprContext {
		public ObjOrderedWithContext objOrderedWith() {
			return getRuleContext(ObjOrderedWithContext.class,0);
		}
		public TerminalNode New() { return getToken(LogicParser.New, 0); }
		public ObjNamedWithContext objNamedWith() {
			return getRuleContext(ObjNamedWithContext.class,0);
		}
		public TerminalNode ID() { return getToken(LogicParser.ID, 0); }
		public NewObjectContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitNewObject(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AttributeFromContext extends ExprContext {
		public TerminalNode From() { return getToken(LogicParser.From, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Attribute() { return getToken(LogicParser.Attribute, 0); }
		public AttributeFromContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitAttributeFrom(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LowercaseContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Lowercase() { return getToken(LogicParser.Lowercase, 0); }
		public LowercaseContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitLowercase(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TimeValContext extends ExprContext {
		public TerminalNode TimeVal() { return getToken(LogicParser.TimeVal, 0); }
		public TimeValContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitTimeVal(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TrimContext extends ExprContext {
		public TerminalNode Left() { return getToken(LogicParser.Left, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Right() { return getToken(LogicParser.Right, 0); }
		public TerminalNode Trim() { return getToken(LogicParser.Trim, 0); }
		public TrimContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitTrim(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExistContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Exist() { return getToken(LogicParser.Exist, 0); }
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public ExistContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitExist(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DayOfWeekContext extends ExprContext {
		public TerminalNode DayOfWeek() { return getToken(LogicParser.DayOfWeek, 0); }
		public DayOfWeekContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitDayOfWeek(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AllContext extends ExprContext {
		public TerminalNode AreTrue() { return getToken(LogicParser.AreTrue, 0); }
		public TerminalNode All() { return getToken(LogicParser.All, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public AllContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitAll(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CosineContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Cosine() { return getToken(LogicParser.Cosine, 0); }
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public CosineContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitCosine(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RemoveFromListContext extends ExprContext {
		public TerminalNode From() { return getToken(LogicParser.From, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode Remove() { return getToken(LogicParser.Remove, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public RemoveFromListContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitRemoveFromList(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MultiplyContext extends ExprContext {
		public TerminalNode Mul() { return getToken(LogicParser.Mul, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public MultiplyContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitMultiply(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VarianceContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public TerminalNode Variance() { return getToken(LogicParser.Variance, 0); }
		public VarianceContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitVariance(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NearestContext extends ExprContext {
		public TerminalNode From() { return getToken(LogicParser.From, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Nearest() { return getToken(LogicParser.Nearest, 0); }
		public TerminalNode Index() { return getToken(LogicParser.Index, 0); }
		public NearestContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitNearest(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ConcatContext extends ExprContext {
		public List<TerminalNode> Concat() { return getTokens(LogicParser.Concat); }
		public TerminalNode Concat(int i) {
			return getToken(LogicParser.Concat, i);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public ConcatContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitConcat(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EmptyListContext extends ExprContext {
		public TerminalNode EmptyList() { return getToken(LogicParser.EmptyList, 0); }
		public EmptyListContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitEmptyList(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Log10Context extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Log10() { return getToken(LogicParser.Log10, 0); }
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public Log10Context(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitLog10(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CurrentTimeContext extends ExprContext {
		public TerminalNode CurrentTime() { return getToken(LogicParser.CurrentTime, 0); }
		public CurrentTimeContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitCurrentTime(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SubtractContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Minus() { return getToken(LogicParser.Minus, 0); }
		public SubtractContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitSubtract(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DurationContext extends ExprContext {
		public DurationExprContext durationExpr() {
			return getRuleContext(DurationExprContext.class,0);
		}
		public DurationContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitDuration(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FindInStringContext extends ExprContext {
		public TerminalNode String() { return getToken(LogicParser.String, 0); }
		public TerminalNode Find() { return getToken(LogicParser.Find, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode StartingAt() { return getToken(LogicParser.StartingAt, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode In() { return getToken(LogicParser.In, 0); }
		public FindInStringContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitFindInString(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IsNotEqualContext extends ExprContext {
		public TerminalNode NotEqualTo() { return getToken(LogicParser.NotEqualTo, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public IsNotEqualContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitIsNotEqual(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IncreaseContext extends ExprContext {
		public TerminalNode Percent() { return getToken(LogicParser.Percent, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Increase() { return getToken(LogicParser.Increase, 0); }
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public IncreaseContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitIncrease(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RaiseToPowerContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode Pow() { return getToken(LogicParser.Pow, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public RaiseToPowerContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitRaiseToPower(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TimeFuncContext extends ExprContext {
		public TerminalNode Time() { return getToken(LogicParser.Time, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public TimeFuncContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitTimeFunc(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class LatestContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Latest() { return getToken(LogicParser.Latest, 0); }
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public LatestContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitLatest(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FirstContext extends ExprContext {
		public TerminalNode First() { return getToken(LogicParser.First, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public FirstContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitFirst(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IsWithinPastContext extends ExprContext {
		public TerminalNode Is() { return getToken(LogicParser.Is, 0); }
		public TerminalNode The() { return getToken(LogicParser.The, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode Not() { return getToken(LogicParser.Not, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode Past() { return getToken(LogicParser.Past, 0); }
		public TerminalNode Within() { return getToken(LogicParser.Within, 0); }
		public IsWithinPastContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitIsWithinPast(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AverageContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode Of() { return getToken(LogicParser.Of, 0); }
		public TerminalNode Avg() { return getToken(LogicParser.Avg, 0); }
		public AverageContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitAverage(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExprContext _localctx = new ExprContext(_ctx, _parentState, _p);
		ExprContext _prevctx = _localctx;
		int _startState = 40;
		enterRecursionRule(_localctx, RULE_expr);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(654);
			switch ( getInterpreter().adaptivePredict(_input,79,_ctx) ) {
			case 1:
				{
				_localctx = new CountContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(242); match(Count);
				setState(244);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(243); match(Of);
					}
				}

				setState(246); expr(126);
				}
				break;

			case 2:
				{
				_localctx = new ExistContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(247); match(Exist);
				setState(249);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(248); match(Of);
					}
				}

				setState(251); expr(125);
				}
				break;

			case 3:
				{
				_localctx = new AverageContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(252); match(Avg);
				setState(254);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(253); match(Of);
					}
				}

				setState(256); expr(124);
				}
				break;

			case 4:
				{
				_localctx = new MedianContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(257); match(Median);
				setState(259);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(258); match(Of);
					}
				}

				setState(261); expr(123);
				}
				break;

			case 5:
				{
				_localctx = new SumContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(262); match(Sum);
				setState(264);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(263); match(Of);
					}
				}

				setState(266); expr(122);
				}
				break;

			case 6:
				{
				_localctx = new StdDevContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(267); match(Stddev);
				setState(269);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(268); match(Of);
					}
				}

				setState(271); expr(121);
				}
				break;

			case 7:
				{
				_localctx = new VarianceContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(272); match(Variance);
				setState(274);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(273); match(Of);
					}
				}

				setState(276); expr(120);
				}
				break;

			case 8:
				{
				_localctx = new AnyContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(277); match(Any);
				setState(279);
				_la = _input.LA(1);
				if (_la==IsTrue) {
					{
					setState(278); match(IsTrue);
					}
				}

				setState(282);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(281); match(Of);
					}
				}

				setState(284); expr(119);
				}
				break;

			case 9:
				{
				_localctx = new AllContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(285); match(All);
				setState(287);
				_la = _input.LA(1);
				if (_la==AreTrue) {
					{
					setState(286); match(AreTrue);
					}
				}

				setState(290);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(289); match(Of);
					}
				}

				setState(292); expr(118);
				}
				break;

			case 10:
				{
				_localctx = new NoContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(293); match(No);
				setState(295);
				_la = _input.LA(1);
				if (_la==IsTrue) {
					{
					setState(294); match(IsTrue);
					}
				}

				setState(298);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(297); match(Of);
					}
				}

				setState(300); expr(117);
				}
				break;

			case 11:
				{
				_localctx = new MinimumContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(301); match(Min);
				setState(303);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(302); match(Of);
					}
				}

				setState(305); expr(108);
				}
				break;

			case 12:
				{
				_localctx = new MaximumContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(306); match(Max);
				setState(308);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(307); match(Of);
					}
				}

				setState(310); expr(107);
				}
				break;

			case 13:
				{
				_localctx = new FirstContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(311); match(First);
				setState(313);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(312); match(Of);
					}
				}

				setState(315); expr(106);
				}
				break;

			case 14:
				{
				_localctx = new LastContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(316); match(Last);
				setState(318);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(317); match(Of);
					}
				}

				setState(320); expr(105);
				}
				break;

			case 15:
				{
				_localctx = new EarliestContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(321); match(Earliest);
				setState(323);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(322); match(Of);
					}
				}

				setState(325); expr(104);
				}
				break;

			case 16:
				{
				_localctx = new LatestContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(326); match(Latest);
				setState(328);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(327); match(Of);
					}
				}

				setState(330); expr(103);
				}
				break;

			case 17:
				{
				_localctx = new IndexContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(331); match(Index);
				setState(332); indexType();
				setState(334);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(333); match(Of);
					}
				}

				setState(336); expr(98);
				}
				break;

			case 18:
				{
				_localctx = new IncreaseContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(339);
				_la = _input.LA(1);
				if (_la==Percent) {
					{
					setState(338); match(Percent);
					}
				}

				setState(341); match(Increase);
				setState(343);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(342); match(Of);
					}
				}

				setState(345); expr(97);
				}
				break;

			case 19:
				{
				_localctx = new DecreaseContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(347);
				_la = _input.LA(1);
				if (_la==Percent) {
					{
					setState(346); match(Percent);
					}
				}

				setState(349); match(Decrease);
				setState(351);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(350); match(Of);
					}
				}

				setState(353); expr(96);
				}
				break;

			case 20:
				{
				_localctx = new IntervalContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(354); match(Interval);
				setState(356);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(355); match(Of);
					}
				}

				setState(358); expr(95);
				}
				break;

			case 21:
				{
				_localctx = new TimeFuncContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(359); match(Time);
				setState(361);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(360); match(Of);
					}
				}

				setState(363); expr(94);
				}
				break;

			case 22:
				{
				_localctx = new TimeOfDayFuncContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(364); match(TimeOfDayFunc);
				setState(366);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(365); match(Of);
					}
				}

				setState(368); expr(93);
				}
				break;

			case 23:
				{
				_localctx = new DayOfWeekFuncContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(369); match(DayOfWeekFunc);
				setState(371);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(370); match(Of);
					}
				}

				setState(373); expr(92);
				}
				break;

			case 24:
				{
				_localctx = new ArccosContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(374); match(Arccos);
				setState(376);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(375); match(Of);
					}
				}

				setState(378); expr(91);
				}
				break;

			case 25:
				{
				_localctx = new ArcsinContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(379); match(Arcsin);
				setState(381);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(380); match(Of);
					}
				}

				setState(383); expr(90);
				}
				break;

			case 26:
				{
				_localctx = new ArctanContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(384); match(Arctan);
				setState(386);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(385); match(Of);
					}
				}

				setState(388); expr(89);
				}
				break;

			case 27:
				{
				_localctx = new CosineContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(389); match(Cosine);
				setState(391);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(390); match(Of);
					}
				}

				setState(393); expr(88);
				}
				break;

			case 28:
				{
				_localctx = new SineContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(394); match(Sine);
				setState(396);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(395); match(Of);
					}
				}

				setState(398); expr(87);
				}
				break;

			case 29:
				{
				_localctx = new TangentContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(399); match(Tangent);
				setState(401);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(400); match(Of);
					}
				}

				setState(403); expr(86);
				}
				break;

			case 30:
				{
				_localctx = new ExpContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(404); match(Exp);
				setState(406);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(405); match(Of);
					}
				}

				setState(408); expr(85);
				}
				break;

			case 31:
				{
				_localctx = new LogContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(409); match(Log);
				setState(411);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(410); match(Of);
					}
				}

				setState(413); expr(84);
				}
				break;

			case 32:
				{
				_localctx = new Log10Context(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(414); match(Log10);
				setState(416);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(415); match(Of);
					}
				}

				setState(418); expr(83);
				}
				break;

			case 33:
				{
				_localctx = new FloorContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(419);
				_la = _input.LA(1);
				if ( !(_la==Int || _la==Floor) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(421);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(420); match(Of);
					}
				}

				setState(423); expr(82);
				}
				break;

			case 34:
				{
				_localctx = new CeilingContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(424); match(Ceiling);
				setState(426);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(425); match(Of);
					}
				}

				setState(428); expr(81);
				}
				break;

			case 35:
				{
				_localctx = new TruncateContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(429); match(Truncate);
				setState(431);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(430); match(Of);
					}
				}

				setState(433); expr(80);
				}
				break;

			case 36:
				{
				_localctx = new RoundContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(434); match(Round);
				setState(436);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(435); match(Of);
					}
				}

				setState(438); expr(79);
				}
				break;

			case 37:
				{
				_localctx = new AbsContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(439); match(Abs);
				setState(441);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(440); match(Of);
					}
				}

				setState(443); expr(78);
				}
				break;

			case 38:
				{
				_localctx = new SqrtContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(444); match(Sqrt);
				setState(446);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(445); match(Of);
					}
				}

				setState(448); expr(77);
				}
				break;

			case 39:
				{
				_localctx = new ExtractContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(449); match(Extract);
				setState(450); temporalUnit();
				setState(452);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(451); match(Of);
					}
				}

				setState(454); expr(76);
				}
				break;

			case 40:
				{
				_localctx = new ReverseContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(456); match(Reverse);
				setState(458);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(457); match(Of);
					}
				}

				setState(460); expr(74);
				}
				break;

			case 41:
				{
				_localctx = new ExtractCharsContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(461); match(Extract);
				setState(462); match(Characters);
				setState(464);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(463); match(Of);
					}
				}

				setState(466); expr(73);
				}
				break;

			case 42:
				{
				_localctx = new BuildStringContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(467); match(String);
				setState(469);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(468); match(Of);
					}
				}

				setState(471); expr(72);
				}
				break;

			case 43:
				{
				_localctx = new LengthContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(472); match(Length);
				setState(474);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(473); match(Of);
					}
				}

				setState(476); expr(71);
				}
				break;

			case 44:
				{
				_localctx = new ExtractAttrNamesContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(477); match(Extract);
				setState(478); match(Attribute);
				setState(479); match(Names);
				setState(480); expr(69);
				}
				break;

			case 45:
				{
				_localctx = new CloneContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(481); match(Clone);
				setState(483);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(482); match(Of);
					}
				}

				setState(485); expr(68);
				}
				break;

			case 46:
				{
				_localctx = new UnaryPlusContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(486); match(Plus);
				setState(487); expr(55);
				}
				break;

			case 47:
				{
				_localctx = new UnaryMinusContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(488); match(Minus);
				setState(489); expr(54);
				}
				break;

			case 48:
				{
				_localctx = new UppercaseContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(490); match(Uppercase);
				setState(491); expr(53);
				}
				break;

			case 49:
				{
				_localctx = new LowercaseContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(492); match(Lowercase);
				setState(493); expr(52);
				}
				break;

			case 50:
				{
				_localctx = new TrimContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(494); match(Trim);
				setState(496);
				_la = _input.LA(1);
				if (_la==Left || _la==Right) {
					{
					setState(495);
					_la = _input.LA(1);
					if ( !(_la==Left || _la==Right) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					}
				}

				setState(498); expr(51);
				}
				break;

			case 51:
				{
				_localctx = new NotContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(499); match(Not);
				setState(500); expr(22);
				}
				break;

			case 52:
				{
				_localctx = new SortContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(501); match(Sort);
				setState(503);
				switch ( getInterpreter().adaptivePredict(_input,67,_ctx) ) {
				case 1:
					{
					setState(502);
					_la = _input.LA(1);
					if ( !(_la==Time || _la==Data) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					}
					break;
				}
				setState(505); expr(16);
				}
				break;

			case 53:
				{
				_localctx = new UnaryListContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(506); match(4);
				setState(507); expr(13);
				}
				break;

			case 54:
				{
				_localctx = new ParensContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(508); match(7);
				push(true);
				setState(510); expr(0);
				pop();
				setState(512); match(2);
				}
				break;

			case 55:
				{
				_localctx = new MinimumFromContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(514); match(Min);
				setState(515); expr(0);
				setState(516); match(From);
				setState(517); expr(0);
				}
				break;

			case 56:
				{
				_localctx = new MaximumFromContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(519); match(Max);
				setState(520); expr(0);
				setState(521); match(From);
				setState(522); expr(0);
				}
				break;

			case 57:
				{
				_localctx = new EarliestFromContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(524); match(Earliest);
				setState(525); expr(0);
				setState(526); match(From);
				setState(527); expr(0);
				}
				break;

			case 58:
				{
				_localctx = new LatestFromContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(529); match(Latest);
				setState(530); expr(0);
				setState(531); match(From);
				setState(532); expr(0);
				}
				break;

			case 59:
				{
				_localctx = new FirstFromContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(534); match(First);
				setState(535); expr(0);
				setState(536); match(From);
				setState(537); expr(0);
				}
				break;

			case 60:
				{
				_localctx = new LastFromContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(539); match(Last);
				setState(540); expr(0);
				setState(541); match(From);
				setState(542); expr(0);
				}
				break;

			case 61:
				{
				_localctx = new NearestContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(545);
				_la = _input.LA(1);
				if (_la==Index) {
					{
					setState(544); match(Index);
					}
				}

				setState(547); match(Nearest);
				setState(548); expr(0);
				setState(549); match(From);
				setState(550); expr(0);
				}
				break;

			case 62:
				{
				_localctx = new AtMostOrLeastContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(552); match(At);
				setState(553);
				_la = _input.LA(1);
				if ( !(_la==Least || _la==Most) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(554); expr(0);
				setState(556);
				_la = _input.LA(1);
				if (_la==IsTrue || _la==AreTrue) {
					{
					setState(555);
					_la = _input.LA(1);
					if ( !(_la==IsTrue || _la==AreTrue) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					}
				}

				setState(558); match(From);
				setState(559); expr(0);
				}
				break;

			case 63:
				{
				_localctx = new SubListContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(561); match(Sublist);
				setState(562); expr(0);
				setState(563); match(Elements);
				setState(566);
				_la = _input.LA(1);
				if (_la==StartingAt) {
					{
					setState(564); match(StartingAt);
					setState(565); expr(0);
					}
				}

				setState(568); match(From);
				setState(569); expr(0);
				}
				break;

			case 64:
				{
				_localctx = new IndexOfFromContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(571); match(Index);
				setState(572); match(Of);
				setState(573); expr(0);
				setState(574); match(From);
				setState(575); expr(0);
				}
				break;

			case 65:
				{
				_localctx = new IndexFromContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(577); match(Index);
				setState(578); indexType();
				setState(579); expr(0);
				setState(580); match(From);
				setState(581); expr(0);
				}
				break;

			case 66:
				{
				_localctx = new ReplaceContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(583); match(Replace);
				setState(584); temporalUnit();
				setState(586);
				_la = _input.LA(1);
				if (_la==Of) {
					{
					setState(585); match(Of);
					}
				}

				setState(588); expr(0);
				setState(589); match(With);
				setState(590); expr(0);
				}
				break;

			case 67:
				{
				_localctx = new AttributeFromContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(592); match(Attribute);
				setState(593); expr(0);
				setState(594); match(From);
				setState(595); expr(0);
				}
				break;

			case 68:
				{
				_localctx = new FindInStringContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(597); match(Find);
				setState(598); expr(0);
				setState(600);
				_la = _input.LA(1);
				if (_la==In) {
					{
					setState(599); match(In);
					}
				}

				setState(602); match(String);
				setState(603); expr(0);
				setState(606);
				switch ( getInterpreter().adaptivePredict(_input,73,_ctx) ) {
				case 1:
					{
					setState(604); match(StartingAt);
					setState(605); expr(0);
					}
					break;
				}
				}
				break;

			case 69:
				{
				_localctx = new DurationContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(608); durationExpr();
				}
				break;

			case 70:
				{
				_localctx = new SubstringContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(609); match(Substring);
				setState(610); expr(0);
				setState(611); match(Characters);
				setState(614);
				_la = _input.LA(1);
				if (_la==StartingAt) {
					{
					setState(612); match(StartingAt);
					setState(613); expr(0);
					}
				}

				setState(616); match(From);
				setState(617); expr(0);
				}
				break;

			case 71:
				{
				_localctx = new AddToListContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(619); match(Add);
				setState(620); expr(0);
				setState(621); match(To);
				setState(622); expr(0);
				setState(625);
				switch ( getInterpreter().adaptivePredict(_input,75,_ctx) ) {
				case 1:
					{
					setState(623); match(At);
					setState(624); expr(0);
					}
					break;
				}
				}
				break;

			case 72:
				{
				_localctx = new RemoveFromListContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(627); match(Remove);
				setState(628); expr(0);
				setState(629); match(From);
				setState(630); expr(0);
				}
				break;

			case 73:
				{
				_localctx = new NewObjectContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(632); match(New);
				setState(633); match(ID);
				setState(635);
				switch ( getInterpreter().adaptivePredict(_input,76,_ctx) ) {
				case 1:
					{
					setState(634); objOrderedWith();
					}
					break;
				}
				setState(638);
				switch ( getInterpreter().adaptivePredict(_input,77,_ctx) ) {
				case 1:
					{
					setState(637); objNamedWith();
					}
					break;
				}
				}
				break;

			case 74:
				{
				_localctx = new NumberValContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(640); match(NumberVal);
				}
				break;

			case 75:
				{
				_localctx = new BooleanValContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(641); match(BooleanVal);
				}
				break;

			case 76:
				{
				_localctx = new NowContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(642); match(Now);
				}
				break;

			case 77:
				{
				_localctx = new CurrentTimeContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(643); match(CurrentTime);
				}
				break;

			case 78:
				{
				_localctx = new TimeValContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(644); match(TimeVal);
				}
				break;

			case 79:
				{
				_localctx = new TimeOfDayValContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(645); match(TimeOfDayVal);
				}
				break;

			case 80:
				{
				_localctx = new DayOfWeekContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(646); match(DayOfWeek);
				}
				break;

			case 81:
				{
				_localctx = new NullValContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(647); match(Null);
				}
				break;

			case 82:
				{
				_localctx = new StringValContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(648); match(StringVal);
				}
				break;

			case 83:
				{
				_localctx = new IdContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(650);
				_la = _input.LA(1);
				if (_la==The) {
					{
					setState(649); match(The);
					}
				}

				setState(652); match(ID);
				}
				break;

			case 84:
				{
				_localctx = new EmptyListContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(653); match(EmptyList);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(925);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,106,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(923);
					switch ( getInterpreter().adaptivePredict(_input,105,_ctx) ) {
					case 1:
						{
						_localctx = new SeqtoContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(656);
						if (!(129 >= _localctx._p)) throw new FailedPredicateException(this, "129 >= $_p");
						setState(657); match(SeqTo);
						setState(658); expr(130);
						}
						break;

					case 2:
						{
						_localctx = new MatchesContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(659);
						if (!(65 >= _localctx._p)) throw new FailedPredicateException(this, "65 >= $_p");
						setState(660); match(MatchesPattern);
						setState(661); expr(66);
						}
						break;

					case 3:
						{
						_localctx = new AtTimeContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(662);
						if (!(63 >= _localctx._p)) throw new FailedPredicateException(this, "63 >= $_p");
						setState(663); match(AtTime);
						setState(664); expr(64);
						}
						break;

					case 4:
						{
						_localctx = new BeforeContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(665);
						if (!(62 >= _localctx._p)) throw new FailedPredicateException(this, "62 >= $_p");
						setState(666); match(Before);
						setState(667); expr(63);
						}
						break;

					case 5:
						{
						_localctx = new AfterContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(668);
						if (!(61 >= _localctx._p)) throw new FailedPredicateException(this, "61 >= $_p");
						setState(669); match(After);
						setState(670); expr(62);
						}
						break;

					case 6:
						{
						_localctx = new RaiseToPowerContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(671);
						if (!(60 >= _localctx._p)) throw new FailedPredicateException(this, "60 >= $_p");
						setState(672); match(Pow);
						setState(673); expr(61);
						}
						break;

					case 7:
						{
						_localctx = new MultiplyContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(674);
						if (!(59 >= _localctx._p)) throw new FailedPredicateException(this, "59 >= $_p");
						setState(675); match(Mul);
						setState(676); expr(60);
						}
						break;

					case 8:
						{
						_localctx = new DivideContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(677);
						if (!(58 >= _localctx._p)) throw new FailedPredicateException(this, "58 >= $_p");
						setState(678); match(Div);
						setState(679); expr(59);
						}
						break;

					case 9:
						{
						_localctx = new AddContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(680);
						if (!(57 >= _localctx._p)) throw new FailedPredicateException(this, "57 >= $_p");
						setState(681); match(Plus);
						setState(682); expr(58);
						}
						break;

					case 10:
						{
						_localctx = new SubtractContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(683);
						if (!(56 >= _localctx._p)) throw new FailedPredicateException(this, "56 >= $_p");
						setState(684); match(Minus);
						setState(685); expr(57);
						}
						break;

					case 11:
						{
						_localctx = new IsEqualContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(686);
						if (!(48 >= _localctx._p)) throw new FailedPredicateException(this, "48 >= $_p");
						setState(687); match(EqualTo);
						setState(688); expr(49);
						}
						break;

					case 12:
						{
						_localctx = new IsNotEqualContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(689);
						if (!(47 >= _localctx._p)) throw new FailedPredicateException(this, "47 >= $_p");
						setState(690); match(NotEqualTo);
						setState(691); expr(48);
						}
						break;

					case 13:
						{
						_localctx = new IsLessThanContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(692);
						if (!(46 >= _localctx._p)) throw new FailedPredicateException(this, "46 >= $_p");
						setState(693); match(LessThan);
						setState(694); expr(47);
						}
						break;

					case 14:
						{
						_localctx = new IsLessThanOrEqualContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(695);
						if (!(45 >= _localctx._p)) throw new FailedPredicateException(this, "45 >= $_p");
						setState(696); match(LessThanOrEqual);
						setState(697); expr(46);
						}
						break;

					case 15:
						{
						_localctx = new IsGreaterThanOrEqualContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(698);
						if (!(44 >= _localctx._p)) throw new FailedPredicateException(this, "44 >= $_p");
						setState(699); match(GreaterThanOrEqual);
						setState(700); expr(45);
						}
						break;

					case 16:
						{
						_localctx = new IsGreaterThanContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(701);
						if (!(43 >= _localctx._p)) throw new FailedPredicateException(this, "43 >= $_p");
						setState(702); match(GreaterThan);
						setState(703); expr(44);
						}
						break;

					case 17:
						{
						_localctx = new AndContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(704);
						if (!(21 >= _localctx._p)) throw new FailedPredicateException(this, "21 >= $_p");
						setState(705); match(And);
						setState(706); expr(22);
						}
						break;

					case 18:
						{
						_localctx = new OrContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(707);
						if (!(20 >= _localctx._p)) throw new FailedPredicateException(this, "20 >= $_p");
						setState(708); match(Or);
						setState(709); expr(21);
						}
						break;

					case 19:
						{
						_localctx = new WhereContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(710);
						if (!(19 >= _localctx._p)) throw new FailedPredicateException(this, "19 >= $_p");
						setState(711); match(Where);
						setState(712); expr(20);
						}
						break;

					case 20:
						{
						_localctx = new MergeContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(713);
						if (!(14 >= _localctx._p)) throw new FailedPredicateException(this, "14 >= $_p");
						setState(714); match(Merge);
						setState(715); expr(15);
						}
						break;

					case 21:
						{
						_localctx = new AsNumberContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(716);
						if (!(132 >= _localctx._p)) throw new FailedPredicateException(this, "132 >= $_p");
						setState(717); match(AsNumber);
						}
						break;

					case 22:
						{
						_localctx = new AsTimeContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(718);
						if (!(131 >= _localctx._p)) throw new FailedPredicateException(this, "131 >= $_p");
						setState(719); match(AsTime);
						}
						break;

					case 23:
						{
						_localctx = new AsStringContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(720);
						if (!(130 >= _localctx._p)) throw new FailedPredicateException(this, "130 >= $_p");
						setState(721); match(AsString);
						}
						break;

					case 24:
						{
						_localctx = new DotContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(722);
						if (!(128 >= _localctx._p)) throw new FailedPredicateException(this, "128 >= $_p");
						setState(723); match(3);
						setState(724); match(ID);
						}
						break;

					case 25:
						{
						_localctx = new ElementContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(725);
						if (!(127 >= _localctx._p)) throw new FailedPredicateException(this, "127 >= $_p");
						setState(726); match(6);
						setState(727); expr(0);
						setState(728); match(1);
						}
						break;

					case 26:
						{
						_localctx = new WhereTimeIsPresentContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(730);
						if (!(101 >= _localctx._p)) throw new FailedPredicateException(this, "101 >= $_p");
						setState(731); match(WhereTimePresent);
						}
						break;

					case 27:
						{
						_localctx = new AgoContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(732);
						if (!(64 >= _localctx._p)) throw new FailedPredicateException(this, "64 >= $_p");
						setState(733); match(Ago);
						}
						break;

					case 28:
						{
						_localctx = new ConcatContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(734);
						if (!(49 >= _localctx._p)) throw new FailedPredicateException(this, "49 >= $_p");
						setState(737); 
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,80,_ctx);
						do {
							switch (_alt) {
							case 1:
								{
								{
								setState(735); match(Concat);
								setState(736); expr(0);
								}
								}
								break;
							default:
								throw new NoViableAltException(this);
							}
							setState(739); 
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,80,_ctx);
						} while ( _alt!=2 && _alt!=-1 );
						}
						break;

					case 29:
						{
						_localctx = new IsWithinToContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(741);
						if (!(42 >= _localctx._p)) throw new FailedPredicateException(this, "42 >= $_p");
						setState(742); match(Is);
						setState(744);
						_la = _input.LA(1);
						if (_la==Not) {
							{
							setState(743); match(Not);
							}
						}

						setState(746); match(Within);
						setState(747); expr(0);
						setState(748); match(To);
						setState(749); expr(0);
						}
						break;

					case 30:
						{
						_localctx = new IsWithinPrecedingContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(751);
						if (!(41 >= _localctx._p)) throw new FailedPredicateException(this, "41 >= $_p");
						setState(752); match(Is);
						setState(754);
						_la = _input.LA(1);
						if (_la==Not) {
							{
							setState(753); match(Not);
							}
						}

						setState(756); match(Within);
						setState(757); expr(0);
						setState(758); match(Preceding);
						setState(759); expr(0);
						}
						break;

					case 31:
						{
						_localctx = new IsWithinFollowingContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(761);
						if (!(40 >= _localctx._p)) throw new FailedPredicateException(this, "40 >= $_p");
						setState(762); match(Is);
						setState(764);
						_la = _input.LA(1);
						if (_la==Not) {
							{
							setState(763); match(Not);
							}
						}

						setState(766); match(Within);
						setState(767); expr(0);
						setState(768); match(Following);
						setState(769); expr(0);
						}
						break;

					case 32:
						{
						_localctx = new IsWithinSurroundingContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(771);
						if (!(39 >= _localctx._p)) throw new FailedPredicateException(this, "39 >= $_p");
						setState(772); match(Is);
						setState(774);
						_la = _input.LA(1);
						if (_la==Not) {
							{
							setState(773); match(Not);
							}
						}

						setState(776); match(Within);
						setState(777); expr(0);
						setState(778); match(Surrounding);
						setState(779); expr(0);
						}
						break;

					case 33:
						{
						_localctx = new IsWithinPastContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(781);
						if (!(38 >= _localctx._p)) throw new FailedPredicateException(this, "38 >= $_p");
						setState(782); match(Is);
						setState(784);
						_la = _input.LA(1);
						if (_la==Not) {
							{
							setState(783); match(Not);
							}
						}

						setState(786); match(Within);
						setState(788);
						_la = _input.LA(1);
						if (_la==The) {
							{
							setState(787); match(The);
							}
						}

						setState(790); match(Past);
						setState(791); expr(0);
						}
						break;

					case 34:
						{
						_localctx = new IsWithinSameDayContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(792);
						if (!(37 >= _localctx._p)) throw new FailedPredicateException(this, "37 >= $_p");
						setState(793); match(Is);
						setState(795);
						_la = _input.LA(1);
						if (_la==Not) {
							{
							setState(794); match(Not);
							}
						}

						setState(797); match(Within);
						setState(798); match(SameDayAs);
						setState(799); expr(0);
						}
						break;

					case 35:
						{
						_localctx = new IsBeforeContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(800);
						if (!(36 >= _localctx._p)) throw new FailedPredicateException(this, "36 >= $_p");
						setState(801); match(Is);
						setState(803);
						_la = _input.LA(1);
						if (_la==Not) {
							{
							setState(802); match(Not);
							}
						}

						setState(805); match(Before);
						setState(806); expr(0);
						}
						break;

					case 36:
						{
						_localctx = new IsAfterContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(807);
						if (!(35 >= _localctx._p)) throw new FailedPredicateException(this, "35 >= $_p");
						setState(808); match(Is);
						setState(810);
						_la = _input.LA(1);
						if (_la==Not) {
							{
							setState(809); match(Not);
							}
						}

						setState(812); match(After);
						setState(813); expr(0);
						}
						break;

					case 37:
						{
						_localctx = new OccurEqualContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(814);
						if (!(34 >= _localctx._p)) throw new FailedPredicateException(this, "34 >= $_p");
						setState(815); match(Occur);
						setState(817);
						_la = _input.LA(1);
						if (_la==Not) {
							{
							setState(816); match(Not);
							}
						}

						setState(819);
						_la = _input.LA(1);
						if ( !(_la==At || _la==Equal) ) {
						_errHandler.recoverInline(this);
						}
						consume();
						setState(820); expr(0);
						}
						break;

					case 38:
						{
						_localctx = new OccurWithinToContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(821);
						if (!(33 >= _localctx._p)) throw new FailedPredicateException(this, "33 >= $_p");
						setState(822); match(Occur);
						setState(824);
						_la = _input.LA(1);
						if (_la==Not) {
							{
							setState(823); match(Not);
							}
						}

						setState(826); match(Within);
						setState(827); expr(0);
						setState(828); match(To);
						setState(829); expr(0);
						}
						break;

					case 39:
						{
						_localctx = new OccurWithinPrecedingContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(831);
						if (!(32 >= _localctx._p)) throw new FailedPredicateException(this, "32 >= $_p");
						setState(832); match(Occur);
						setState(834);
						_la = _input.LA(1);
						if (_la==Not) {
							{
							setState(833); match(Not);
							}
						}

						setState(836); match(Within);
						setState(837); expr(0);
						setState(838); match(Preceding);
						setState(839); expr(0);
						}
						break;

					case 40:
						{
						_localctx = new OccurWithinFollowingContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(841);
						if (!(31 >= _localctx._p)) throw new FailedPredicateException(this, "31 >= $_p");
						setState(842); match(Occur);
						setState(844);
						_la = _input.LA(1);
						if (_la==Not) {
							{
							setState(843); match(Not);
							}
						}

						setState(846); match(Within);
						setState(847); expr(0);
						setState(848); match(Following);
						setState(849); expr(0);
						}
						break;

					case 41:
						{
						_localctx = new OccurWithinSurroundingContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(851);
						if (!(30 >= _localctx._p)) throw new FailedPredicateException(this, "30 >= $_p");
						setState(852); match(Occur);
						setState(854);
						_la = _input.LA(1);
						if (_la==Not) {
							{
							setState(853); match(Not);
							}
						}

						setState(856); match(Within);
						setState(857); expr(0);
						setState(858); match(Surrounding);
						setState(859); expr(0);
						}
						break;

					case 42:
						{
						_localctx = new OccurWithinPastContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(861);
						if (!(29 >= _localctx._p)) throw new FailedPredicateException(this, "29 >= $_p");
						setState(862); match(Occur);
						setState(864);
						_la = _input.LA(1);
						if (_la==Not) {
							{
							setState(863); match(Not);
							}
						}

						setState(866); match(Within);
						setState(868);
						_la = _input.LA(1);
						if (_la==The) {
							{
							setState(867); match(The);
							}
						}

						setState(870); match(Past);
						setState(871); expr(0);
						}
						break;

					case 43:
						{
						_localctx = new OccurWithinSameDayContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(872);
						if (!(28 >= _localctx._p)) throw new FailedPredicateException(this, "28 >= $_p");
						setState(873); match(Occur);
						setState(875);
						_la = _input.LA(1);
						if (_la==Not) {
							{
							setState(874); match(Not);
							}
						}

						setState(877); match(Within);
						setState(878); match(SameDayAs);
						setState(879); expr(0);
						}
						break;

					case 44:
						{
						_localctx = new OccurBeforeContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(880);
						if (!(27 >= _localctx._p)) throw new FailedPredicateException(this, "27 >= $_p");
						setState(881); match(Occur);
						setState(883);
						_la = _input.LA(1);
						if (_la==Not) {
							{
							setState(882); match(Not);
							}
						}

						setState(885); match(Before);
						setState(886); expr(0);
						}
						break;

					case 45:
						{
						_localctx = new OccurAfterContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(887);
						if (!(26 >= _localctx._p)) throw new FailedPredicateException(this, "26 >= $_p");
						setState(888); match(Occur);
						setState(890);
						_la = _input.LA(1);
						if (_la==Not) {
							{
							setState(889); match(Not);
							}
						}

						setState(892); match(After);
						setState(893); expr(0);
						}
						break;

					case 46:
						{
						_localctx = new IsInContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(894);
						if (!(25 >= _localctx._p)) throw new FailedPredicateException(this, "25 >= $_p");
						setState(896);
						_la = _input.LA(1);
						if (_la==Is) {
							{
							setState(895); match(Is);
							}
						}

						setState(899);
						_la = _input.LA(1);
						if (_la==Not) {
							{
							setState(898); match(Not);
							}
						}

						setState(901); match(In);
						setState(902); expr(0);
						}
						break;

					case 47:
						{
						_localctx = new IsDataTypeContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(903);
						if (!(24 >= _localctx._p)) throw new FailedPredicateException(this, "24 >= $_p");
						setState(904); match(Is);
						setState(906);
						_la = _input.LA(1);
						if (_la==Not) {
							{
							setState(905); match(Not);
							}
						}

						setState(908); dataType();
						}
						break;

					case 48:
						{
						_localctx = new IsObjectTypeContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(909);
						if (!(23 >= _localctx._p)) throw new FailedPredicateException(this, "23 >= $_p");
						setState(910); match(Is);
						setState(912);
						switch ( getInterpreter().adaptivePredict(_input,103,_ctx) ) {
						case 1:
							{
							setState(911); match(Not);
							}
							break;
						}
						setState(914); expr(0);
						}
						break;

					case 49:
						{
						_localctx = new BinaryListContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(915);
						if (!(15 >= _localctx._p)) throw new FailedPredicateException(this, "15 >= $_p");
						setState(916);
						if (!(list())) throw new FailedPredicateException(this, "list()");
						setState(919); 
						_errHandler.sync(this);
						_alt = getInterpreter().adaptivePredict(_input,104,_ctx);
						do {
							switch (_alt) {
							case 1:
								{
								{
								setState(917); match(4);
								setState(918); expr(0);
								}
								}
								break;
							default:
								throw new NoViableAltException(this);
							}
							setState(921); 
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,104,_ctx);
						} while ( _alt!=2 && _alt!=-1 );
						}
						break;
					}
					} 
				}
				setState(927);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,106,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ObjOrderedWithContext extends ParserRuleContext {
		public TerminalNode With() { return getToken(LogicParser.With, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public ObjOrderedWithContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_objOrderedWith; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitObjOrderedWith(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ObjOrderedWithContext objOrderedWith() throws RecognitionException {
		ObjOrderedWithContext _localctx = new ObjOrderedWithContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_objOrderedWith);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			push(false);
			setState(929); match(With);
			setState(930); expr(0);
			setState(935);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,107,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					{
					{
					setState(931); match(4);
					setState(932); expr(0);
					}
					} 
				}
				setState(937);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,107,_ctx);
			}
			pop();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ObjNamedWithContext extends ParserRuleContext {
		public TerminalNode With() { return getToken(LogicParser.With, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<TerminalNode> ID() { return getTokens(LogicParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(LogicParser.ID, i);
		}
		public ObjNamedWithContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_objNamedWith; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof LogicVisitor ) return ((LogicVisitor<? extends T>)visitor).visitObjNamedWith(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ObjNamedWithContext objNamedWith() throws RecognitionException {
		ObjNamedWithContext _localctx = new ObjNamedWithContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_objNamedWith);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(940); match(With);
			setState(941); match(6);
			setState(942); match(ID);
			setState(943); match(5);
			setState(944); expr(0);
			setState(951);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==4) {
				{
				{
				setState(945); match(4);
				setState(946); match(ID);
				setState(947); match(5);
				setState(948); expr(0);
				}
				}
				setState(953);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(954); match(1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 20: return expr_sempred((ExprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expr_sempred(ExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0: return 129 >= _localctx._p;

		case 1: return 65 >= _localctx._p;

		case 2: return 63 >= _localctx._p;

		case 3: return 62 >= _localctx._p;

		case 4: return 61 >= _localctx._p;

		case 5: return 60 >= _localctx._p;

		case 6: return 59 >= _localctx._p;

		case 7: return 58 >= _localctx._p;

		case 8: return 57 >= _localctx._p;

		case 9: return 56 >= _localctx._p;

		case 10: return 48 >= _localctx._p;

		case 11: return 47 >= _localctx._p;

		case 12: return 46 >= _localctx._p;

		case 13: return 45 >= _localctx._p;

		case 14: return 44 >= _localctx._p;

		case 15: return 43 >= _localctx._p;

		case 17: return 20 >= _localctx._p;

		case 16: return 21 >= _localctx._p;

		case 19: return 14 >= _localctx._p;

		case 18: return 19 >= _localctx._p;

		case 21: return 131 >= _localctx._p;

		case 20: return 132 >= _localctx._p;

		case 23: return 128 >= _localctx._p;

		case 22: return 130 >= _localctx._p;

		case 25: return 101 >= _localctx._p;

		case 24: return 127 >= _localctx._p;

		case 27: return 49 >= _localctx._p;

		case 26: return 64 >= _localctx._p;

		case 29: return 41 >= _localctx._p;

		case 28: return 42 >= _localctx._p;

		case 31: return 39 >= _localctx._p;

		case 30: return 40 >= _localctx._p;

		case 34: return 36 >= _localctx._p;

		case 35: return 35 >= _localctx._p;

		case 32: return 38 >= _localctx._p;

		case 33: return 37 >= _localctx._p;

		case 38: return 32 >= _localctx._p;

		case 39: return 31 >= _localctx._p;

		case 36: return 34 >= _localctx._p;

		case 37: return 33 >= _localctx._p;

		case 42: return 28 >= _localctx._p;

		case 43: return 27 >= _localctx._p;

		case 40: return 30 >= _localctx._p;

		case 41: return 29 >= _localctx._p;

		case 46: return 24 >= _localctx._p;

		case 47: return 23 >= _localctx._p;

		case 44: return 26 >= _localctx._p;

		case 45: return 25 >= _localctx._p;

		case 49: return list();

		case 48: return 15 >= _localctx._p;
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\uacf5\uee8c\u4f5d\u8b0d\u4a45\u78bd\u1b2f\u3378\3\u00b4\u03bf\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\3\2\7\2\62"+
		"\n\2\f\2\16\2\65\13\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\7\3@\n\3\f\3"+
		"\16\3C\13\3\3\3\3\3\5\3G\n\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\6\4Q\n\4"+
		"\r\4\16\4R\3\4\3\4\5\4W\n\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3"+
		"\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\b\3\b\3\t\7\tn\n\t\f\t\16\tq\13\t\3\t"+
		"\3\t\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\5\13\u0096\n\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f"+
		"\3\f\3\f\3\f\3\f\7\f\u00a3\n\f\f\f\16\f\u00a6\13\f\3\f\3\f\3\f\3\f\3\f"+
		"\3\f\3\f\3\f\7\f\u00b0\n\f\f\f\16\f\u00b3\13\f\3\f\3\f\3\f\3\f\3\f\6\f"+
		"\u00ba\n\f\r\f\16\f\u00bb\3\f\3\f\3\f\5\f\u00c1\n\f\3\r\3\r\3\r\3\r\3"+
		"\r\3\r\5\r\u00c9\n\r\3\16\3\16\5\16\u00cd\n\16\3\17\3\17\3\17\3\17\3\17"+
		"\3\17\7\17\u00d5\n\17\f\17\16\17\u00d8\13\17\5\17\u00da\n\17\3\20\3\20"+
		"\3\20\3\21\3\21\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\23\3\23\5\23\u00ea"+
		"\n\23\3\24\3\24\6\24\u00ee\n\24\r\24\16\24\u00ef\3\25\3\25\3\26\3\26\3"+
		"\26\5\26\u00f7\n\26\3\26\3\26\3\26\5\26\u00fc\n\26\3\26\3\26\3\26\5\26"+
		"\u0101\n\26\3\26\3\26\3\26\5\26\u0106\n\26\3\26\3\26\3\26\5\26\u010b\n"+
		"\26\3\26\3\26\3\26\5\26\u0110\n\26\3\26\3\26\3\26\5\26\u0115\n\26\3\26"+
		"\3\26\3\26\5\26\u011a\n\26\3\26\5\26\u011d\n\26\3\26\3\26\3\26\5\26\u0122"+
		"\n\26\3\26\5\26\u0125\n\26\3\26\3\26\3\26\5\26\u012a\n\26\3\26\5\26\u012d"+
		"\n\26\3\26\3\26\3\26\5\26\u0132\n\26\3\26\3\26\3\26\5\26\u0137\n\26\3"+
		"\26\3\26\3\26\5\26\u013c\n\26\3\26\3\26\3\26\5\26\u0141\n\26\3\26\3\26"+
		"\3\26\5\26\u0146\n\26\3\26\3\26\3\26\5\26\u014b\n\26\3\26\3\26\3\26\3"+
		"\26\5\26\u0151\n\26\3\26\3\26\3\26\5\26\u0156\n\26\3\26\3\26\5\26\u015a"+
		"\n\26\3\26\3\26\5\26\u015e\n\26\3\26\3\26\5\26\u0162\n\26\3\26\3\26\3"+
		"\26\5\26\u0167\n\26\3\26\3\26\3\26\5\26\u016c\n\26\3\26\3\26\3\26\5\26"+
		"\u0171\n\26\3\26\3\26\3\26\5\26\u0176\n\26\3\26\3\26\3\26\5\26\u017b\n"+
		"\26\3\26\3\26\3\26\5\26\u0180\n\26\3\26\3\26\3\26\5\26\u0185\n\26\3\26"+
		"\3\26\3\26\5\26\u018a\n\26\3\26\3\26\3\26\5\26\u018f\n\26\3\26\3\26\3"+
		"\26\5\26\u0194\n\26\3\26\3\26\3\26\5\26\u0199\n\26\3\26\3\26\3\26\5\26"+
		"\u019e\n\26\3\26\3\26\3\26\5\26\u01a3\n\26\3\26\3\26\3\26\5\26\u01a8\n"+
		"\26\3\26\3\26\3\26\5\26\u01ad\n\26\3\26\3\26\3\26\5\26\u01b2\n\26\3\26"+
		"\3\26\3\26\5\26\u01b7\n\26\3\26\3\26\3\26\5\26\u01bc\n\26\3\26\3\26\3"+
		"\26\5\26\u01c1\n\26\3\26\3\26\3\26\3\26\5\26\u01c7\n\26\3\26\3\26\3\26"+
		"\3\26\5\26\u01cd\n\26\3\26\3\26\3\26\3\26\5\26\u01d3\n\26\3\26\3\26\3"+
		"\26\5\26\u01d8\n\26\3\26\3\26\3\26\5\26\u01dd\n\26\3\26\3\26\3\26\3\26"+
		"\3\26\3\26\3\26\5\26\u01e6\n\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26"+
		"\3\26\3\26\3\26\5\26\u01f3\n\26\3\26\3\26\3\26\3\26\3\26\5\26\u01fa\n"+
		"\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3"+
		"\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3"+
		"\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\5\26\u0224"+
		"\n\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\5\26\u022f\n\26\3\26"+
		"\3\26\3\26\3\26\3\26\3\26\3\26\3\26\5\26\u0239\n\26\3\26\3\26\3\26\3\26"+
		"\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26"+
		"\5\26\u024d\n\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26"+
		"\3\26\5\26\u025b\n\26\3\26\3\26\3\26\3\26\5\26\u0261\n\26\3\26\3\26\3"+
		"\26\3\26\3\26\3\26\5\26\u0269\n\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26"+
		"\3\26\3\26\5\26\u0274\n\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\5\26"+
		"\u027e\n\26\3\26\5\26\u0281\n\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3"+
		"\26\3\26\3\26\5\26\u028d\n\26\3\26\3\26\5\26\u0291\n\26\3\26\3\26\3\26"+
		"\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26"+
		"\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26"+
		"\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26"+
		"\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26"+
		"\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26"+
		"\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\6\26\u02e4\n\26\r\26\16\26\u02e5"+
		"\3\26\3\26\3\26\5\26\u02eb\n\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26"+
		"\5\26\u02f5\n\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\5\26\u02ff\n"+
		"\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\5\26\u0309\n\26\3\26\3\26"+
		"\3\26\3\26\3\26\3\26\3\26\3\26\5\26\u0313\n\26\3\26\3\26\5\26\u0317\n"+
		"\26\3\26\3\26\3\26\3\26\3\26\5\26\u031e\n\26\3\26\3\26\3\26\3\26\3\26"+
		"\3\26\5\26\u0326\n\26\3\26\3\26\3\26\3\26\3\26\5\26\u032d\n\26\3\26\3"+
		"\26\3\26\3\26\3\26\5\26\u0334\n\26\3\26\3\26\3\26\3\26\3\26\5\26\u033b"+
		"\n\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\5\26\u0345\n\26\3\26\3\26"+
		"\3\26\3\26\3\26\3\26\3\26\3\26\5\26\u034f\n\26\3\26\3\26\3\26\3\26\3\26"+
		"\3\26\3\26\3\26\5\26\u0359\n\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26"+
		"\5\26\u0363\n\26\3\26\3\26\5\26\u0367\n\26\3\26\3\26\3\26\3\26\3\26\5"+
		"\26\u036e\n\26\3\26\3\26\3\26\3\26\3\26\3\26\5\26\u0376\n\26\3\26\3\26"+
		"\3\26\3\26\3\26\5\26\u037d\n\26\3\26\3\26\3\26\3\26\5\26\u0383\n\26\3"+
		"\26\5\26\u0386\n\26\3\26\3\26\3\26\3\26\3\26\5\26\u038d\n\26\3\26\3\26"+
		"\3\26\3\26\5\26\u0393\n\26\3\26\3\26\3\26\3\26\3\26\6\26\u039a\n\26\r"+
		"\26\16\26\u039b\7\26\u039e\n\26\f\26\16\26\u03a1\13\26\3\27\3\27\3\27"+
		"\3\27\3\27\7\27\u03a8\n\27\f\27\16\27\u03ab\13\27\3\27\3\27\3\30\3\30"+
		"\3\30\3\30\3\30\3\30\3\30\3\30\3\30\7\30\u03b8\n\30\f\30\16\30\u03bb\13"+
		"\30\3\30\3\30\3\30\2\31\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*"+
		",.\2\22\3\2;>\t\2UUWWhh\u0082\u0084\u0087\u0088\u008c\u008c\u00ae\u00ae"+
		"\4\2oovv\4\2ppww\4\2qqxx\4\2rryy\4\2sszz\4\2tt{{\4\2uu||\4\2opru\3\2."+
		"/\3\2]^\4\2UUnn\3\2@A\3\2QR\4\2\u00a9\u00a9\u00ad\u00ad\u04a5\2\63\3\2"+
		"\2\2\4\66\3\2\2\2\6J\3\2\2\2\bZ\3\2\2\2\n`\3\2\2\2\fh\3\2\2\2\16j\3\2"+
		"\2\2\20o\3\2\2\2\22t\3\2\2\2\24\u0095\3\2\2\2\26\u00c0\3\2\2\2\30\u00c8"+
		"\3\2\2\2\32\u00cc\3\2\2\2\34\u00ce\3\2\2\2\36\u00db\3\2\2\2 \u00de\3\2"+
		"\2\2\"\u00e0\3\2\2\2$\u00e9\3\2\2\2&\u00ed\3\2\2\2(\u00f1\3\2\2\2*\u0290"+
		"\3\2\2\2,\u03a2\3\2\2\2.\u03ae\3\2\2\2\60\62\5\24\13\2\61\60\3\2\2\2\62"+
		"\65\3\2\2\2\63\61\3\2\2\2\63\64\3\2\2\2\64\3\3\2\2\2\65\63\3\2\2\2\66"+
		"\67\7\f\2\2\678\5*\26\289\7\r\2\29A\5\2\2\2:;\7\16\2\2;<\5*\26\2<=\7\r"+
		"\2\2=>\5\2\2\2>@\3\2\2\2?:\3\2\2\2@C\3\2\2\2A?\3\2\2\2AB\3\2\2\2BF\3\2"+
		"\2\2CA\3\2\2\2DE\7\17\2\2EG\5\2\2\2FD\3\2\2\2FG\3\2\2\2GH\3\2\2\2HI\7"+
		"\20\2\2I\5\3\2\2\2JK\7\24\2\2KP\7\u00b0\2\2LM\7\25\2\2MN\5*\26\2NO\5\2"+
		"\2\2OQ\3\2\2\2PL\3\2\2\2QR\3\2\2\2RP\3\2\2\2RS\3\2\2\2SV\3\2\2\2TU\7\26"+
		"\2\2UW\5\2\2\2VT\3\2\2\2VW\3\2\2\2WX\3\2\2\2XY\7\27\2\2Y\7\3\2\2\2Z[\7"+
		"\21\2\2[\\\5*\26\2\\]\7\22\2\2]^\5\2\2\2^_\7\23\2\2_\t\3\2\2\2`a\7\u0097"+
		"\2\2ab\7\u00b0\2\2bc\7\u00a8\2\2cd\5*\26\2de\7\22\2\2ef\5\2\2\2fg\7\23"+
		"\2\2g\13\3\2\2\2hi\7\30\2\2i\r\3\2\2\2jk\7\31\2\2k\17\3\2\2\2ln\5\24\13"+
		"\2ml\3\2\2\2nq\3\2\2\2om\3\2\2\2op\3\2\2\2pr\3\2\2\2qo\3\2\2\2rs\7\2\2"+
		"\3s\21\3\2\2\2tu\7\u0096\2\2uv\5*\26\2v\23\3\2\2\2wx\5\26\f\2xy\7\n\2"+
		"\2y\u0096\3\2\2\2z{\5\4\3\2{|\7\n\2\2|\u0096\3\2\2\2}~\5\6\4\2~\177\7"+
		"\n\2\2\177\u0096\3\2\2\2\u0080\u0081\5\b\5\2\u0081\u0082\7\n\2\2\u0082"+
		"\u0096\3\2\2\2\u0083\u0084\5\n\6\2\u0084\u0085\7\n\2\2\u0085\u0096\3\2"+
		"\2\2\u0086\u0087\5\f\7\2\u0087\u0088\7\n\2\2\u0088\u0096\3\2\2\2\u0089"+
		"\u008a\5\16\b\2\u008a\u008b\7\n\2\2\u008b\u0096\3\2\2\2\u008c\u008d\5"+
		"\36\20\2\u008d\u008e\7\n\2\2\u008e\u0096\3\2\2\2\u008f\u0090\5\34\17\2"+
		"\u0090\u0091\7\n\2\2\u0091\u0096\3\2\2\2\u0092\u0093\5\22\n\2\u0093\u0094"+
		"\7\n\2\2\u0094\u0096\3\2\2\2\u0095w\3\2\2\2\u0095z\3\2\2\2\u0095}\3\2"+
		"\2\2\u0095\u0080\3\2\2\2\u0095\u0083\3\2\2\2\u0095\u0086\3\2\2\2\u0095"+
		"\u0089\3\2\2\2\u0095\u008c\3\2\2\2\u0095\u008f\3\2\2\2\u0095\u0092\3\2"+
		"\2\2\u0096\25\3\2\2\2\u0097\u0098\7\u00b0\2\2\u0098\u0099\7\7\2\2\u0099"+
		"\u00c1\5\32\16\2\u009a\u009b\7\u0098\2\2\u009b\u009c\7\u00b0\2\2\u009c"+
		"\u009d\7\u0099\2\2\u009d\u00c1\5\32\16\2\u009e\u009f\7\t\2\2\u009f\u00a4"+
		"\7\u00b0\2\2\u00a0\u00a1\7\6\2\2\u00a1\u00a3\7\u00b0\2\2\u00a2\u00a0\3"+
		"\2\2\2\u00a3\u00a6\3\2\2\2\u00a4\u00a2\3\2\2\2\u00a4\u00a5\3\2\2\2\u00a5"+
		"\u00a7\3\2\2\2\u00a6\u00a4\3\2\2\2\u00a7\u00a8\7\4\2\2\u00a8\u00a9\7\7"+
		"\2\2\u00a9\u00c1\5\32\16\2\u00aa\u00ab\7\u0098\2\2\u00ab\u00ac\7\t\2\2"+
		"\u00ac\u00b1\7\u00b0\2\2\u00ad\u00ae\7\6\2\2\u00ae\u00b0\7\u00b0\2\2\u00af"+
		"\u00ad\3\2\2\2\u00b0\u00b3\3\2\2\2\u00b1\u00af\3\2\2\2\u00b1\u00b2\3\2"+
		"\2\2\u00b2\u00b4\3\2\2\2\u00b3\u00b1\3\2\2\2\u00b4\u00b5\7\4\2\2\u00b5"+
		"\u00b6\7\u0099\2\2\u00b6\u00c1\5\32\16\2\u00b7\u00b9\7\u00b0\2\2\u00b8"+
		"\u00ba\5\30\r\2\u00b9\u00b8\3\2\2\2\u00ba\u00bb\3\2\2\2\u00bb\u00b9\3"+
		"\2\2\2\u00bb\u00bc\3\2\2\2\u00bc\u00bd\3\2\2\2\u00bd\u00be\7\7\2\2\u00be"+
		"\u00bf\5\32\16\2\u00bf\u00c1\3\2\2\2\u00c0\u0097\3\2\2\2\u00c0\u009a\3"+
		"\2\2\2\u00c0\u009e\3\2\2\2\u00c0\u00aa\3\2\2\2\u00c0\u00b7\3\2\2\2\u00c1"+
		"\27\3\2\2\2\u00c2\u00c3\7\5\2\2\u00c3\u00c9\7\u00b0\2\2\u00c4\u00c5\7"+
		"\b\2\2\u00c5\u00c6\5*\26\2\u00c6\u00c7\7\3\2\2\u00c7\u00c9\3\2\2\2\u00c8"+
		"\u00c2\3\2\2\2\u00c8\u00c4\3\2\2\2\u00c9\31\3\2\2\2\u00ca\u00cd\5*\26"+
		"\2\u00cb\u00cd\5\34\17\2\u00cc\u00ca\3\2\2\2\u00cc\u00cb\3\2\2\2\u00cd"+
		"\33\3\2\2\2\u00ce\u00cf\7\u00ab\2\2\u00cf\u00d9\7\u00b0\2\2\u00d0\u00d1"+
		"\7\u00ac\2\2\u00d1\u00d6\5*\26\2\u00d2\u00d3\7\6\2\2\u00d3\u00d5\5*\26"+
		"\2\u00d4\u00d2\3\2\2\2\u00d5\u00d8\3\2\2\2\u00d6\u00d4\3\2\2\2\u00d6\u00d7"+
		"\3\2\2\2\u00d7\u00da\3\2\2\2\u00d8\u00d6\3\2\2\2\u00d9\u00d0\3\2\2\2\u00d9"+
		"\u00da\3\2\2\2\u00da\35\3\2\2\2\u00db\u00dc\7\13\2\2\u00dc\u00dd\5*\26"+
		"\2\u00dd\37\3\2\2\2\u00de\u00df\t\2\2\2\u00df!\3\2\2\2\u00e0\u00e1\t\3"+
		"\2\2\u00e1#\3\2\2\2\u00e2\u00ea\t\4\2\2\u00e3\u00ea\t\5\2\2\u00e4\u00ea"+
		"\t\6\2\2\u00e5\u00ea\t\7\2\2\u00e6\u00ea\t\b\2\2\u00e7\u00ea\t\t\2\2\u00e8"+
		"\u00ea\t\n\2\2\u00e9\u00e2\3\2\2\2\u00e9\u00e3\3\2\2\2\u00e9\u00e4\3\2"+
		"\2\2\u00e9\u00e5\3\2\2\2\u00e9\u00e6\3\2\2\2\u00e9\u00e7\3\2\2\2\u00e9"+
		"\u00e8\3\2\2\2\u00ea%\3\2\2\2\u00eb\u00ec\7\u008d\2\2\u00ec\u00ee\5$\23"+
		"\2\u00ed\u00eb\3\2\2\2\u00ee\u00ef\3\2\2\2\u00ef\u00ed\3\2\2\2\u00ef\u00f0"+
		"\3\2\2\2\u00f0\'\3\2\2\2\u00f1\u00f2\t\13\2\2\u00f2)\3\2\2\2\u00f3\u00f4"+
		"\b\26\1\2\u00f4\u00f6\7D\2\2\u00f5\u00f7\7P\2\2\u00f6\u00f5\3\2\2\2\u00f6"+
		"\u00f7\3\2\2\2\u00f7\u00f8\3\2\2\2\u00f8\u0291\5*\26\2\u00f9\u00fb\7F"+
		"\2\2\u00fa\u00fc\7P\2\2\u00fb\u00fa\3\2\2\2\u00fb\u00fc\3\2\2\2\u00fc"+
		"\u00fd\3\2\2\2\u00fd\u0291\5*\26\2\u00fe\u0100\7G\2\2\u00ff\u0101\7P\2"+
		"\2\u0100\u00ff\3\2\2\2\u0100\u0101\3\2\2\2\u0101\u0102\3\2\2\2\u0102\u0291"+
		"\5*\26\2\u0103\u0105\7H\2\2\u0104\u0106\7P\2\2\u0105\u0104\3\2\2\2\u0105"+
		"\u0106\3\2\2\2\u0106\u0107\3\2\2\2\u0107\u0291\5*\26\2\u0108\u010a\7I"+
		"\2\2\u0109\u010b\7P\2\2\u010a\u0109\3\2\2\2\u010a\u010b\3\2\2\2\u010b"+
		"\u010c\3\2\2\2\u010c\u0291\5*\26\2\u010d\u010f\7J\2\2\u010e\u0110\7P\2"+
		"\2\u010f\u010e\3\2\2\2\u010f\u0110\3\2\2\2\u0110\u0111\3\2\2\2\u0111\u0291"+
		"\5*\26\2\u0112\u0114\7K\2\2\u0113\u0115\7P\2\2\u0114\u0113\3\2\2\2\u0114"+
		"\u0115\3\2\2\2\u0115\u0116\3\2\2\2\u0116\u0291\5*\26\2\u0117\u0119\7M"+
		"\2\2\u0118\u011a\7Q\2\2\u0119\u0118\3\2\2\2\u0119\u011a\3\2\2\2\u011a"+
		"\u011c\3\2\2\2\u011b\u011d\7P\2\2\u011c\u011b\3\2\2\2\u011c\u011d\3\2"+
		"\2\2\u011d\u011e\3\2\2\2\u011e\u0291\5*\26\2\u011f\u0121\7N\2\2\u0120"+
		"\u0122\7R\2\2\u0121\u0120\3\2\2\2\u0121\u0122\3\2\2\2\u0122\u0124\3\2"+
		"\2\2\u0123\u0125\7P\2\2\u0124\u0123\3\2\2\2\u0124\u0125\3\2\2\2\u0125"+
		"\u0126\3\2\2\2\u0126\u0291\5*\26\2\u0127\u0129\7O\2\2\u0128\u012a\7Q\2"+
		"\2\u0129\u0128\3\2\2\2\u0129\u012a\3\2\2\2\u012a\u012c\3\2\2\2\u012b\u012d"+
		"\7P\2\2\u012c\u012b\3\2\2\2\u012c\u012d\3\2\2\2\u012d\u012e\3\2\2\2\u012e"+
		"\u0291\5*\26\2\u012f\u0131\7;\2\2\u0130\u0132\7P\2\2\u0131\u0130\3\2\2"+
		"\2\u0131\u0132\3\2\2\2\u0132\u0133\3\2\2\2\u0133\u0291\5*\26\2\u0134\u0136"+
		"\7<\2\2\u0135\u0137\7P\2\2\u0136\u0135\3\2\2\2\u0136\u0137\3\2\2\2\u0137"+
		"\u0138\3\2\2\2\u0138\u0291\5*\26\2\u0139\u013b\7B\2\2\u013a\u013c\7P\2"+
		"\2\u013b\u013a\3\2\2\2\u013b\u013c\3\2\2\2\u013c\u013d\3\2\2\2\u013d\u0291"+
		"\5*\26\2\u013e\u0140\7C\2\2\u013f\u0141\7P\2\2\u0140\u013f\3\2\2\2\u0140"+
		"\u0141\3\2\2\2\u0141\u0142\3\2\2\2\u0142\u0291\5*\26\2\u0143\u0145\7="+
		"\2\2\u0144\u0146\7P\2\2\u0145\u0144\3\2\2\2\u0145\u0146\3\2\2\2\u0146"+
		"\u0147\3\2\2\2\u0147\u0291\5*\26\2\u0148\u014a\7>\2\2\u0149\u014b\7P\2"+
		"\2\u014a\u0149\3\2\2\2\u014a\u014b\3\2\2\2\u014b\u014c\3\2\2\2\u014c\u0291"+
		"\5*\26\2\u014d\u014e\7L\2\2\u014e\u0150\5 \21\2\u014f\u0151\7P\2\2\u0150"+
		"\u014f\3\2\2\2\u0150\u0151\3\2\2\2\u0151\u0152\3\2\2\2\u0152\u0153\5*"+
		"\26\2\u0153\u0291\3\2\2\2\u0154\u0156\7e\2\2\u0155\u0154\3\2\2\2\u0155"+
		"\u0156\3\2\2\2\u0156\u0157\3\2\2\2\u0157\u0159\7f\2\2\u0158\u015a\7P\2"+
		"\2\u0159\u0158\3\2\2\2\u0159\u015a\3\2\2\2\u015a\u015b\3\2\2\2\u015b\u0291"+
		"\5*\26\2\u015c\u015e\7e\2\2\u015d\u015c\3\2\2\2\u015d\u015e\3\2\2\2\u015e"+
		"\u015f\3\2\2\2\u015f\u0161\7g\2\2\u0160\u0162\7P\2\2\u0161\u0160\3\2\2"+
		"\2\u0161\u0162\3\2\2\2\u0162\u0163\3\2\2\2\u0163\u0291\5*\26\2\u0164\u0166"+
		"\7E\2\2\u0165\u0167\7P\2\2\u0166\u0165\3\2\2\2\u0166\u0167\3\2\2\2\u0167"+
		"\u0168\3\2\2\2\u0168\u0291\5*\26\2\u0169\u016b\7U\2\2\u016a\u016c\7P\2"+
		"\2\u016b\u016a\3\2\2\2\u016b\u016c\3\2\2\2\u016c\u016d\3\2\2\2\u016d\u0291"+
		"\5*\26\2\u016e\u0170\7\u0092\2\2\u016f\u0171\7P\2\2\u0170\u016f\3\2\2"+
		"\2\u0170\u0171\3\2\2\2\u0171\u0172\3\2\2\2\u0172\u0291\5*\26\2\u0173\u0175"+
		"\7\u0094\2\2\u0174\u0176\7P\2\2\u0175\u0174\3\2\2\2\u0175\u0176\3\2\2"+
		"\2\u0176\u0177\3\2\2\2\u0177\u0291\5*\26\2\u0178\u017a\7%\2\2\u0179\u017b"+
		"\7P\2\2\u017a\u0179\3\2\2\2\u017a\u017b\3\2\2\2\u017b\u017c\3\2\2\2\u017c"+
		"\u0291\5*\26\2\u017d\u017f\7&\2\2\u017e\u0180\7P\2\2\u017f\u017e\3\2\2"+
		"\2\u017f\u0180\3\2\2\2\u0180\u0181\3\2\2\2\u0181\u0291\5*\26\2\u0182\u0184"+
		"\7\'\2\2\u0183\u0185\7P\2\2\u0184\u0183\3\2\2\2\u0184\u0185\3\2\2\2\u0185"+
		"\u0186\3\2\2\2\u0186\u0291\5*\26\2\u0187\u0189\7(\2\2\u0188\u018a\7P\2"+
		"\2\u0189\u0188\3\2\2\2\u0189\u018a\3\2\2\2\u018a\u018b\3\2\2\2\u018b\u0291"+
		"\5*\26\2\u018c\u018e\7)\2\2\u018d\u018f\7P\2\2\u018e\u018d\3\2\2\2\u018e"+
		"\u018f\3\2\2\2\u018f\u0190\3\2\2\2\u0190\u0291\5*\26\2\u0191\u0193\7*"+
		"\2\2\u0192\u0194\7P\2\2\u0193\u0192\3\2\2\2\u0193\u0194\3\2\2\2\u0194"+
		"\u0195\3\2\2\2\u0195\u0291\5*\26\2\u0196\u0198\7+\2\2\u0197\u0199\7P\2"+
		"\2\u0198\u0197\3\2\2\2\u0198\u0199\3\2\2\2\u0199\u019a\3\2\2\2\u019a\u0291"+
		"\5*\26\2\u019b\u019d\7,\2\2\u019c\u019e\7P\2\2\u019d\u019c\3\2\2\2\u019d"+
		"\u019e\3\2\2\2\u019e\u019f\3\2\2\2\u019f\u0291\5*\26\2\u01a0\u01a2\7-"+
		"\2\2\u01a1\u01a3\7P\2\2\u01a2\u01a1\3\2\2\2\u01a2\u01a3\3\2\2\2\u01a3"+
		"\u01a4\3\2\2\2\u01a4\u0291\5*\26\2\u01a5\u01a7\t\f\2\2\u01a6\u01a8\7P"+
		"\2\2\u01a7\u01a6\3\2\2\2\u01a7\u01a8\3\2\2\2\u01a8\u01a9\3\2\2\2\u01a9"+
		"\u0291\5*\26\2\u01aa\u01ac\7\60\2\2\u01ab\u01ad\7P\2\2\u01ac\u01ab\3\2"+
		"\2\2\u01ac\u01ad\3\2\2\2\u01ad\u01ae\3\2\2\2\u01ae\u0291\5*\26\2\u01af"+
		"\u01b1\7\61\2\2\u01b0\u01b2\7P\2\2\u01b1\u01b0\3\2\2\2\u01b1\u01b2\3\2"+
		"\2\2\u01b2\u01b3\3\2\2\2\u01b3\u0291\5*\26\2\u01b4\u01b6\7\62\2\2\u01b5"+
		"\u01b7\7P\2\2\u01b6\u01b5\3\2\2\2\u01b6\u01b7\3\2\2\2\u01b7\u01b8\3\2"+
		"\2\2\u01b8\u0291\5*\26\2\u01b9\u01bb\7\63\2\2\u01ba\u01bc\7P\2\2\u01bb"+
		"\u01ba\3\2\2\2\u01bb\u01bc\3\2\2\2\u01bc\u01bd\3\2\2\2\u01bd\u0291\5*"+
		"\26\2\u01be\u01c0\7\64\2\2\u01bf\u01c1\7P\2\2\u01c0\u01bf\3\2\2\2\u01c0"+
		"\u01c1\3\2\2\2\u01c1\u01c2\3\2\2\2\u01c2\u0291\5*\26\2\u01c3\u01c4\7\u0081"+
		"\2\2\u01c4\u01c6\5(\25\2\u01c5\u01c7\7P\2\2\u01c6\u01c5\3\2\2\2\u01c6"+
		"\u01c7\3\2\2\2\u01c7\u01c8\3\2\2\2\u01c8\u01c9\5*\26\2\u01c9\u0291\3\2"+
		"\2\2\u01ca\u01cc\7\u0089\2\2\u01cb\u01cd\7P\2\2\u01cc\u01cb\3\2\2\2\u01cc"+
		"\u01cd\3\2\2\2\u01cd\u01ce\3\2\2\2\u01ce\u0291\5*\26\2\u01cf\u01d0\7\u0081"+
		"\2\2\u01d0\u01d2\7b\2\2\u01d1\u01d3\7P\2\2\u01d2\u01d1\3\2\2\2\u01d2\u01d3"+
		"\3\2\2\2\u01d3\u01d4\3\2\2\2\u01d4\u0291\5*\26\2\u01d5\u01d7\7W\2\2\u01d6"+
		"\u01d8\7P\2\2\u01d7\u01d6\3\2\2\2\u01d7\u01d8\3\2\2\2\u01d8\u01d9\3\2"+
		"\2\2\u01d9\u0291\5*\26\2\u01da\u01dc\7Y\2\2\u01db\u01dd\7P\2\2\u01dc\u01db"+
		"\3\2\2\2\u01dc\u01dd\3\2\2\2\u01dd\u01de\3\2\2\2\u01de\u0291\5*\26\2\u01df"+
		"\u01e0\7\u0081\2\2\u01e0\u01e1\7\177\2\2\u01e1\u01e2\7\u0080\2\2\u01e2"+
		"\u0291\5*\26\2\u01e3\u01e5\7~\2\2\u01e4\u01e6\7P\2\2\u01e5\u01e4\3\2\2"+
		"\2\u01e5\u01e6\3\2\2\2\u01e6\u01e7\3\2\2\2\u01e7\u0291\5*\26\2\u01e8\u01e9"+
		"\7 \2\2\u01e9\u0291\5*\26\2\u01ea\u01eb\7!\2\2\u01eb\u0291\5*\26\2\u01ec"+
		"\u01ed\7Z\2\2\u01ed\u0291\5*\26\2\u01ee\u01ef\7[\2\2\u01ef\u0291\5*\26"+
		"\2\u01f0\u01f2\7\\\2\2\u01f1\u01f3\t\r\2\2\u01f2\u01f1\3\2\2\2\u01f2\u01f3"+
		"\3\2\2\2\u01f3\u01f4\3\2\2\2\u01f4\u0291\5*\26\2\u01f5\u01f6\7\u00a7\2"+
		"\2\u01f6\u0291\5*\26\2\u01f7\u01f9\7m\2\2\u01f8\u01fa\t\16\2\2\u01f9\u01f8"+
		"\3\2\2\2\u01f9\u01fa\3\2\2\2\u01fa\u01fb\3\2\2\2\u01fb\u0291\5*\26\2\u01fc"+
		"\u01fd\7\6\2\2\u01fd\u0291\5*\26\2\u01fe\u01ff\7\t\2\2\u01ff\u0200\b\26"+
		"\1\2\u0200\u0201\5*\26\2\u0201\u0202\b\26\1\2\u0202\u0203\7\4\2\2\u0203"+
		"\u0291\3\2\2\2\u0204\u0205\7;\2\2\u0205\u0206\5*\26\2\u0206\u0207\7\u00aa"+
		"\2\2\u0207\u0208\5*\26\2\u0208\u0291\3\2\2\2\u0209\u020a\7<\2\2\u020a"+
		"\u020b\5*\26\2\u020b\u020c\7\u00aa\2\2\u020c\u020d\5*\26\2\u020d\u0291"+
		"\3\2\2\2\u020e\u020f\7=\2\2\u020f\u0210\5*\26\2\u0210\u0211\7\u00aa\2"+
		"\2\u0211\u0212\5*\26\2\u0212\u0291\3\2\2\2\u0213\u0214\7>\2\2\u0214\u0215"+
		"\5*\26\2\u0215\u0216\7\u00aa\2\2\u0216\u0217\5*\26\2\u0217\u0291\3\2\2"+
		"\2\u0218\u0219\7B\2\2\u0219\u021a\5*\26\2\u021a\u021b\7\u00aa\2\2\u021b"+
		"\u021c\5*\26\2\u021c\u0291\3\2\2\2\u021d\u021e\7C\2\2\u021e\u021f\5*\26"+
		"\2\u021f\u0220\7\u00aa\2\2\u0220\u0221\5*\26\2\u0221\u0291\3\2\2\2\u0222"+
		"\u0224\7L\2\2\u0223\u0222\3\2\2\2\u0223\u0224\3\2\2\2\u0224\u0225\3\2"+
		"\2\2\u0225\u0226\7?\2\2\u0226\u0227\5*\26\2\u0227\u0228\7\u00aa\2\2\u0228"+
		"\u0229\5*\26\2\u0229\u0291\3\2\2\2\u022a\u022b\7\u00a9\2\2\u022b\u022c"+
		"\t\17\2\2\u022c\u022e\5*\26\2\u022d\u022f\t\20\2\2\u022e\u022d\3\2\2\2"+
		"\u022e\u022f\3\2\2\2\u022f\u0230\3\2\2\2\u0230\u0231\7\u00aa\2\2\u0231"+
		"\u0232\5*\26\2\u0232\u0291\3\2\2\2\u0233\u0234\7i\2\2\u0234\u0235\5*\26"+
		"\2\u0235\u0238\7d\2\2\u0236\u0237\7`\2\2\u0237\u0239\5*\26\2\u0238\u0236"+
		"\3\2\2\2\u0238\u0239\3\2\2\2\u0239\u023a\3\2\2\2\u023a\u023b\7\u00aa\2"+
		"\2\u023b\u023c\5*\26\2\u023c\u0291\3\2\2\2\u023d\u023e\7L\2\2\u023e\u023f"+
		"\7P\2\2\u023f\u0240\5*\26\2\u0240\u0241\7\u00aa\2\2\u0241\u0242\5*\26"+
		"\2\u0242\u0291\3\2\2\2\u0243\u0244\7L\2\2\u0244\u0245\5 \21\2\u0245\u0246"+
		"\5*\26\2\u0246\u0247\7\u00aa\2\2\u0247\u0248\5*\26\2\u0248\u0291\3\2\2"+
		"\2\u0249\u024a\7\u0086\2\2\u024a\u024c\5(\25\2\u024b\u024d\7P\2\2\u024c"+
		"\u024b\3\2\2\2\u024c\u024d\3\2\2\2\u024d\u024e\3\2\2\2\u024e\u024f\5*"+
		"\26\2\u024f\u0250\7\u00ac\2\2\u0250\u0251\5*\26\2\u0251\u0291\3\2\2\2"+
		"\u0252\u0253\7\177\2\2\u0253\u0254\5*\26\2\u0254\u0255\7\u00aa\2\2\u0255"+
		"\u0256\5*\26\2\u0256\u0291\3\2\2\2\u0257\u0258\7_\2\2\u0258\u025a\5*\26"+
		"\2\u0259\u025b\7\u00a8\2\2\u025a\u0259\3\2\2\2\u025a\u025b\3\2\2\2\u025b"+
		"\u025c\3\2\2\2\u025c\u025d\7W\2\2\u025d\u0260\5*\26\2\u025e\u025f\7`\2"+
		"\2\u025f\u0261\5*\26\2\u0260\u025e\3\2\2\2\u0260\u0261\3\2\2\2\u0261\u0291"+
		"\3\2\2\2\u0262\u0291\5&\24\2\u0263\u0264\7a\2\2\u0264\u0265\5*\26\2\u0265"+
		"\u0268\7b\2\2\u0266\u0267\7`\2\2\u0267\u0269\5*\26\2\u0268\u0266\3\2\2"+
		"\2\u0268\u0269\3\2\2\2\u0269\u026a\3\2\2\2\u026a\u026b\7\u00aa\2\2\u026b"+
		"\u026c\5*\26\2\u026c\u0291\3\2\2\2\u026d\u026e\7c\2\2\u026e\u026f\5*\26"+
		"\2\u026f\u0270\7\u009e\2\2\u0270\u0273\5*\26\2\u0271\u0272\7\u00a9\2\2"+
		"\u0272\u0274\5*\26\2\u0273\u0271\3\2\2\2\u0273\u0274\3\2\2\2\u0274\u0291"+
		"\3\2\2\2\u0275\u0276\7j\2\2\u0276\u0277\5*\26\2\u0277\u0278\7\u00aa\2"+
		"\2\u0278\u0279\5*\26\2\u0279\u0291\3\2\2\2\u027a\u027b\7}\2\2\u027b\u027d"+
		"\7\u00b0\2\2\u027c\u027e\5,\27\2\u027d\u027c\3\2\2\2\u027d\u027e\3\2\2"+
		"\2\u027e\u0280\3\2\2\2\u027f\u0281\5.\30\2\u0280\u027f\3\2\2\2\u0280\u0281"+
		"\3\2\2\2\u0281\u0291\3\2\2\2\u0282\u0291\7\u008d\2\2\u0283\u0291\7\u0085"+
		"\2\2\u0284\u0291\7\u008f\2\2\u0285\u0291\7\u0090\2\2\u0286\u0291\7\u0091"+
		"\2\2\u0287\u0291\7\u0093\2\2\u0288\u0291\7\u0095\2\2\u0289\u0291\7\u0083"+
		"\2\2\u028a\u0291\7\u00af\2\2\u028b\u028d\7\u009d\2\2\u028c\u028b\3\2\2"+
		"\2\u028c\u028d\3\2\2\2\u028d\u028e\3\2\2\2\u028e\u0291\7\u00b0\2\2\u028f"+
		"\u0291\7\u008a\2\2\u0290\u00f3\3\2\2\2\u0290\u00f9\3\2\2\2\u0290\u00fe"+
		"\3\2\2\2\u0290\u0103\3\2\2\2\u0290\u0108\3\2\2\2\u0290\u010d\3\2\2\2\u0290"+
		"\u0112\3\2\2\2\u0290\u0117\3\2\2\2\u0290\u011f\3\2\2\2\u0290\u0127\3\2"+
		"\2\2\u0290\u012f\3\2\2\2\u0290\u0134\3\2\2\2\u0290\u0139\3\2\2\2\u0290"+
		"\u013e\3\2\2\2\u0290\u0143\3\2\2\2\u0290\u0148\3\2\2\2\u0290\u014d\3\2"+
		"\2\2\u0290\u0155\3\2\2\2\u0290\u015d\3\2\2\2\u0290\u0164\3\2\2\2\u0290"+
		"\u0169\3\2\2\2\u0290\u016e\3\2\2\2\u0290\u0173\3\2\2\2\u0290\u0178\3\2"+
		"\2\2\u0290\u017d\3\2\2\2\u0290\u0182\3\2\2\2\u0290\u0187\3\2\2\2\u0290"+
		"\u018c\3\2\2\2\u0290\u0191\3\2\2\2\u0290\u0196\3\2\2\2\u0290\u019b\3\2"+
		"\2\2\u0290\u01a0\3\2\2\2\u0290\u01a5\3\2\2\2\u0290\u01aa\3\2\2\2\u0290"+
		"\u01af\3\2\2\2\u0290\u01b4\3\2\2\2\u0290\u01b9\3\2\2\2\u0290\u01be\3\2"+
		"\2\2\u0290\u01c3\3\2\2\2\u0290\u01ca\3\2\2\2\u0290\u01cf\3\2\2\2\u0290"+
		"\u01d5\3\2\2\2\u0290\u01da\3\2\2\2\u0290\u01df\3\2\2\2\u0290\u01e3\3\2"+
		"\2\2\u0290\u01e8\3\2\2\2\u0290\u01ea\3\2\2\2\u0290\u01ec\3\2\2\2\u0290"+
		"\u01ee\3\2\2\2\u0290\u01f0\3\2\2\2\u0290\u01f5\3\2\2\2\u0290\u01f7\3\2"+
		"\2\2\u0290\u01fc\3\2\2\2\u0290\u01fe\3\2\2\2\u0290\u0204\3\2\2\2\u0290"+
		"\u0209\3\2\2\2\u0290\u020e\3\2\2\2\u0290\u0213\3\2\2\2\u0290\u0218\3\2"+
		"\2\2\u0290\u021d\3\2\2\2\u0290\u0223\3\2\2\2\u0290\u022a\3\2\2\2\u0290"+
		"\u0233\3\2\2\2\u0290\u023d\3\2\2\2\u0290\u0243\3\2\2\2\u0290\u0249\3\2"+
		"\2\2\u0290\u0252\3\2\2\2\u0290\u0257\3\2\2\2\u0290\u0262\3\2\2\2\u0290"+
		"\u0263\3\2\2\2\u0290\u026d\3\2\2\2\u0290\u0275\3\2\2\2\u0290\u027a\3\2"+
		"\2\2\u0290\u0282\3\2\2\2\u0290\u0283\3\2\2\2\u0290\u0284\3\2\2\2\u0290"+
		"\u0285\3\2\2\2\u0290\u0286\3\2\2\2\u0290\u0287\3\2\2\2\u0290\u0288\3\2"+
		"\2\2\u0290\u0289\3\2\2\2\u0290\u028a\3\2\2\2\u0290\u028c\3\2\2\2\u0290"+
		"\u028f\3\2\2\2\u0291\u039f\3\2\2\2\u0292\u0293\6\26\2\3\u0293\u0294\7"+
		"\34\2\2\u0294\u039e\5*\26\2\u0295\u0296\6\26\3\3\u0296\u0297\7X\2\2\u0297"+
		"\u039e\5*\26\2\u0298\u0299\6\26\4\3\u0299\u029a\7T\2\2\u029a\u039e\5*"+
		"\26\2\u029b\u029c\6\26\5\3\u029c\u029d\7\u00a4\2\2\u029d\u039e\5*\26\2"+
		"\u029e\u029f\6\26\6\3\u029f\u02a0\7\u00a5\2\2\u02a0\u039e\5*\26\2\u02a1"+
		"\u02a2\6\26\7\3\u02a2\u02a3\7$\2\2\u02a3\u039e\5*\26\2\u02a4\u02a5\6\26"+
		"\b\3\u02a5\u02a6\7\"\2\2\u02a6\u039e\5*\26\2\u02a7\u02a8\6\26\t\3\u02a8"+
		"\u02a9\7#\2\2\u02a9\u039e\5*\26\2\u02aa\u02ab\6\26\n\3\u02ab\u02ac\7 "+
		"\2\2\u02ac\u039e\5*\26\2\u02ad\u02ae\6\26\13\3\u02ae\u02af\7!\2\2\u02af"+
		"\u039e\5*\26\2\u02b0\u02b1\6\26\f\3\u02b1\u02b2\7\67\2\2\u02b2\u039e\5"+
		"*\26\2\u02b3\u02b4\6\26\r\3\u02b4\u02b5\78\2\2\u02b5\u039e\5*\26\2\u02b6"+
		"\u02b7\6\26\16\3\u02b7\u02b8\7\65\2\2\u02b8\u039e\5*\26\2\u02b9\u02ba"+
		"\6\26\17\3\u02ba\u02bb\7\66\2\2\u02bb\u039e\5*\26\2\u02bc\u02bd\6\26\20"+
		"\3\u02bd\u02be\7:\2\2\u02be\u039e\5*\26\2\u02bf\u02c0\6\26\21\3\u02c0"+
		"\u02c1\79\2\2\u02c1\u039e\5*\26\2\u02c2\u02c3\6\26\22\3\u02c3\u02c4\7"+
		"\32\2\2\u02c4\u039e\5*\26\2\u02c5\u02c6\6\26\23\3\u02c6\u02c7\7\33\2\2"+
		"\u02c7\u039e\5*\26\2\u02c8\u02c9\6\26\24\3\u02c9\u02ca\7\u009b\2\2\u02ca"+
		"\u039e\5*\26\2\u02cb\u02cc\6\26\25\3\u02cc\u02cd\7k\2\2\u02cd\u039e\5"+
		"*\26\2\u02ce\u02cf\6\26\26\3\u02cf\u039e\7\35\2\2\u02d0\u02d1\6\26\27"+
		"\3\u02d1\u039e\7\36\2\2\u02d2\u02d3\6\26\30\3\u02d3\u039e\7\37\2\2\u02d4"+
		"\u02d5\6\26\31\3\u02d5\u02d6\7\5\2\2\u02d6\u039e\7\u00b0\2\2\u02d7\u02d8"+
		"\6\26\32\3\u02d8\u02d9\7\b\2\2\u02d9\u02da\5*\26\2\u02da\u02db\7\3\2\2"+
		"\u02db\u039e\3\2\2\2\u02dc\u02dd\6\26\33\3\u02dd\u039e\7l\2\2\u02de\u02df"+
		"\6\26\34\3\u02df\u039e\7S\2\2\u02e0\u02e3\6\26\35\3\u02e1\u02e2\7\u008b"+
		"\2\2\u02e2\u02e4\5*\26\2\u02e3\u02e1\3\2\2\2\u02e4\u02e5\3\2\2\2\u02e5"+
		"\u02e3\3\2\2\2\u02e5\u02e6\3\2\2\2\u02e6\u039e\3\2\2\2\u02e7\u02e8\6\26"+
		"\36\3\u02e8\u02ea\7V\2\2\u02e9\u02eb\7\u00a7\2\2\u02ea\u02e9\3\2\2\2\u02ea"+
		"\u02eb\3\2\2\2\u02eb\u02ec\3\2\2\2\u02ec\u02ed\7\u009c\2\2\u02ed\u02ee"+
		"\5*\26\2\u02ee\u02ef\7\u009e\2\2\u02ef\u02f0\5*\26\2\u02f0\u039e\3\2\2"+
		"\2\u02f1\u02f2\6\26\37\3\u02f2\u02f4\7V\2\2\u02f3\u02f5\7\u00a7\2\2\u02f4"+
		"\u02f3\3\2\2\2\u02f4\u02f5\3\2\2\2\u02f5\u02f6\3\2\2\2\u02f6\u02f7\7\u009c"+
		"\2\2\u02f7\u02f8\5*\26\2\u02f8\u02f9\7\u009f\2\2\u02f9\u02fa\5*\26\2\u02fa"+
		"\u039e\3\2\2\2\u02fb\u02fc\6\26 \3\u02fc\u02fe\7V\2\2\u02fd\u02ff\7\u00a7"+
		"\2\2\u02fe\u02fd\3\2\2\2\u02fe\u02ff\3\2\2\2\u02ff\u0300\3\2\2\2\u0300"+
		"\u0301\7\u009c\2\2\u0301\u0302\5*\26\2\u0302\u0303\7\u00a0\2\2\u0303\u0304"+
		"\5*\26\2\u0304\u039e\3\2\2\2\u0305\u0306\6\26!\3\u0306\u0308\7V\2\2\u0307"+
		"\u0309\7\u00a7\2\2\u0308\u0307\3\2\2\2\u0308\u0309\3\2\2\2\u0309\u030a"+
		"\3\2\2\2\u030a\u030b\7\u009c\2\2\u030b\u030c\5*\26\2\u030c\u030d\7\u00a1"+
		"\2\2\u030d\u030e\5*\26\2\u030e\u039e\3\2\2\2\u030f\u0310\6\26\"\3\u0310"+
		"\u0312\7V\2\2\u0311\u0313\7\u00a7\2\2\u0312\u0311\3\2\2\2\u0312\u0313"+
		"\3\2\2\2\u0313\u0314\3\2\2\2\u0314\u0316\7\u009c\2\2\u0315\u0317\7\u009d"+
		"\2\2\u0316\u0315\3\2\2\2\u0316\u0317\3\2\2\2\u0317\u0318\3\2\2\2\u0318"+
		"\u0319\7\u00a2\2\2\u0319\u039e\5*\26\2\u031a\u031b\6\26#\3\u031b\u031d"+
		"\7V\2\2\u031c\u031e\7\u00a7\2\2\u031d\u031c\3\2\2\2\u031d\u031e\3\2\2"+
		"\2\u031e\u031f\3\2\2\2\u031f\u0320\7\u009c\2\2\u0320\u0321\7\u00a3\2\2"+
		"\u0321\u039e\5*\26\2\u0322\u0323\6\26$\3\u0323\u0325\7V\2\2\u0324\u0326"+
		"\7\u00a7\2\2\u0325\u0324\3\2\2\2\u0325\u0326\3\2\2\2\u0326\u0327\3\2\2"+
		"\2\u0327\u0328\7\u00a4\2\2\u0328\u039e\5*\26\2\u0329\u032a\6\26%\3\u032a"+
		"\u032c\7V\2\2\u032b\u032d\7\u00a7\2\2\u032c\u032b\3\2\2\2\u032c\u032d"+
		"\3\2\2\2\u032d\u032e\3\2\2\2\u032e\u032f\7\u00a5\2\2\u032f\u039e\5*\26"+
		"\2\u0330\u0331\6\26&\3\u0331\u0333\7\u00a6\2\2\u0332\u0334\7\u00a7\2\2"+
		"\u0333\u0332\3\2\2\2\u0333\u0334\3\2\2\2\u0334\u0335\3\2\2\2\u0335\u0336"+
		"\t\21\2\2\u0336\u039e\5*\26\2\u0337\u0338\6\26\'\3\u0338\u033a\7\u00a6"+
		"\2\2\u0339\u033b\7\u00a7\2\2\u033a\u0339\3\2\2\2\u033a\u033b\3\2\2\2\u033b"+
		"\u033c\3\2\2\2\u033c\u033d\7\u009c\2\2\u033d\u033e\5*\26\2\u033e\u033f"+
		"\7\u009e\2\2\u033f\u0340\5*\26\2\u0340\u039e\3\2\2\2\u0341\u0342\6\26"+
		"(\3\u0342\u0344\7\u00a6\2\2\u0343\u0345\7\u00a7\2\2\u0344\u0343\3\2\2"+
		"\2\u0344\u0345\3\2\2\2\u0345\u0346\3\2\2\2\u0346\u0347\7\u009c\2\2\u0347"+
		"\u0348\5*\26\2\u0348\u0349\7\u009f\2\2\u0349\u034a\5*\26\2\u034a\u039e"+
		"\3\2\2\2\u034b\u034c\6\26)\3\u034c\u034e\7\u00a6\2\2\u034d\u034f\7\u00a7"+
		"\2\2\u034e\u034d\3\2\2\2\u034e\u034f\3\2\2\2\u034f\u0350\3\2\2\2\u0350"+
		"\u0351\7\u009c\2\2\u0351\u0352\5*\26\2\u0352\u0353\7\u00a0\2\2\u0353\u0354"+
		"\5*\26\2\u0354\u039e\3\2\2\2\u0355\u0356\6\26*\3\u0356\u0358\7\u00a6\2"+
		"\2\u0357\u0359\7\u00a7\2\2\u0358\u0357\3\2\2\2\u0358\u0359\3\2\2\2\u0359"+
		"\u035a\3\2\2\2\u035a\u035b\7\u009c\2\2\u035b\u035c\5*\26\2\u035c\u035d"+
		"\7\u00a1\2\2\u035d\u035e\5*\26\2\u035e\u039e\3\2\2\2\u035f\u0360\6\26"+
		"+\3\u0360\u0362\7\u00a6\2\2\u0361\u0363\7\u00a7\2\2\u0362\u0361\3\2\2"+
		"\2\u0362\u0363\3\2\2\2\u0363\u0364\3\2\2\2\u0364\u0366\7\u009c\2\2\u0365"+
		"\u0367\7\u009d\2\2\u0366\u0365\3\2\2\2\u0366\u0367\3\2\2\2\u0367\u0368"+
		"\3\2\2\2\u0368\u0369\7\u00a2\2\2\u0369\u039e\5*\26\2\u036a\u036b\6\26"+
		",\3\u036b\u036d\7\u00a6\2\2\u036c\u036e\7\u00a7\2\2\u036d\u036c\3\2\2"+
		"\2\u036d\u036e\3\2\2\2\u036e\u036f\3\2\2\2\u036f\u0370\7\u009c\2\2\u0370"+
		"\u0371\7\u00a3\2\2\u0371\u039e\5*\26\2\u0372\u0373\6\26-\3\u0373\u0375"+
		"\7\u00a6\2\2\u0374\u0376\7\u00a7\2\2\u0375\u0374\3\2\2\2\u0375\u0376\3"+
		"\2\2\2\u0376\u0377\3\2\2\2\u0377\u0378\7\u00a4\2\2\u0378\u039e\5*\26\2"+
		"\u0379\u037a\6\26.\3\u037a\u037c\7\u00a6\2\2\u037b\u037d\7\u00a7\2\2\u037c"+
		"\u037b\3\2\2\2\u037c\u037d\3\2\2\2\u037d\u037e\3\2\2\2\u037e\u037f\7\u00a5"+
		"\2\2\u037f\u039e\5*\26\2\u0380\u0382\6\26/\3\u0381\u0383\7V\2\2\u0382"+
		"\u0381\3\2\2\2\u0382\u0383\3\2\2\2\u0383\u0385\3\2\2\2\u0384\u0386\7\u00a7"+
		"\2\2\u0385\u0384\3\2\2\2\u0385\u0386\3\2\2\2\u0386\u0387\3\2\2\2\u0387"+
		"\u0388\7\u00a8\2\2\u0388\u039e\5*\26\2\u0389\u038a\6\26\60\3\u038a\u038c"+
		"\7V\2\2\u038b\u038d\7\u00a7\2\2\u038c\u038b\3\2\2\2\u038c\u038d\3\2\2"+
		"\2\u038d\u038e\3\2\2\2\u038e\u039e\5\"\22\2\u038f\u0390\6\26\61\3\u0390"+
		"\u0392\7V\2\2\u0391\u0393\7\u00a7\2\2\u0392\u0391\3\2\2\2\u0392\u0393"+
		"\3\2\2\2\u0393\u0394\3\2\2\2\u0394\u039e\5*\26\2\u0395\u0396\6\26\62\3"+
		"\u0396\u0399\6\26\63\2\u0397\u0398\7\6\2\2\u0398\u039a\5*\26\2\u0399\u0397"+
		"\3\2\2\2\u039a\u039b\3\2\2\2\u039b\u0399\3\2\2\2\u039b\u039c\3\2\2\2\u039c"+
		"\u039e\3\2\2\2\u039d\u0292\3\2\2\2\u039d\u0295\3\2\2\2\u039d\u0298\3\2"+
		"\2\2\u039d\u029b\3\2\2\2\u039d\u029e\3\2\2\2\u039d\u02a1\3\2\2\2\u039d"+
		"\u02a4\3\2\2\2\u039d\u02a7\3\2\2\2\u039d\u02aa\3\2\2\2\u039d\u02ad\3\2"+
		"\2\2\u039d\u02b0\3\2\2\2\u039d\u02b3\3\2\2\2\u039d\u02b6\3\2\2\2\u039d"+
		"\u02b9\3\2\2\2\u039d\u02bc\3\2\2\2\u039d\u02bf\3\2\2\2\u039d\u02c2\3\2"+
		"\2\2\u039d\u02c5\3\2\2\2\u039d\u02c8\3\2\2\2\u039d\u02cb\3\2\2\2\u039d"+
		"\u02ce\3\2\2\2\u039d\u02d0\3\2\2\2\u039d\u02d2\3\2\2\2\u039d\u02d4\3\2"+
		"\2\2\u039d\u02d7\3\2\2\2\u039d\u02dc\3\2\2\2\u039d\u02de\3\2\2\2\u039d"+
		"\u02e0\3\2\2\2\u039d\u02e7\3\2\2\2\u039d\u02f1\3\2\2\2\u039d\u02fb\3\2"+
		"\2\2\u039d\u0305\3\2\2\2\u039d\u030f\3\2\2\2\u039d\u031a\3\2\2\2\u039d"+
		"\u0322\3\2\2\2\u039d\u0329\3\2\2\2\u039d\u0330\3\2\2\2\u039d\u0337\3\2"+
		"\2\2\u039d\u0341\3\2\2\2\u039d\u034b\3\2\2\2\u039d\u0355\3\2\2\2\u039d"+
		"\u035f\3\2\2\2\u039d\u036a\3\2\2\2\u039d\u0372\3\2\2\2\u039d\u0379\3\2"+
		"\2\2\u039d\u0380\3\2\2\2\u039d\u0389\3\2\2\2\u039d\u038f\3\2\2\2\u039d"+
		"\u0395\3\2\2\2\u039e\u03a1\3\2\2\2\u039f\u039d\3\2\2\2\u039f\u03a0\3\2"+
		"\2\2\u03a0+\3\2\2\2\u03a1\u039f\3\2\2\2\u03a2\u03a3\b\27\1\2\u03a3\u03a4"+
		"\7\u00ac\2\2\u03a4\u03a9\5*\26\2\u03a5\u03a6\7\6\2\2\u03a6\u03a8\5*\26"+
		"\2\u03a7\u03a5\3\2\2\2\u03a8\u03ab\3\2\2\2\u03a9\u03a7\3\2\2\2\u03a9\u03aa"+
		"\3\2\2\2\u03aa\u03ac\3\2\2\2\u03ab\u03a9\3\2\2\2\u03ac\u03ad\b\27\1\2"+
		"\u03ad-\3\2\2\2\u03ae\u03af\7\u00ac\2\2\u03af\u03b0\7\b\2\2\u03b0\u03b1"+
		"\7\u00b0\2\2\u03b1\u03b2\7\7\2\2\u03b2\u03b9\5*\26\2\u03b3\u03b4\7\6\2"+
		"\2\u03b4\u03b5\7\u00b0\2\2\u03b5\u03b6\7\7\2\2\u03b6\u03b8\5*\26\2\u03b7"+
		"\u03b3\3\2\2\2\u03b8\u03bb\3\2\2\2\u03b9\u03b7\3\2\2\2\u03b9\u03ba\3\2"+
		"\2\2\u03ba\u03bc\3\2\2\2\u03bb\u03b9\3\2\2\2\u03bc\u03bd\7\3\2\2\u03bd"+
		"/\3\2\2\2o\63AFRVo\u0095\u00a4\u00b1\u00bb\u00c0\u00c8\u00cc\u00d6\u00d9"+
		"\u00e9\u00ef\u00f6\u00fb\u0100\u0105\u010a\u010f\u0114\u0119\u011c\u0121"+
		"\u0124\u0129\u012c\u0131\u0136\u013b\u0140\u0145\u014a\u0150\u0155\u0159"+
		"\u015d\u0161\u0166\u016b\u0170\u0175\u017a\u017f\u0184\u0189\u018e\u0193"+
		"\u0198\u019d\u01a2\u01a7\u01ac\u01b1\u01b6\u01bb\u01c0\u01c6\u01cc\u01d2"+
		"\u01d7\u01dc\u01e5\u01f2\u01f9\u0223\u022e\u0238\u024c\u025a\u0260\u0268"+
		"\u0273\u027d\u0280\u028c\u0290\u02e5\u02ea\u02f4\u02fe\u0308\u0312\u0316"+
		"\u031d\u0325\u032c\u0333\u033a\u0344\u034e\u0358\u0362\u0366\u036d\u0375"+
		"\u037c\u0382\u0385\u038c\u0392\u039b\u039d\u039f\u03a9\u03b9";
	public static final ATN _ATN =
		ATNSimulator.deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}