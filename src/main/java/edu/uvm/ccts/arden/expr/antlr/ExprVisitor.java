// Generated from Expr.g4 by ANTLR 4.1
package edu.uvm.ccts.arden.expr.antlr;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link ExprParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface ExprVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link ExprParser#durationExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDurationExpr(@NotNull ExprParser.DurationExprContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#arccos}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArccos(@NotNull ExprParser.ArccosContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#no}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNo(@NotNull ExprParser.NoContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#asString}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsString(@NotNull ExprParser.AsStringContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#firstFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFirstFrom(@NotNull ExprParser.FirstFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#dot}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDot(@NotNull ExprParser.DotContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#temporalUnit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTemporalUnit(@NotNull ExprParser.TemporalUnitContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#minimumFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMinimumFrom(@NotNull ExprParser.MinimumFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#indexType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexType(@NotNull ExprParser.IndexTypeContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#durationUnit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDurationUnit(@NotNull ExprParser.DurationUnitContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#matches}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMatches(@NotNull ExprParser.MatchesContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#abs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAbs(@NotNull ExprParser.AbsContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#isIn}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsIn(@NotNull ExprParser.IsInContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#indexOfFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexOfFrom(@NotNull ExprParser.IndexOfFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#substring}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubstring(@NotNull ExprParser.SubstringContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#element}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElement(@NotNull ExprParser.ElementContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#clone}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClone(@NotNull ExprParser.CloneContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#occurEqual}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurEqual(@NotNull ExprParser.OccurEqualContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#isWithinFollowing}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsWithinFollowing(@NotNull ExprParser.IsWithinFollowingContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#unaryMinus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryMinus(@NotNull ExprParser.UnaryMinusContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#or}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOr(@NotNull ExprParser.OrContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#unaryList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryList(@NotNull ExprParser.UnaryListContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#parens}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParens(@NotNull ExprParser.ParensContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#log}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLog(@NotNull ExprParser.LogContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#atMostOrLeast}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtMostOrLeast(@NotNull ExprParser.AtMostOrLeastContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#extract}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExtract(@NotNull ExprParser.ExtractContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#timeOfDayVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimeOfDayVal(@NotNull ExprParser.TimeOfDayValContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#not}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNot(@NotNull ExprParser.NotContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#median}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMedian(@NotNull ExprParser.MedianContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#floor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFloor(@NotNull ExprParser.FloorContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#now}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNow(@NotNull ExprParser.NowContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#truncate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTruncate(@NotNull ExprParser.TruncateContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#isLessThanOrEqual}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsLessThanOrEqual(@NotNull ExprParser.IsLessThanOrEqualContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#nullVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNullVal(@NotNull ExprParser.NullValContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#occurBefore}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurBefore(@NotNull ExprParser.OccurBeforeContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#whereTimeIsPresent}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhereTimeIsPresent(@NotNull ExprParser.WhereTimeIsPresentContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#sort}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSort(@NotNull ExprParser.SortContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#interval}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInterval(@NotNull ExprParser.IntervalContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#minimum}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMinimum(@NotNull ExprParser.MinimumContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#before}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBefore(@NotNull ExprParser.BeforeContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#merge}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMerge(@NotNull ExprParser.MergeContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#isLessThan}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsLessThan(@NotNull ExprParser.IsLessThanContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#isBefore}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsBefore(@NotNull ExprParser.IsBeforeContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#unaryPlus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryPlus(@NotNull ExprParser.UnaryPlusContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#length}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLength(@NotNull ExprParser.LengthContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#addToList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAddToList(@NotNull ExprParser.AddToListContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#atTime}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtTime(@NotNull ExprParser.AtTimeContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#occurWithinFollowing}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurWithinFollowing(@NotNull ExprParser.OccurWithinFollowingContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#earliestFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEarliestFrom(@NotNull ExprParser.EarliestFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#occurWithinPreceding}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurWithinPreceding(@NotNull ExprParser.OccurWithinPrecedingContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#reverse}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReverse(@NotNull ExprParser.ReverseContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#isDataType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsDataType(@NotNull ExprParser.IsDataTypeContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#arctan}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArctan(@NotNull ExprParser.ArctanContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#sqrt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSqrt(@NotNull ExprParser.SqrtContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#occurWithinPast}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurWithinPast(@NotNull ExprParser.OccurWithinPastContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#asNumber}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsNumber(@NotNull ExprParser.AsNumberContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#divide}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDivide(@NotNull ExprParser.DivideContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#stringVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringVal(@NotNull ExprParser.StringValContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#replace}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReplace(@NotNull ExprParser.ReplaceContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#arcsin}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArcsin(@NotNull ExprParser.ArcsinContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#isWithinTo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsWithinTo(@NotNull ExprParser.IsWithinToContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#maximumFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMaximumFrom(@NotNull ExprParser.MaximumFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#isWithinSurrounding}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsWithinSurrounding(@NotNull ExprParser.IsWithinSurroundingContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#isObjectType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsObjectType(@NotNull ExprParser.IsObjectTypeContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#ago}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAgo(@NotNull ExprParser.AgoContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#decrease}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDecrease(@NotNull ExprParser.DecreaseContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#stdDev}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStdDev(@NotNull ExprParser.StdDevContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#extractChars}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExtractChars(@NotNull ExprParser.ExtractCharsContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#count}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCount(@NotNull ExprParser.CountContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#last}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLast(@NotNull ExprParser.LastContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#numberVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumberVal(@NotNull ExprParser.NumberValContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#booleanVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBooleanVal(@NotNull ExprParser.BooleanValContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#round}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRound(@NotNull ExprParser.RoundContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#where}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhere(@NotNull ExprParser.WhereContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#isWithinPreceding}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsWithinPreceding(@NotNull ExprParser.IsWithinPrecedingContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#lastFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLastFrom(@NotNull ExprParser.LastFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#binaryList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinaryList(@NotNull ExprParser.BinaryListContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#isAfter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsAfter(@NotNull ExprParser.IsAfterContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#buildString}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuildString(@NotNull ExprParser.BuildStringContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#uppercase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUppercase(@NotNull ExprParser.UppercaseContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#after}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAfter(@NotNull ExprParser.AfterContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#occurWithinTo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurWithinTo(@NotNull ExprParser.OccurWithinToContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#earliest}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEarliest(@NotNull ExprParser.EarliestContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#occurWithinSameDay}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurWithinSameDay(@NotNull ExprParser.OccurWithinSameDayContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#dayOfWeekFunc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDayOfWeekFunc(@NotNull ExprParser.DayOfWeekFuncContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#any}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAny(@NotNull ExprParser.AnyContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#isWithinSameDay}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsWithinSameDay(@NotNull ExprParser.IsWithinSameDayContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#subList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubList(@NotNull ExprParser.SubListContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#objNamedWith}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObjNamedWith(@NotNull ExprParser.ObjNamedWithContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#index}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndex(@NotNull ExprParser.IndexContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#init}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInit(@NotNull ExprParser.InitContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#latestFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLatestFrom(@NotNull ExprParser.LatestFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#indexFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexFrom(@NotNull ExprParser.IndexFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#and}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnd(@NotNull ExprParser.AndContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#isGreaterThanOrEqual}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsGreaterThanOrEqual(@NotNull ExprParser.IsGreaterThanOrEqualContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#extractAttrNames}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExtractAttrNames(@NotNull ExprParser.ExtractAttrNamesContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExp(@NotNull ExprParser.ExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#ceiling}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCeiling(@NotNull ExprParser.CeilingContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#sine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSine(@NotNull ExprParser.SineContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#isEqual}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsEqual(@NotNull ExprParser.IsEqualContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#maximum}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMaximum(@NotNull ExprParser.MaximumContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#asTime}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsTime(@NotNull ExprParser.AsTimeContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#tangent}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTangent(@NotNull ExprParser.TangentContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#isGreaterThan}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsGreaterThan(@NotNull ExprParser.IsGreaterThanContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#occurAfter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurAfter(@NotNull ExprParser.OccurAfterContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#sum}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSum(@NotNull ExprParser.SumContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#add}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdd(@NotNull ExprParser.AddContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#timeOfDayFunc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimeOfDayFunc(@NotNull ExprParser.TimeOfDayFuncContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#seqto}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSeqto(@NotNull ExprParser.SeqtoContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#occurWithinSurrounding}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurWithinSurrounding(@NotNull ExprParser.OccurWithinSurroundingContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#id}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitId(@NotNull ExprParser.IdContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#newObject}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNewObject(@NotNull ExprParser.NewObjectContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#attributeFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAttributeFrom(@NotNull ExprParser.AttributeFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#lowercase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLowercase(@NotNull ExprParser.LowercaseContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#objOrderedWith}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObjOrderedWith(@NotNull ExprParser.ObjOrderedWithContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#timeVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimeVal(@NotNull ExprParser.TimeValContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#trim}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTrim(@NotNull ExprParser.TrimContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#exist}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExist(@NotNull ExprParser.ExistContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#all}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAll(@NotNull ExprParser.AllContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#dayOfWeek}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDayOfWeek(@NotNull ExprParser.DayOfWeekContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#cosine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCosine(@NotNull ExprParser.CosineContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#removeFromList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRemoveFromList(@NotNull ExprParser.RemoveFromListContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#multiply}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiply(@NotNull ExprParser.MultiplyContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#variance}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariance(@NotNull ExprParser.VarianceContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#concat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConcat(@NotNull ExprParser.ConcatContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#nearest}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNearest(@NotNull ExprParser.NearestContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#log10}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLog10(@NotNull ExprParser.Log10Context ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#emptyList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEmptyList(@NotNull ExprParser.EmptyListContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#currentTime}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCurrentTime(@NotNull ExprParser.CurrentTimeContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#subtract}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubtract(@NotNull ExprParser.SubtractContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#dataType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDataType(@NotNull ExprParser.DataTypeContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#duration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDuration(@NotNull ExprParser.DurationContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#findInString}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFindInString(@NotNull ExprParser.FindInStringContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#isNotEqual}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsNotEqual(@NotNull ExprParser.IsNotEqualContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#increase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIncrease(@NotNull ExprParser.IncreaseContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#timeFunc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimeFunc(@NotNull ExprParser.TimeFuncContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#raiseToPower}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRaiseToPower(@NotNull ExprParser.RaiseToPowerContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#latest}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLatest(@NotNull ExprParser.LatestContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#isWithinPast}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsWithinPast(@NotNull ExprParser.IsWithinPastContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#first}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFirst(@NotNull ExprParser.FirstContext ctx);

	/**
	 * Visit a parse tree produced by {@link ExprParser#average}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAverage(@NotNull ExprParser.AverageContext ctx);
}