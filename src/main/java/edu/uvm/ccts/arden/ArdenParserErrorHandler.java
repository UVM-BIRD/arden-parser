/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Arden Parser.
 *
 * Arden Parser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Arden Parser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Arden Parser.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.arden;

import edu.uvm.ccts.common.exceptions.antlr.AntlrException;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.NotNull;
import org.apache.commons.logging.Log;

/**
 * Created by mstorer on 12/20/13.
 */
public class ArdenParserErrorHandler extends DefaultErrorStrategy {
    private Log log;

    public ArdenParserErrorHandler(Log log) {
        this.log = log;
    }

    @Override
    public void recover(Parser recognizer, RecognitionException e) {
        String message = "failed to parse Arden logic.  offending token: " + e.getOffendingToken().toString();
        throw new AntlrException(AntlrException.Type.PARSER, message, e);
    }

    @Override
    public Token recoverInline(Parser recognizer) throws RecognitionException {
        InputMismatchException e = new InputMismatchException(recognizer);
        String message = "failed to parse Arden logic.  offending token: " + e.getOffendingToken().toString();
        throw new AntlrException(AntlrException.Type.PARSER, message, e);
    }

    @Override
    public void reportError(Parser recognizer, RecognitionException e) {
        if (log.isDebugEnabled()) {
            super.reportError(recognizer, e);
        }
    }

    @Override
    public void sync(Parser recognizer) { }

    @Override
    protected void reportNoViableAlternative(@NotNull Parser recognizer, @NotNull NoViableAltException e) {
        log.debug("no viable alternative for token " + buildTokenInfo(e.getOffendingToken()));
    }

    @Override
    protected void reportInputMismatch(@NotNull Parser recognizer, @NotNull InputMismatchException e) {
        log.debug("input mismatch for token " + buildTokenInfo(e.getOffendingToken()));
    }

    @Override
    protected void reportFailedPredicate(@NotNull Parser recognizer, @NotNull FailedPredicateException e) {
        log.debug("failed predicate for token " + buildTokenInfo(e.getOffendingToken()));
    }

    private String buildTokenInfo(Token t) {
        return "'" + t.getText() + "' at (" + t.getLine() + ":" + t.getCharPositionInLine() + ")";
    }
}
