// Generated from MLM.g4 by ANTLR 4.1
package edu.uvm.ccts.arden.mlm.antlr;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MLMLexer extends Lexer {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, CategoryID=2, SlotID=3, Data=4, WS=5;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] tokenNames = {
		"<INVALID>",
		"'end:'", "CategoryID", "SlotID", "Data", "WS"
	};
	public static final String[] ruleNames = {
		"T__0", "CategoryID", "SlotID", "Data", "WS"
	};


	  private static final int NONE = 0;
	  private static final int CATEGORY = 1;
	  private static final int SLOT_DATA = 2;
	  private int level = NONE;


	public MLMLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "MLM.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	@Override
	public void action(RuleContext _localctx, int ruleIndex, int actionIndex) {
		switch (ruleIndex) {
		case 1: CategoryID_action((RuleContext)_localctx, actionIndex); break;

		case 2: SlotID_action((RuleContext)_localctx, actionIndex); break;

		case 3: Data_action((RuleContext)_localctx, actionIndex); break;

		case 4: WS_action((RuleContext)_localctx, actionIndex); break;
		}
	}
	private void Data_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 2: level=CATEGORY; break;
		}
	}
	private void WS_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 3: skip();  break;
		}
	}
	private void SlotID_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 1: level=SLOT_DATA; break;
		}
	}
	private void CategoryID_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 0: level=CATEGORY; break;
		}
	}
	@Override
	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 1: return CategoryID_sempred((RuleContext)_localctx, predIndex);

		case 2: return SlotID_sempred((RuleContext)_localctx, predIndex);

		case 3: return Data_sempred((RuleContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean Data_sempred(RuleContext _localctx, int predIndex) {
		switch (predIndex) {
		case 2: return level==SLOT_DATA;

		case 3: return !(_input.LA(0)==';' && _input.LA(1)==';');
		}
		return true;
	}
	private boolean SlotID_sempred(RuleContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1: return level==CATEGORY;
		}
		return true;
	}
	private boolean CategoryID_sempred(RuleContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0: return level==NONE || level==CATEGORY;
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\uacf5\uee8c\u4f5d\u8b0d\u4a45\u78bd\u1b2f\u3378\2\7[\b\1\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\5\3<\n\3\3\3\3\3\3\4\3\4\6\4B\n\4\r\4\16\4C\3\4\3\4\3\4\3\5\3\5\3"+
		"\5\6\5L\n\5\r\5\16\5M\3\5\3\5\3\5\3\5\3\5\3\6\6\6V\n\6\r\6\16\6W\3\6\3"+
		"\6\3M\7\3\3\1\5\4\2\7\5\3\t\6\4\13\7\5\3\2\4\3\2c|\5\2\13\f\17\17\"\""+
		"`\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\3\r\3\2"+
		"\2\2\5\22\3\2\2\2\7?\3\2\2\2\tH\3\2\2\2\13U\3\2\2\2\r\16\7g\2\2\16\17"+
		"\7p\2\2\17\20\7f\2\2\20\21\7<\2\2\21\4\3\2\2\2\22;\6\3\2\2\23\24\7o\2"+
		"\2\24\25\7c\2\2\25\26\7k\2\2\26\27\7p\2\2\27\30\7v\2\2\30\31\7g\2\2\31"+
		"\32\7p\2\2\32\33\7c\2\2\33\34\7p\2\2\34\35\7e\2\2\35\36\7g\2\2\36<\7<"+
		"\2\2\37 \7n\2\2 !\7k\2\2!\"\7d\2\2\"#\7t\2\2#$\7c\2\2$%\7t\2\2%&\7{\2"+
		"\2&<\7<\2\2\'(\7m\2\2()\7p\2\2)*\7q\2\2*+\7y\2\2+,\7n\2\2,-\7g\2\2-.\7"+
		"f\2\2./\7i\2\2/\60\7g\2\2\60<\7<\2\2\61\62\7t\2\2\62\63\7g\2\2\63\64\7"+
		"u\2\2\64\65\7q\2\2\65\66\7w\2\2\66\67\7t\2\2\678\7e\2\289\7g\2\29:\7u"+
		"\2\2:<\7<\2\2;\23\3\2\2\2;\37\3\2\2\2;\'\3\2\2\2;\61\3\2\2\2<=\3\2\2\2"+
		"=>\b\3\2\2>\6\3\2\2\2?A\6\4\3\2@B\t\2\2\2A@\3\2\2\2BC\3\2\2\2CA\3\2\2"+
		"\2CD\3\2\2\2DE\3\2\2\2EF\7<\2\2FG\b\4\3\2G\b\3\2\2\2HK\6\5\4\2IJ\6\5\5"+
		"\2JL\13\2\2\2KI\3\2\2\2LM\3\2\2\2MN\3\2\2\2MK\3\2\2\2NO\3\2\2\2OP\7=\2"+
		"\2PQ\7=\2\2QR\3\2\2\2RS\b\5\4\2S\n\3\2\2\2TV\t\3\2\2UT\3\2\2\2VW\3\2\2"+
		"\2WU\3\2\2\2WX\3\2\2\2XY\3\2\2\2YZ\b\6\5\2Z\f\3\2\2\2\7\2;CMW";
	public static final ATN _ATN =
		ATNSimulator.deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}