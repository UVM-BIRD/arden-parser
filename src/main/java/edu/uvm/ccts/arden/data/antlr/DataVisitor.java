// Generated from Data.g4 by ANTLR 4.1
package edu.uvm.ccts.arden.data.antlr;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link DataParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface DataVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link DataParser#durationExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDurationExpr(@NotNull DataParser.DurationExprContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#arccos}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArccos(@NotNull DataParser.ArccosContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#no}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNo(@NotNull DataParser.NoContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#asString}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsString(@NotNull DataParser.AsStringContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#firstFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFirstFrom(@NotNull DataParser.FirstFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#dot}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDot(@NotNull DataParser.DotContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#temporalUnit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTemporalUnit(@NotNull DataParser.TemporalUnitContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#minimumFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMinimumFrom(@NotNull DataParser.MinimumFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#indexType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexType(@NotNull DataParser.IndexTypeContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#matches}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMatches(@NotNull DataParser.MatchesContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#durationUnit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDurationUnit(@NotNull DataParser.DurationUnitContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#abs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAbs(@NotNull DataParser.AbsContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#isIn}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsIn(@NotNull DataParser.IsInContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#indexOfFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexOfFrom(@NotNull DataParser.IndexOfFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#substring}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubstring(@NotNull DataParser.SubstringContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#declareDestination}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclareDestination(@NotNull DataParser.DeclareDestinationContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#forLoop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForLoop(@NotNull DataParser.ForLoopContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#element}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElement(@NotNull DataParser.ElementContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#clone}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClone(@NotNull DataParser.CloneContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#occurEqual}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurEqual(@NotNull DataParser.OccurEqualContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#isWithinFollowing}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsWithinFollowing(@NotNull DataParser.IsWithinFollowingContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#unaryMinus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryMinus(@NotNull DataParser.UnaryMinusContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#or}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOr(@NotNull DataParser.OrContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#unaryList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryList(@NotNull DataParser.UnaryListContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#parens}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParens(@NotNull DataParser.ParensContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#atMostOrLeast}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtMostOrLeast(@NotNull DataParser.AtMostOrLeastContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#log}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLog(@NotNull DataParser.LogContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#extract}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExtract(@NotNull DataParser.ExtractContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#timeOfDayVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimeOfDayVal(@NotNull DataParser.TimeOfDayValContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#include}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInclude(@NotNull DataParser.IncludeContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#aggregation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAggregation(@NotNull DataParser.AggregationContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#not}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNot(@NotNull DataParser.NotContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#median}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMedian(@NotNull DataParser.MedianContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#declareDestinationAs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclareDestinationAs(@NotNull DataParser.DeclareDestinationAsContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#floor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFloor(@NotNull DataParser.FloorContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#now}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNow(@NotNull DataParser.NowContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#truncate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTruncate(@NotNull DataParser.TruncateContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#isLessThanOrEqual}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsLessThanOrEqual(@NotNull DataParser.IsLessThanOrEqualContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#nullVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNullVal(@NotNull DataParser.NullValContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#occurBefore}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurBefore(@NotNull DataParser.OccurBeforeContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#objOrIndexRule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObjOrIndexRule(@NotNull DataParser.ObjOrIndexRuleContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#whereTimeIsPresent}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhereTimeIsPresent(@NotNull DataParser.WhereTimeIsPresentContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#sort}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSort(@NotNull DataParser.SortContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#interval}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInterval(@NotNull DataParser.IntervalContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#minimum}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMinimum(@NotNull DataParser.MinimumContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#before}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBefore(@NotNull DataParser.BeforeContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#merge}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMerge(@NotNull DataParser.MergeContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#constraint}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstraint(@NotNull DataParser.ConstraintContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(@NotNull DataParser.BlockContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#isLessThan}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsLessThan(@NotNull DataParser.IsLessThanContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#isBefore}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsBefore(@NotNull DataParser.IsBeforeContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#unaryPlus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryPlus(@NotNull DataParser.UnaryPlusContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#addToList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAddToList(@NotNull DataParser.AddToListContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#length}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLength(@NotNull DataParser.LengthContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#atTime}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtTime(@NotNull DataParser.AtTimeContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#occurWithinFollowing}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurWithinFollowing(@NotNull DataParser.OccurWithinFollowingContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#earliestFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEarliestFrom(@NotNull DataParser.EarliestFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#occurWithinPreceding}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurWithinPreceding(@NotNull DataParser.OccurWithinPrecedingContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#multipleAssignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultipleAssignment(@NotNull DataParser.MultipleAssignmentContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#reverse}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReverse(@NotNull DataParser.ReverseContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#isDataType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsDataType(@NotNull DataParser.IsDataTypeContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#arctan}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArctan(@NotNull DataParser.ArctanContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#sqrt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSqrt(@NotNull DataParser.SqrtContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#occurWithinPast}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurWithinPast(@NotNull DataParser.OccurWithinPastContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#asNumber}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsNumber(@NotNull DataParser.AsNumberContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#divide}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDivide(@NotNull DataParser.DivideContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#stringVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringVal(@NotNull DataParser.StringValContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#declareEvent}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclareEvent(@NotNull DataParser.DeclareEventContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#replace}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReplace(@NotNull DataParser.ReplaceContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#arcsin}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArcsin(@NotNull DataParser.ArcsinContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#isWithinTo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsWithinTo(@NotNull DataParser.IsWithinToContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment(@NotNull DataParser.AssignmentContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#switchStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwitchStatement(@NotNull DataParser.SwitchStatementContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#maximumFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMaximumFrom(@NotNull DataParser.MaximumFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#isWithinSurrounding}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsWithinSurrounding(@NotNull DataParser.IsWithinSurroundingContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#isObjectType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsObjectType(@NotNull DataParser.IsObjectTypeContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#whileLoop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileLoop(@NotNull DataParser.WhileLoopContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#ago}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAgo(@NotNull DataParser.AgoContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#declareObject}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclareObject(@NotNull DataParser.DeclareObjectContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#decrease}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDecrease(@NotNull DataParser.DecreaseContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#stdDev}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStdDev(@NotNull DataParser.StdDevContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#extractChars}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExtractChars(@NotNull DataParser.ExtractCharsContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#count}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCount(@NotNull DataParser.CountContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#last}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLast(@NotNull DataParser.LastContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#numberVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumberVal(@NotNull DataParser.NumberValContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#booleanVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBooleanVal(@NotNull DataParser.BooleanValContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#round}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRound(@NotNull DataParser.RoundContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#where}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhere(@NotNull DataParser.WhereContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#isWithinPreceding}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsWithinPreceding(@NotNull DataParser.IsWithinPrecedingContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#lastFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLastFrom(@NotNull DataParser.LastFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#binaryList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinaryList(@NotNull DataParser.BinaryListContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#isAfter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsAfter(@NotNull DataParser.IsAfterContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#after}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAfter(@NotNull DataParser.AfterContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#buildString}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBuildString(@NotNull DataParser.BuildStringContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#uppercase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUppercase(@NotNull DataParser.UppercaseContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#occurWithinTo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurWithinTo(@NotNull DataParser.OccurWithinToContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#earliest}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEarliest(@NotNull DataParser.EarliestContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#occurWithinSameDay}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurWithinSameDay(@NotNull DataParser.OccurWithinSameDayContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#any}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAny(@NotNull DataParser.AnyContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#dayOfWeekFunc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDayOfWeekFunc(@NotNull DataParser.DayOfWeekFuncContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#isWithinSameDay}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsWithinSameDay(@NotNull DataParser.IsWithinSameDayContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#subList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubList(@NotNull DataParser.SubListContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#objNamedWith}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObjNamedWith(@NotNull DataParser.ObjNamedWithContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#argument}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArgument(@NotNull DataParser.ArgumentContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#assignable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignable(@NotNull DataParser.AssignableContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#index}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndex(@NotNull DataParser.IndexContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#continueLoop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitContinueLoop(@NotNull DataParser.ContinueLoopContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#init}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInit(@NotNull DataParser.InitContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#latestFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLatestFrom(@NotNull DataParser.LatestFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#indexFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexFrom(@NotNull DataParser.IndexFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#declareInterface}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclareInterface(@NotNull DataParser.DeclareInterfaceContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#and}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnd(@NotNull DataParser.AndContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#isGreaterThanOrEqual}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsGreaterThanOrEqual(@NotNull DataParser.IsGreaterThanOrEqualContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#extractAttrNames}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExtractAttrNames(@NotNull DataParser.ExtractAttrNamesContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#declareMlm}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclareMlm(@NotNull DataParser.DeclareMlmContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#exp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExp(@NotNull DataParser.ExpContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#breakLoop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBreakLoop(@NotNull DataParser.BreakLoopContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#sine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSine(@NotNull DataParser.SineContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#ceiling}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCeiling(@NotNull DataParser.CeilingContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#isEqual}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsEqual(@NotNull DataParser.IsEqualContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#maximum}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMaximum(@NotNull DataParser.MaximumContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#asTime}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAsTime(@NotNull DataParser.AsTimeContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#tangent}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTangent(@NotNull DataParser.TangentContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#enhancedAssignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnhancedAssignment(@NotNull DataParser.EnhancedAssignmentContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#declareMessage}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclareMessage(@NotNull DataParser.DeclareMessageContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#isGreaterThan}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsGreaterThan(@NotNull DataParser.IsGreaterThanContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#call}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCall(@NotNull DataParser.CallContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#occurAfter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurAfter(@NotNull DataParser.OccurAfterContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#sum}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSum(@NotNull DataParser.SumContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#readAs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReadAs(@NotNull DataParser.ReadAsContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#add}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdd(@NotNull DataParser.AddContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#seqto}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSeqto(@NotNull DataParser.SeqtoContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#timeOfDayFunc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimeOfDayFunc(@NotNull DataParser.TimeOfDayFuncContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#occurWithinSurrounding}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOccurWithinSurrounding(@NotNull DataParser.OccurWithinSurroundingContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#id}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitId(@NotNull DataParser.IdContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#newObject}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNewObject(@NotNull DataParser.NewObjectContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#attributeFrom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAttributeFrom(@NotNull DataParser.AttributeFromContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#lowercase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLowercase(@NotNull DataParser.LowercaseContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#objOrderedWith}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitObjOrderedWith(@NotNull DataParser.ObjOrderedWithContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#timeVal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimeVal(@NotNull DataParser.TimeValContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#read}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRead(@NotNull DataParser.ReadContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#trim}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTrim(@NotNull DataParser.TrimContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#exist}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExist(@NotNull DataParser.ExistContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#dayOfWeek}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDayOfWeek(@NotNull DataParser.DayOfWeekContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#all}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAll(@NotNull DataParser.AllContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#cosine}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCosine(@NotNull DataParser.CosineContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt(@NotNull DataParser.StmtContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#removeFromList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRemoveFromList(@NotNull DataParser.RemoveFromListContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#multiply}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiply(@NotNull DataParser.MultiplyContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#variance}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariance(@NotNull DataParser.VarianceContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#concat}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConcat(@NotNull DataParser.ConcatContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#nearest}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNearest(@NotNull DataParser.NearestContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#emptyList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEmptyList(@NotNull DataParser.EmptyListContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#transformation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTransformation(@NotNull DataParser.TransformationContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#print}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrint(@NotNull DataParser.PrintContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#log10}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLog10(@NotNull DataParser.Log10Context ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#ifStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfStatement(@NotNull DataParser.IfStatementContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#currentTime}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCurrentTime(@NotNull DataParser.CurrentTimeContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#subtract}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubtract(@NotNull DataParser.SubtractContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#duration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDuration(@NotNull DataParser.DurationContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#dataType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDataType(@NotNull DataParser.DataTypeContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#findInString}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFindInString(@NotNull DataParser.FindInStringContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#isNotEqual}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsNotEqual(@NotNull DataParser.IsNotEqualContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#declareMessageAs}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclareMessageAs(@NotNull DataParser.DeclareMessageAsContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#increase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIncrease(@NotNull DataParser.IncreaseContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#raiseToPower}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRaiseToPower(@NotNull DataParser.RaiseToPowerContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#timeFunc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimeFunc(@NotNull DataParser.TimeFuncContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#latest}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLatest(@NotNull DataParser.LatestContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#isWithinPast}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsWithinPast(@NotNull DataParser.IsWithinPastContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#first}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFirst(@NotNull DataParser.FirstContext ctx);

	/**
	 * Visit a parse tree produced by {@link DataParser#average}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAverage(@NotNull DataParser.AverageContext ctx);
}