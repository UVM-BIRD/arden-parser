/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Arden Parser.
 *
 * Arden Parser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Arden Parser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Arden Parser.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.uvm.ccts.arden.data;

import edu.uvm.ccts.arden.data.antlr.DataParser;
import edu.uvm.ccts.arden.ArdenParserErrorHandler;
import org.antlr.v4.runtime.TokenStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by mstorer on 12/18/13.
 */
public class BailDataParser extends DataParser {
    private static final Log log = LogFactory.getLog(DataParser.class);

    public BailDataParser(TokenStream input) {
        super(input);
        setErrorHandler(new ArdenParserErrorHandler(log));
    }
}
