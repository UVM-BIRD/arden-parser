// Copyright 2015 The University of Vermont and State
// Agricultural College, Vermont Oxford Network, and The University
// of Vermont Medical Center.  All rights reserved.
//
// Written by Matthew B. Storer <matthewbstorer@gmail.com>
//
// This file is part of Arden Parser.
//
// Arden Parser is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Arden Parser is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Arden Parser.  If not, see <http://www.gnu.org/licenses/>.

grammar JMethodSig;

init : className ':' methodName parameters EOF ;

className : (ID '.')+ ID ;

methodName : ID ;

parameters : '(' (type (',' type)*)? ')' ;

type : PrimitiveType
     | complexType
     ;

complexType : ListType ('<' type '>')?
            | MapType ('<' type ',' type '>')?
            ;


///////////////////////////////////////////////////////////
// lexer rules
//

ListType : 'Collection'
         | 'List'
         ;

MapType : 'Map'
        ;

PrimitiveType : [Bb] 'yte'
              | [Ss] 'hort'
              | ('int' | 'Integer')
              | [Ll] 'ong'
              | [Ff] 'loat'
              | [Dd] 'ouble'
              | [Bb] 'oolean'
              | ('char' | 'Character')
              | 'String'
              | 'Date'
              ;

ID : [a-zA-Z_] [a-zA-Z0-9_]* ;

WS : [ \t\r\n]+ -> skip ;                       // whitespace
