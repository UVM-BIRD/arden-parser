/*
 * Copyright 2015 The University of Vermont and State
 * Agricultural College, Vermont Oxford Network, and The University
 * of Vermont Medical Center.  All rights reserved.
 *
 * Written by Matthew B. Storer <matthewbstorer@gmail.com>
 *
 * This file is part of Arden Parser.
 *
 * Arden Parser is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Arden Parser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Arden Parser.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by mstorer on 12/17/13.
 */
public class GrammarBuilder {
    private static final Pattern INCLUDE_PATTERN = Pattern.compile("^\\s*\\/\\/\\s*gbinc:.+");
    private static final Pattern COPYRIGHT_PATTERN = Pattern.compile("^\\s*\\/\\/\\s*Copyright\\s*.*");
    private static final Pattern COMMENT_PATTERN = Pattern.compile("^\\s*\\/\\/.*");

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("please provide an Antlr 4 Grammar Template (G4T) filename");
            System.exit(-1);
        }

        try {
            new GrammarBuilder().build(args[0]);
            System.exit(0);

        } catch (Exception e) {
            System.out.println("caught " + e.getClass().getName() + " - " + e.getMessage());
            e.printStackTrace();
            System.exit(-1);
        }
    }

    private void build(String filename) throws IOException {
        if (filename.isEmpty() || ! filename.toLowerCase().endsWith(".g4t")) {
            throw new IOException("invalid Antlr 4 Grammar Template (G4T) file : " + filename);
        }

        StringBuilder sb = new StringBuilder();

        build(filename, sb, true);

        int index = filename.lastIndexOf('.');
        String outFilename = index > 0 ?
                filename.substring(0, index) + ".g4" :
                filename + ".g4";

        write(outFilename, sb.toString());

        System.out.println("successfully generated grammar " + outFilename);
    }

    private void build(String filename, StringBuilder sb, boolean includeCopyright) throws IOException {
        boolean inCopyright = false;

        for (String line : readLines(filename)) {
            if ( ! inCopyright && COPYRIGHT_PATTERN.matcher(line).matches() ) {
                inCopyright = true;

            } else if (inCopyright && ! COMMENT_PATTERN.matcher(line).matches()) {
                inCopyright = false;
            }

            if (INCLUDE_PATTERN.matcher(line).matches()) {
                inCopyright = false;
                int index = line.indexOf(':');
                String incFilename = line.substring(index + 1) + ".g4i";
                String path = getPathPart(filename);

                if ( ! path.isEmpty() ) {
                    build(path + "/" + incFilename, sb, false);

                } else {
                    build(incFilename, sb, false);
                }

            } else if ((includeCopyright && inCopyright) || ! inCopyright) {
                sb.append(line).append("\n");
            }
        }
    }

    private void write(String filename, String s) throws IOException {
        OutputStream output = new BufferedOutputStream(new FileOutputStream(filename, false));

        try {
            output.write(s.getBytes());

        } finally {
            try { output.flush(); } catch (Exception e) {}
            try { output.close(); } catch (Exception e) {}
        }
    }

    private String getPathPart(String filename) {
        int index = filename.lastIndexOf(File.separatorChar);
        return index >= 0 ? filename.substring(0, index) : "";
    }

    private List<String> readLines(String filename) throws IOException {
        List<String> lines = new ArrayList<String>();

        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                lines.add(line);
            }

        } finally {
            try { reader.close(); } catch (Exception e) {}
        }

        return lines;
    }
}
