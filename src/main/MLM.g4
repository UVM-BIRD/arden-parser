// Copyright 2015 The University of Vermont and State
// Agricultural College, Vermont Oxford Network, and The University
// of Vermont Medical Center.  All rights reserved.
//
// Written by Matthew B. Storer <matthewbstorer@gmail.com>
//
// This file is part of Arden Parser.
//
// Arden Parser is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Arden Parser is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Arden Parser.  If not, see <http://www.gnu.org/licenses/>.

grammar MLM;

@lexer::members {
  private static final int NONE = 0;
  private static final int CATEGORY = 1;
  private static final int SLOT_DATA = 2;
  private int level = NONE;
}

init : category+ 'end:' EOF ;

category : CategoryID slot+ ;

slot : SlotID Data ;


//////////////////////////////////////////////////////////////
// LEXER RULES

CategoryID : {level==NONE || level==CATEGORY}?
              ('maintenance:' | 'library:' | 'knowledge:' | 'resources:')
              {level=CATEGORY;}
            ;

SlotID : {level==CATEGORY}?
          [a-z]+ ':'
          {level=SLOT_DATA;}
        ;

Data : {level==SLOT_DATA}?
       ({!(_input.LA(0)==';' && _input.LA(1)==';')}? . )+? ';;'
       {level=CATEGORY;}
     ;

WS : [ \t\r\n]+ -> skip;